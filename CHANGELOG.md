## 0.0.1

* add Chat Page
* add Login Page
* add Registration Page
* add Styling 

## 0.0.2

* Improvement Chat Page

## 0.0.3

* Fixing bug read chat

## 0.0.4

* Fixing reply widget
## 0.0.5

* Update photo widget
## 0.0.6

* add onCollapsedTap
## 0.0.7

* fixing darktheme
## 0.0.8

* add onCollapsedTapDown sidebar
## 0.0.9

* fix bug onCollapsedTapDown sidebar
## 0.0.10

* add checkbox
## 0.0.11

* add combobox
## 0.0.12

* fix bug combobox
## 0.0.13

* add combobox data
## 0.0.14

* Fix Styling combobox
## 0.0.15

* Fix bug ontap upload widget
## 0.0.16

* add widget_size widget
## 0.0.17

* Fix Login gradient background
## 0.0.18

* add multilevel menu
## 0.0.19

* onchange size topbar & sidebar
## 0.0.20

* Fixing chatbox desc overflow
## 0.0.21

* Add table
## 0.0.22

* Add Auth Template
## 0.0.23

* Refactor table pagination
## 0.0.24

* add collapsible
## 0.0.25

* add collapsible group
## 0.0.26

* fixing bug dialog widget
## 0.0.27

* fixing bug login button
## 0.0.28

* fixing Registration confirm password
## 0.0.29

* fixing bug chat bubble
## 0.0.30

* add chat feature
## 0.0.31

* Fixing chat feature
## 0.0.32

* Fixing send chat customProperties
## 0.0.33

* Fixing customFeature 
## 0.0.34

* Fixing customFeature  render

## 0.0.35

* Fixing User Model
## 0.0.36

* Add rating dialog
## 0.0.37

* add refresh token payload
## 0.0.38

* add function chat ontap header
## 0.0.39

* fix bug header chat tap
## 0.0.40

* fix bug header chat tap
## 0.0.41

* fix bug header chat tap
## 0.0.43

* add day model calendar

## 0.0.44

* add blog template
## 0.0.45

* add blog model
* fixing widget size
* fixing navbar menu
* fixing calendar
## 0.0.46

* add event model
## 0.0.47

* fixing blog model
## 0.0.48

* add firebase instance in blog model
## 0.0.49

* add blog editorbl
## 0.0.50

* add academy editor
## 0.0.51

* fixing academy model
## 0.0.52

* fixing card
## 0.0.53

* fixing card
## 0.0.54

* add misc editor

## 0.0.55

* add misc content model
## 0.0.56

* fix misc content model
## 0.0.57

* fix blog model
## 0.0.58

* fix blog model
## 0.0.59

* fix Input Widget
## 0.0.60

* fix Input Widget
## 0.0.61

* edit misc content

## 0.0.62

* input/add-maxlength
## 0.0.63

* Fixing Markdown Viewer
## 0.0.64

* Update user model
## 0.0.65

* Update google fonts
## 0.0.66

* Upgrade all dependencies
## 0.0.67

* Upgrade html_editor_enhanced
## 0.0.68
* Update flutter sdk
## 0.0.69
* Update flutter sdk
## 0.0.70
* Update flutter sdk
## 0.0.71
* Update dependencies
## 0.0.72
* Update dependencies
## 0.0.73
* Update dependencies
