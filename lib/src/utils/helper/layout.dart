import 'package:flutter/material.dart';

vspace(double? size) {
  return SizedBox(
    height: size,
  );
}

hspace(double? size) {
  return SizedBox(
    width: size,
  );
}

class CustomShape extends CustomPainter {
  final Color bgColor;
  final bool isMe;

  CustomShape(this.bgColor, this.isMe);

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()..color = bgColor;

    var path = Path();
    if (isMe) {
      path.moveTo(-5, -2);
      path.lineTo(-24, -7);
      path.lineTo(-19, -20);
    } else {
      path.lineTo(30, -15);
      path.lineTo(10, -30);
    }

    // path.lineTo(-10, 0);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
