import 'package:ametory_ui/ametory_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

Future<T?> alert<T>(
  BuildContext context,
  String message, {
  TextStyle? textStyle,
  EdgeInsets? padding,
  Widget? header,
  Widget? child,
  Widget? footer,
  double? width,
  double? height,
  BorderRadius? borderRadius,
  Color? textColor,
  Color? backgroundColor,
  TextAlign? textAlign,
  double? elevation,
  bool hideDefaultHeader = true,
  bool hideDefaultFooter = true,
  bool centerMessage = true,
  String defaultHeader = "Attention",
  String cancelText = "Cancel",
  String okText = "OK",
  Function()? onTapOK,
}) {
  return showDialog<T>(
      context: context,
      builder: (context) => Dialog(
            // elevation: elevation,
            backgroundColor: backgroundColor,
            shape: RoundedRectangleBorder(borderRadius: borderRadius ?? BorderRadius.circular(10)),
            child: Container(
              width: width ?? 300,
              height: height ?? 200,
              padding: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  header ??
                      (!hideDefaultHeader
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  defaultHeader,
                                  style: MyTypo.heading4.copyWith(fontWeight: FontWeight.w600),
                                ),
                                InkWell(
                                    onTap: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: const Icon(
                                      Icons.close,
                                      size: 14,
                                      color: Colors.grey,
                                    ))
                              ],
                            )
                          : Container()),
                  if (!hideDefaultHeader) vspace(10),
                  Expanded(
                    child: child ??
                        (!centerMessage
                            ? Text(
                                message,
                                style: MyTypo.bodyText1.copyWith(color: textColor),
                                textAlign: textAlign ?? TextAlign.left,
                              )
                            : Center(
                                child: Text(
                                  message,
                                  style: MyTypo.bodyText1.copyWith(color: textColor),
                                  textAlign: textAlign ?? TextAlign.center,
                                ),
                              )),
                  ),
                  if (!hideDefaultFooter) vspace(10),
                  footer ??
                      (!hideDefaultFooter
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  style: MyStyle.buttonRed,
                                  child: Text(cancelText),
                                ),
                                TextButton(
                                  onPressed: () {
                                    if (onTapOK != null) {
                                      onTapOK();
                                    } else {
                                      Navigator.of(context).pop();
                                    }
                                  },
                                  style: MyStyle.buttonWhite,
                                  child: Text(okText),
                                ),
                              ],
                            )
                          : Container()),
                ],
              ),
            ),
          ));
}

Future<T?> popUpMenu<T>(
  BuildContext context, {
  double? elevation,
  T? initialValue,
  String? semanticLabel,
  ShapeBorder? shape,
  Color? color,
  bool useRootNavigator = false,
  BoxConstraints? constraints,
  required Offset offset,
  required List<PopupMenuEntry<T>> items,
}) {
  final RenderObject? overlay = Overlay.of(context).context.findRenderObject();
  return showMenu<T>(
      context: context,
      elevation: elevation ?? 1,
      color: color,
      constraints: constraints,
      initialValue: initialValue,
      position: RelativeRect.fromSize(offset & const Size(40, 40), overlay!.paintBounds.size),
      items: items);
}

flush(
  BuildContext context,
  message, {
  int duration = 3,
  double? width,
  String ok = "OK",
  Function()? onPressed,
  TextStyle? textStyle,
  Color? actionTextColor,
  Color? backgroundColor,
  EdgeInsetsGeometry? margin,
  EdgeInsetsGeometry? padding,
  double? elevation,
}) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    behavior: SnackBarBehavior.floating,
    margin: margin,
    padding: padding,
    backgroundColor: backgroundColor,
    elevation: elevation ?? 1,
    width: width ?? 300,
    content: Text(message, style: textStyle),
    duration: Duration(seconds: duration),
    action: SnackBarAction(
      label: ok,
      textColor: actionTextColor,
      onPressed: onPressed ??
          () {
            Clipboard.setData(ClipboardData(text: message));
          },
    ),
  ));
}
