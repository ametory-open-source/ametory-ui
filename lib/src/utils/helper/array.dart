List<List<Object>> arrayChunk(List<Object> arr, int chunkSize) {
  List<List<Object>> chunks = [];
  for (var i = 0; i < arr.length; i += chunkSize) {
    chunks.add(arr.sublist(
        i, i + chunkSize > arr.length ? arr.length : i + chunkSize));
  }
  return chunks;
}
