import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_auth/firebase_auth.dart';

Future<String?> uploadToStorage(
    Uint8List data, String filename, String mimeType) async {
  try {
    FirebaseAuth.instance.currentUser;
    final storageRef = FirebaseStorage.instance.ref("images/$filename");
    await storageRef.putData(data, SettableMetadata(contentType: mimeType));
    return filename;
  } on FirebaseException catch (e) {
    print("ERROR $e");
  }
  return null;
}

// String getStorageUrl(BuildContext context, String, storageBaseUrl, String path) {
//   return Uri.parse("$storageBaseUrl$path?alt=media").toString();
// }
