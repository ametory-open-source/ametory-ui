import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:google_sign_in/google_sign_in.dart';

googleLogout() async {
  final GoogleSignIn googleSignIn = GoogleSignIn();
  try {
    if (!kIsWeb) {
      await googleSignIn.signOut();
    }
    await FirebaseAuth.instance.signOut();
  } catch (e) {
    print("ERROR LOGOUT");
  }
}

Future<UserCredential> loginWithCustomToken(String token) async {
  return await FirebaseAuth.instance.signInWithCustomToken(token);
}

Future<User?> googleUser() async {
  return FirebaseAuth.instance.currentUser;
}

Future<User?> googleSilentlyAuth() async {
  final GoogleSignIn googleSignIn = GoogleSignIn(
    scopes: [
      'email',
    ],
  );
  try {
    GoogleSignInAccount? googleAuth = await googleSignIn.signInSilently();

    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleAuth!.authentication;
    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );
    final userCredential =
        await FirebaseAuth.instance.signInWithCredential(credential);
    return userCredential.user;
  } catch (e) {
    return null;
  }
}
