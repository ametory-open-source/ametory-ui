import 'package:ametory_ui/src/models/chat/chat.dart';
import 'package:ametory_ui/src/models/user_model.dart';
import 'package:ametory_ui/src/utils/helper/string.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

import '../../models/chat/room.dart';

Future<String> createRoom(MyRoom roomData, {String roomKey = "rooms"}) async {
  try {
    var id = const Uuid().v4();
    CollectionReference<MyRoom> roomsRef =
        FirebaseFirestore.instance.collection(roomKey).withConverter(
              fromFirestore: (snapshot, _) => MyRoom.fromJson(snapshot.data()!),
              toFirestore: (MyRoom room, _) => room.toJson(),
            );
    roomData.id = id;
    // check exist room
    final existsRoom = await roomsRef
        .where("user_ids", arrayContains: roomData.userIDs)
        .where("company_id", isEqualTo: roomData.companyID)
        .get();
    if (existsRoom.docs.isNotEmpty) {
      return existsRoom.docs.first.id;
    }
    await roomsRef.doc(id).set(roomData);

    return id;
  } catch (e) {
    print("ERROR CREATE ROOM $e");
    return "";
  }
}

sendChat(String textMsg, UserModel userData, String roomID, String companyID,
    {String roomKey = "rooms",
    String chatKey = "chats",
    MyChatMedia? media,
    Map<String, dynamic>? customProperties}) async {
  final chatsRef = FirebaseFirestore.instance
      .collection(roomKey)
      .doc(roomID)
      .collection(chatKey)
      .withConverter(
        fromFirestore: (snapshot, _) =>
            MyChatMessage.fromJson(snapshot.data()!),
        toFirestore: (MyChatMessage room, _) => room.toJson(),
      );
  String chatID = const Uuid().v4();
  List<MyChatMedia> chatMedia = [];
  if (isYoutube(textMsg)) {
    chatMedia.add(MyChatMedia(
        uploadedDate: DateTime.now().toLocal(),
        customProperties: {
          "is_youtube": true,
        },
        url: textMsg,
        fileName: textMsg,
        type: MyMediaType.video));
  }
  if (media != null) {
    media.customProperties = {};
    chatMedia.add(media);
  }

  MyChatMessage chatMessage = MyChatMessage(
      id: chatID,
      customProperties: customProperties ?? {},
      text: textMsg,
      createdAt: DateTime.now().toLocal(),
      medias: chatMedia,
      mentions: [],
      user: MyChatUser(
          id: userData.id,
          firstName: userData.name,
          profileImage: userData.avatar,
          customProperties: {
            "position": userData.position,
          }));
  await chatsRef.doc(chatID).set(chatMessage);
  var data = {
    "last_message_date": Timestamp.now(),
    "last_message": textMsg,
    "company_id": companyID,
    "readed_ids": [userData.id]
  };
  await roomUpdate(data, roomID, roomKey: roomKey);
}

deleteMessage(String chatID, String roomID,
    {String roomKey = "rooms", String chatKey = "chats"}) async {
  final chatsRef = FirebaseFirestore.instance
      .collection(roomKey)
      .doc(roomID)
      .collection(chatKey)
      .withConverter(
        fromFirestore: (snapshot, _) =>
            MyChatMessage.fromJson(snapshot.data()!),
        toFirestore: (MyChatMessage room, _) => room.toJson(),
      );
  await chatsRef.doc(chatID).delete();
}

roomUpdate(Map<String, dynamic> data, String roomID,
    {String roomKey = "rooms"}) async {
  final roomRef =
      FirebaseFirestore.instance.collection(roomKey).doc(roomID).withConverter(
            fromFirestore: (snapshot, _) => MyRoom.fromJson(snapshot.data()!),
            toFirestore: (MyRoom room, _) => room.toJson(),
          );
  await roomRef.update(data);
}

Future<MyRoom> getRoom(String roomID, {String roomKey = "rooms"}) async {
  DocumentSnapshot<MyRoom> data = await FirebaseFirestore.instance
      .collection(roomKey)
      .doc(roomID)
      .withConverter(
        fromFirestore: (snapshot, _) => MyRoom.fromJson(snapshot.data()!),
        toFirestore: (MyRoom room, _) => room.toJson(),
      )
      .get();

  return data.data()!;
}

int getUnreadChat(
    List<QueryDocumentSnapshot<MyRoom>> data, UserModel userData) {
  int unread = 0;

  for (var room in data) {
    int count = room.data().readedIDs!.where((e) => e == userData.id).length;
    if (count == 0 && room.data().lastMessage!.isNotEmpty) {
      unread++;
    }
  }
  return unread;
}
