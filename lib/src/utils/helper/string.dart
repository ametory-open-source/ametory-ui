import 'package:flutter/material.dart';
import 'package:flutter_parsed_text/flutter_parsed_text.dart';
import 'package:url_launcher/url_launcher.dart';

List<MatchText> defaultParsePatterns = <MatchText>[
  MatchText(
    type: ParsedType.URL,
    style: const TextStyle(
      decoration: TextDecoration.underline,
    ),
    onTap: (String url) {
      if (!url.startsWith('http://') && !url.startsWith('https://')) {
        url = 'http://$url';
      }
      launchUrl(Uri.parse(url));
    },
  ),
];

String? convertYoutubeUrlToId(String url, {bool trimWhitespaces = true}) {
  if (!url.contains("http") && (url.length == 11)) return url;
  if (trimWhitespaces) url = url.trim();

  for (var exp in [
    RegExp(
        r"^https:\/\/(?:www\.|m\.)?youtube\.com\/watch\?v=([_\-a-zA-Z0-9]{11}).*$"),
    RegExp(
        r"^https:\/\/(?:www\.|m\.)?youtube(?:-nocookie)?\.com\/embed\/([_\-a-zA-Z0-9]{11}).*$"),
    RegExp(r"^https:\/\/youtu\.be\/([_\-a-zA-Z0-9]{11}).*$")
  ]) {
    Match? match = exp.firstMatch(url);
    if (match != null && match.groupCount >= 1) return match.group(1);
  }

  return null;
}

bool isYoutube(String content) {
  RegExp regExp = RegExp(
      r'^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube(-nocookie)?\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$');
  RegExp regExp2 = RegExp(
      r"^https:\/\/(?:www\.|m\.)?youtube\.com\/watch\?v=([_\-a-zA-Z0-9]{11}).*$");
  String? matches = regExp.stringMatch(content);
  String? matches2 = regExp2.stringMatch(content);

  if (matches == null && matches2 == null) {
    return false; // Always returns here while the video URL is in the content paramter
  }
  return true;
}

String cleanComma(String text) {
  var res = text;
  try {
    res = (text.lastIndexOf(",") > 0)
        ? text.substring(0, text.lastIndexOf(","))
        : text;
  } catch (e) {
    print("ERROR $e => $text");
  }

  return res;
}

List<String> generateKeywords(List<String?> words) {
  List<String> keywords = [];
  for (var e in words) {
    if (e == null) continue;
    keywords.addAll(e.split(" ").map((e) => cleanComma(e).toLowerCase()));
  }

  keywords = keywords.toSet().toList();
  return keywords;
}
