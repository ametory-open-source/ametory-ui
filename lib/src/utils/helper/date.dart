String formatDate(DateTime date, {String format = "dd/MM/yyyy"}) {
  var str =
      format.replaceAll("dd", (date.day < 10 ? "0" : "") + date.day.toString());
  str = str.replaceAll(
      "MM", (date.month < 10 ? "0" : "") + date.month.toString());
  str = str.replaceAll("yyyy", date.year.toString());
  return str;
}

///get first date of week
DateTime getFirstDateOfWeek(DateTime date) {
  return date.weekday == 7 ? date : date.add(Duration(days: -date.weekday));
}

///get all days of week
List<int> getDaysOfWeek(DateTime date) {
  var firstDay = getFirstDateOfWeek(date);
  var days = <int>[];
  for (var i = 0; i < 7; i++) {
    days.add(firstDay.add(Duration(days: i)).day);
  }
  return days;
}

List<DateTime> getDaysOfWeekInDate(DateTime date) {
  var firstDay = getFirstDateOfWeek(date);
  var days = <DateTime>[];
  for (var i = 0; i < 7; i++) {
    days.add(firstDay.add(Duration(days: i)));
  }
  return days;
}

int getDayOfYear(DateTime date) {
  final diff = DateTime.now().difference(DateTime(date.year, 1, 1, 0, 0));
  return diff.inDays;
}

int getWeekOfYear(DateTime date) {
  final startOfYear = DateTime(date.year, 1, 1, 0, 0);
  final firstMonday = startOfYear.weekday;
  final daysInFirstWeek = 8 - firstMonday;
  final diff = date.difference(startOfYear);
  var weeks = ((diff.inDays - daysInFirstWeek) / 7).ceil();
  if (daysInFirstWeek > 3) {
    weeks += 1;
  }
  return weeks;
}

int getMonthOfYear(DateTime date) {
  return date.month;
}

bool isThisYear(DateTime now, DateTime then) {
  return now.year == then.year;
}

bool isThisMonth(DateTime now, DateTime then) {
  return DateTime(now.year, now.month, 1) == DateTime(then.year, then.month, 1);
}

bool isThisday(DateTime now, DateTime then) {
  return DateTime(now.year, now.month, now.day) ==
      DateTime(then.year, then.month, then.day);
}

bool isBeetween(DateTime start, DateTime end, DateTime now) {
  return now.difference(start).inDays >= 0 && end.difference(now).inDays >= 0;
}
