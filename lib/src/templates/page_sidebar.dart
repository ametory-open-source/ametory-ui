import 'package:ametory_ui/ametory_ui.dart';
import 'package:ametory_ui/src/models/menu_model.dart';
import 'package:ametory_ui/src/widgets/top_bar.dart';
import 'package:flutter/material.dart';
import 'package:seo_renderer/seo_renderer.dart';

import '../widgets/drawer_menu.dart';
import '../widgets/side_bar.dart';

class PageSideBar extends StatefulWidget {
  final GlobalKey<ScaffoldState>? scaffoldKey;
  final String? pageTitle;
  final Widget? topBarLeading;
  final Widget? endDrawer;
  final Widget? responsiveTopBar;
  final Widget? topBarContent;
  final Widget? topBarTrailing;
  final double? topBarSpacing;
  final Widget? floatingActionButton;
  final FloatingActionButtonAnimator? floatingActionButtonAnimator;
  final FloatingActionButtonLocation? floatingActionButtonLocation;
  final Color? drawerBackground;
  final Color? responsiveTopBarColor;
  final double? responsiveTopBarElevation;
  final Widget? responsiveLeading;
  final List<Widget>? responsiveActions;
  final bool isAuth;
  final bool isCollapsed;
  final bool hideSidebar;
  final bool hideTopbar;
  final bool hideDrawer;
  final bool drawerShowHeader;
  final Widget? drawerBeforeMenu;
  final EdgeInsets? paddingMenu;
  final TopBarTheme? topBarTheme;
  final SidebarTheme? sidebarTheme;
  final Widget? child;
  final EdgeInsets? paddingContent;
  final List<MenuModel>? sideBarMenu;
  final List<BottomNavigationBarItem>? bottomNavigationBarItem;
  final int? bottomNavigationBarIndex;
  final Widget? bottomNavigationBar;
  final Function(int)? bottomNavigationBarOnTap;
  final Function(bool, bool)? onCollapsedTap;
  final Function(bool, bool)? onCollapsedTapDown;
  final double collapsedWidth;
  final bool showLabelCollapsed;
  final Color? sidebarActiveMenuColor;
  final BorderRadius? sidebarActiveBorderRadius;
  final double? bottomMenuSelectedFontSize;
  final double? bottomMenuUnselectedFontSize;
  final double? endDrawerWidth;
  final Widget? drawerFooter;
  final Widget? sidebarTrailing;
  final Widget? sidebarLeading;
  final Function(Size)? onSidebarChanged;
  final Function(Size)? onTopbarChanged;
  final bool blogMode;

  const PageSideBar({
    Key? key,
    this.child,
    this.pageTitle,
    this.drawerBackground,
    this.floatingActionButton,
    this.responsiveTopBar,
    this.responsiveTopBarColor,
    this.responsiveTopBarElevation,
    this.responsiveActions,
    this.responsiveLeading,
    this.sidebarTheme,
    this.endDrawer,
    this.topBarTheme,
    this.topBarLeading,
    this.topBarContent,
    this.topBarTrailing,
    this.topBarSpacing,
    this.paddingContent,
    this.paddingMenu,
    this.drawerBeforeMenu,
    this.drawerShowHeader = true,
    this.showLabelCollapsed = true,
    this.isAuth = false,
    this.isCollapsed = false,
    this.hideSidebar = false,
    this.hideTopbar = false,
    this.hideDrawer = false,
    this.blogMode = false,
    this.collapsedWidth = 80,
    this.sideBarMenu,
    this.scaffoldKey,
    this.bottomNavigationBarIndex,
    this.bottomNavigationBar,
    this.bottomNavigationBarItem,
    this.bottomNavigationBarOnTap,
    this.floatingActionButtonAnimator,
    this.floatingActionButtonLocation,
    this.sidebarActiveMenuColor,
    this.bottomMenuSelectedFontSize,
    this.bottomMenuUnselectedFontSize,
    this.drawerFooter,
    this.sidebarTrailing,
    this.sidebarLeading,
    this.onCollapsedTap,
    this.onCollapsedTapDown,
    this.onSidebarChanged,
    this.onTopbarChanged,
    this.sidebarActiveBorderRadius,
    this.endDrawerWidth,
  })  : assert(!(bottomNavigationBar != null && bottomNavigationBarItem != null)),
        super(key: key);

  @override
  State<PageSideBar> createState() => _PageSideBarState();
}

class _PageSideBarState extends State<PageSideBar> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    if (widget.scaffoldKey != null) {
      _scaffoldKey = widget.scaffoldKey!;
    }
    super.initState();
  }

  PreferredSizeWidget? _renderTopResponsive() {
    if (widget.hideTopbar) return null;
    return AppBar(
      elevation: widget.responsiveTopBarElevation ?? widget.topBarTheme?.elevation,
      backgroundColor: widget.topBarTheme?.color ?? widget.responsiveTopBarColor,
      title: widget.responsiveTopBar ?? widget.topBarLeading,
      leading: widget.responsiveLeading ??
          (widget.hideDrawer
              ? Container()
              : IconButton(
                  onPressed: () {
                    _scaffoldKey.currentState?.openDrawer();
                  },
                  icon: const Icon(
                    Icons.menu,
                    color: Colors.black12,
                  ),
                )),
      actions: widget.responsiveActions,
    );
  }

  Widget _renderTop() {
    if (widget.hideTopbar) return Container();
    return WidgetSize(
      onChange: (Size size) {
        if (widget.onTopbarChanged != null) {
          widget.onTopbarChanged!(size);
        }
      },
      child: TopBar(
        isAuth: widget.isAuth,
        height: widget.topBarTheme?.height,
        elevation: widget.topBarTheme?.elevation,
        decoration: widget.topBarTheme?.decoration,
        padding: widget.topBarTheme?.padding,
        color: widget.topBarTheme?.color,
        leading: widget.topBarLeading,
        trailing: widget.topBarTrailing,
        content: widget.topBarContent,
        spacing: widget.topBarSpacing,
      ),
    );
  }

  Widget _renderBody() {
    if (widget.hideSidebar) {
      return Padding(
        padding: widget.paddingContent ?? const EdgeInsets.all(10.0),
        child: widget.child ?? Container(),
      );
    }
    return Row(
      children: [
        if (!ResponsiveWidget.isSmallScreen(context))
          WidgetSize(
            onChange: (Size size) {
              if (widget.onSidebarChanged != null) {
                widget.onSidebarChanged!(size);
              }
            },
            child: SideBar(
              onCollapsedTap: widget.onCollapsedTap,
              onCollapsedTapDown: widget.onCollapsedTapDown,
              sidebarTrailing: widget.sidebarTrailing,
              sidebarLeading: widget.sidebarLeading,
              sidebarActiveMenuColor: widget.sidebarActiveMenuColor,
              sidebarActiveBorderRadius: widget.sidebarActiveBorderRadius,
              showLabelCollapsed: widget.showLabelCollapsed,
              collapsedWidth: widget.collapsedWidth,
              padding: widget.paddingMenu,
              menuModel: widget.sideBarMenu,
              isAuth: widget.isAuth,
              isCollapsed: widget.isCollapsed,
              expandedIconColor: widget.sidebarTheme?.expandedIconColor,
              collapsedIconColor: widget.sidebarTheme?.collapsedIconColor,
              backgroundColor: widget.sidebarTheme?.color,
              width: widget.sidebarTheme?.width,
              elevation: widget.sidebarTheme?.elevation,
              duration: widget.sidebarTheme?.duration,
              curve: widget.sidebarTheme?.curve,
            ),
          ),
        Expanded(
            child: Padding(
          padding: widget.paddingContent ?? const EdgeInsets.all(10.0),
          child: widget.child ?? Container(),
        ))
      ],
    );
  }

  Widget? renderBottomNavigation() {
    if (widget.bottomNavigationBarItem != null) {
      return BottomNavigationBar(
        selectedFontSize: widget.bottomMenuSelectedFontSize ?? 14,
        unselectedFontSize: widget.bottomMenuUnselectedFontSize ?? 14,
        onTap: widget.bottomNavigationBarOnTap == null ? null : (index) => widget.bottomNavigationBarOnTap!(index),
        currentIndex: widget.bottomNavigationBarIndex ?? 0,
        items: widget.bottomNavigationBarItem!,
      );
    }
    return null;
  }

  Widget body() => Title(
        key: GlobalKey<ScaffoldState>(),
        color: ThemeData().primaryColor,
        title: widget.pageTitle ?? "",
        child: Scaffold(
          key: _scaffoldKey,
          floatingActionButtonLocation: widget.floatingActionButtonLocation,
          floatingActionButton: widget.floatingActionButton,
          floatingActionButtonAnimator: widget.floatingActionButtonAnimator,
          appBar: ResponsiveWidget.isSmallScreen(context) ? _renderTopResponsive() : null,
          endDrawer: widget.endDrawer == null
              ? null
              : AnimatedContainer(
                  duration: const Duration(milliseconds: 300),
                  curve: Curves.easeOut,
                  width: widget.endDrawerWidth,
                  child: Drawer(
                    child: widget.endDrawer,
                  ),
                ),
          drawer: widget.hideDrawer
              ? null
              : DrawerMenu(
                  showHeader: widget.drawerShowHeader,
                  header: widget.topBarLeading,
                  beforeMenu: widget.drawerBeforeMenu,
                  footer: widget.drawerFooter,
                  padding: widget.paddingMenu,
                  backgroundColor: widget.drawerBackground ?? widget.sidebarTheme?.color,
                  menu: widget.sideBarMenu,
                ),
          bottomNavigationBar: widget.bottomNavigationBar ?? renderBottomNavigation(),
          body: Column(
            children: [
              if (!ResponsiveWidget.isSmallScreen(context)) _renderTop(),
              Expanded(child: _renderBody()),
            ],
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return widget.blogMode ? RobotDetector(child: body()) : body();
  }
}
