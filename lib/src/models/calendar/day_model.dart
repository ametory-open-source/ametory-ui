import 'event_model.dart';

class DayModel {
  final DateTime? date;
  final bool enabled;
  final List<EventModel>? events;
  const DayModel({this.date, this.events, this.enabled = true});
  DayModel copyWith(
      {DateTime? date, List<EventModel>? events, List<EventModel>? holidays}) {
    return DayModel(
      date: date ?? this.date,
      events: events ?? this.events,
      enabled: enabled,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'date': date,
      'enabled': enabled,
      'events':
          events?.map<Map<String, dynamic>>((data) => data.toJson()).toList(),
    };
  }

  static DayModel fromJson(Map<String, dynamic> json) {
    return DayModel(
        date: json['date'],
        events: json['events'] == null
            ? null
            : (json['events'] as List)
                .map<EventModel>(
                    (data) => EventModel.fromJson(data as Map<String, dynamic>))
                .toList());
  }

  @override
  String toString() {
    return '''DayModel(
                date:$date,
                enabled:$enabled,
events:${events.toString()},
}
    ) ''';
  }
}
