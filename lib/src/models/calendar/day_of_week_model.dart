class DayOfWeekModel {
  String? name;
  String? abbreviation;
  bool? isWeekend;

  DayOfWeekModel({this.name, this.abbreviation, this.isWeekend});

  DayOfWeekModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    abbreviation = json['abbreviation'];
    isWeekend = json['is_weekend'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['abbreviation'] = abbreviation;
    data['is_weekend'] = isWeekend;
    return data;
  }
}
