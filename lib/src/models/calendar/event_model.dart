class EventModel {
  final String? name;
  final String? description;
  final String? location;
  final DateTime? startDate;
  final DateTime? endDate;
  final Map<String, dynamic>? data;
  final bool isHoliday;
  const EventModel(
      {this.name,
      this.description,
      this.startDate,
      this.location,
      this.endDate,
      this.data,
      this.isHoliday = false});
  EventModel copyWith(
      {String? name,
      String? description,
      String? location,
      DateTime? startDate,
      DateTime? endDate,
      bool isHoliday = false,
      Map<String, dynamic>? data}) {
    return EventModel(
        name: name ?? this.name,
        description: description ?? this.description,
        location: location ?? this.location,
        startDate: startDate ?? this.startDate,
        endDate: endDate ?? this.endDate,
        isHoliday: isHoliday,
        data: data ?? this.data);
  }

  Map<String, Object?> toJson() {
    return {
      'name': name,
      'description': description,
      'location': location,
      'startDate': startDate,
      'endDate': endDate,
      'isHoliday': isHoliday,
      'data': data
    };
  }

  static EventModel fromJson(Map<String, dynamic> json) {
    return EventModel(
        name: json['name'],
        description: json['description'],
        location: json['location'],
        isHoliday: json["isHoliday"],
        startDate: json['startDate'],
        endDate: json['endDate'],
        data: json['data']);
  }

  @override
  String toString() {
    return '''EventModel(
                name:$name,
description:$description,
isHoliday:$isHoliday,
startDate:$startDate,
endDate:$endDate,
data:$data
    ) ''';
  }
}
