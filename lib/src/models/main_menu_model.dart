import 'package:ametory_ui/src/models/menu_model.dart';
import 'package:flutter/material.dart';

class MainMenuModel implements MenuModel {
  @override
  List<Widget>? actions;

  @override
  Widget? icon;

  @override
  Widget? menu;

  @override
  Function()? onTap;

  EdgeInsets? padding;

  @override
  String? tooltip;

  @override
  Widget? label;

  bool isActive;

  @override
  List<MenuModel>? children;

  MainMenuModel({this.actions, this.icon, this.menu, this.onTap, this.padding, this.children, this.isActive = false, this.label, this.tooltip});
}
