import 'package:flutter/material.dart';

abstract class MenuModel {
  Widget? icon;
  Widget? menu;
  String? tooltip;
  Widget? label;
  List<Widget>? actions;
  List<MenuModel>? children;

  Function()? onTap;
}
