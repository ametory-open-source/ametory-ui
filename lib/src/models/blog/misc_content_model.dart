import 'package:cloud_firestore/cloud_firestore.dart';

class MiscContent {
  late CollectionReference<MiscContent> _fbref;
  String collectionName = "misc_contents";
  String? id;
  String? title;
  String? subtitle;
  String? description;
  int? order;
  String? cover;
  String? url;

  late String miscContentName;
  late String miscId;

  MiscContent({
    this.id,
    this.title,
    this.subtitle,
    this.description,
    this.order,
    this.cover,
    this.url,
    this.miscContentName = "misc",
    required this.miscId,
  }) {
    _fbref = FirebaseFirestore.instance.collection(miscContentName).doc(miscId).collection(collectionName).withConverter(
          fromFirestore: (snapshot, _) => MiscContent.fromJson(snapshot.data()!),
          toFirestore: (MiscContent data, _) => data.toJson(),
        );
  }

  MiscContent.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    subtitle = json['subtitle'];
    description = json['description'];
    order = json['order'] ?? 0;
    cover = json['cover'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['subtitle'] = subtitle;
    data['description'] = description;
    data['order'] = order ?? 0;
    data['cover'] = cover;
    data['url'] = url;
    return data;
  }

  Future<QuerySnapshot<MiscContent>> firebaseGet({
    String orderBy = "created",
    bool descending = true,
    int limit = 10,
    DocumentSnapshot<MiscContent>? startAfterDoc,
    DocumentSnapshot<MiscContent>? startAtDoc,
    DocumentSnapshot<MiscContent>? endAtDoc,
    DocumentSnapshot<MiscContent>? endBeforeDoc,
  }) async {
    Query<MiscContent> query = _fbref.orderBy(orderBy, descending: descending);
    var numOfNull = 0;
    if (startAfterDoc != null) {
      query = query.startAfterDocument(startAfterDoc);
      numOfNull++;
    }
    if (startAtDoc != null) {
      query = query.startAtDocument(startAtDoc);
      numOfNull++;
    }
    if (endBeforeDoc != null) {
      query = query.endBeforeDocument(endBeforeDoc);
      numOfNull++;
    }
    if (endAtDoc != null) {
      query = query.endAtDocument(endAtDoc);
      numOfNull++;
    }

    assert(numOfNull < 2, "you cannot use [startAfterDoc, startAtDoc, endAtDoc, endBeforeDoc] in the same time");
    return await query.get();
  }

  Future<void> firebaseSet() {
    return _fbref.doc(id).set(this);
  }

  Future<void> firebaseUpdate(Map<String, dynamic> data) async {
    return await _fbref.doc(id).update(data);
  }

  Future<void> firebaseDelete() async {
    return await _fbref.doc(id).delete();
  }

  Future<MiscContent?> firebaseDoc() async {
    final data = await _fbref.doc(id).get();
    if (data.exists) {
      return data.data();
    }
    return null;
  }

  DocumentReference<MiscContent> firebaseDocRef() {
    return _fbref.doc(id);
  }

  CollectionReference<MiscContent> firebaseColRef() {
    return _fbref;
  }
}
