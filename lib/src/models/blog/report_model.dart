import 'package:ametory_ui/ametory_ui.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class BlogReportModel {
  late CollectionReference<BlogReportModel> _fbref;
  String collectionName = "reports";
  String? id;
  String? refId;
  String? type;
  String? message;
  DateTime? created;
  UserModel? user;

  BlogReportModel({this.id, this.refId, this.type, this.message, this.created, this.user}) {
    _fbref = FirebaseFirestore.instance.collection(collectionName).withConverter(
          fromFirestore: (snapshot, _) => BlogReportModel.fromJson(snapshot.data()!),
          toFirestore: (BlogReportModel data, _) => data.toJson(),
        );
  }

  BlogReportModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    refId = json['refId'];
    type = json['type'];
    message = json['message'];
    if (json['created'] != null) created = json['created'].toDate();
    user = json['user'] != null ? UserModel.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['refId'] = refId;
    data['type'] = type;
    data['message'] = message;
    if (created != null) data['created'] = Timestamp.fromDate(created!);
    if (user != null) {
      data['user'] = user!.toJson();
    }
    return data;
  }

  Future<QuerySnapshot<BlogReportModel>> firebaseGet({
    String orderBy = "created",
    bool descending = true,
    int limit = 10,
    DocumentSnapshot<BlogReportModel>? startAfterDoc,
    DocumentSnapshot<BlogReportModel>? startAtDoc,
    DocumentSnapshot<BlogReportModel>? endAtDoc,
    DocumentSnapshot<BlogReportModel>? endBeforeDoc,
  }) async {
    Query<BlogReportModel> query = _fbref.orderBy(orderBy, descending: descending);
    var numOfNull = 0;
    if (startAfterDoc != null) {
      query = query.startAfterDocument(startAfterDoc);
      numOfNull++;
    }
    if (startAtDoc != null) {
      query = query.startAtDocument(startAtDoc);
      numOfNull++;
    }
    if (endBeforeDoc != null) {
      query = query.endBeforeDocument(endBeforeDoc);
      numOfNull++;
    }
    if (endAtDoc != null) {
      query = query.endAtDocument(endAtDoc);
      numOfNull++;
    }

    assert(numOfNull < 2, "you cannot use [startAfterDoc, startAtDoc, endAtDoc, endBeforeDoc] in the same time");
    return await query.get();
  }

  Future<void> firebaseSet() {
    return _fbref.doc(id).set(this);
  }

  Future<void> firebaseUpdate(Map<String, dynamic> data) async {
    return await _fbref.doc(id).update(data);
  }

  Future<void> firebaseDelete() async {
    return await _fbref.doc(id).delete();
  }

  Future<BlogReportModel?> firebaseDoc() async {
    final data = await _fbref.doc(id).get();
    if (data.exists) {
      return data.data();
    }
    return null;
  }

  DocumentReference<BlogReportModel> firebaseDocRef() {
    return _fbref.doc(id);
  }

  CollectionReference<BlogReportModel> firebaseColRef() {
    return _fbref;
  }
}
