import 'package:ametory_ui/ametory_ui.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AcademyModel {
  late CollectionReference<AcademyModel> _fbref;
  String collectionName = "academies";
  String? id;
  String? title;
  String? permalink;
  String? description;
  String? image;
  String? imageCaption;
  DateTime? modified;
  DateTime? created;
  int? totalContents;
  String? userId;
  UserModel? user;
  List<String>? tags;
  List<String>? keywords;
  bool? published;
  List<AcademyContent>? contents;
  AcademyEnrollmentMode? enrollmentMode;
  AcademyRank? rank;

  AcademyModel(
      {this.id,
      this.title,
      this.permalink,
      this.description,
      this.image,
      this.imageCaption,
      this.modified,
      this.created,
      this.totalContents,
      this.userId,
      this.user,
      this.tags,
      this.keywords,
      this.enrollmentMode,
      this.rank,
      this.published,
      this.contents}) {
    _fbref = FirebaseFirestore.instance.collection(collectionName).withConverter(
          fromFirestore: (snapshot, _) => AcademyModel.fromJson(snapshot.data()!),
          toFirestore: (AcademyModel data, _) => data.toJson(),
        );
  }

  AcademyModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    permalink = json['permalink'];
    description = json['description'];
    image = json['image'];
    imageCaption = json['imageCaption'];
    if (json['enrollmentMode'] != null) {
      switch (json['enrollmentMode']) {
        case "free":
          enrollmentMode = AcademyEnrollmentMode.free;
          break;
        case "partialFree":
          enrollmentMode = AcademyEnrollmentMode.partialFree;
          break;
        default:
          enrollmentMode = AcademyEnrollmentMode.paid;
      }
    }
    if (json['rank'] != null) {
      switch (json['rank']) {
        case "basic":
          rank = AcademyRank.basic;
          break;
        case "intermediate":
          rank = AcademyRank.intermediate;
          break;
        default:
          rank = AcademyRank.advance;
      }
    }
    if (json['created'] != null) created = json['created'].toDate();
    if (json['modified'] != null) {
      modified = json['modified'].toDate();
    }
    totalContents = json['totalContents'] ?? 0;
    userId = json['userId'];
    if (json['user'] != null) {
      user = UserModel.fromJson(json['user']);
      userId = user!.id;
    } else {
      user = null;
    }
    if (json['tags'] != null) {
      tags = [];
      json['tags'].forEach((e) {
        tags!.add(e.toString());
      });
    }
    if (json['keywords'] != null) {
      keywords = [];
      json['keywords'].forEach((e) {
        keywords!.add(e.toString());
      });
    }
    published = json['published'];
    // if (json['contents'] != null) {
    //   contents = <AcademyContent>[];
    //   json['contents'].forEach((v) {
    //     contents!.add(AcademyContent.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['permalink'] = permalink;
    data['description'] = description;
    data['image'] = image;
    data['imageCaption'] = imageCaption;
    if (enrollmentMode != null) data['enrollmentMode'] = enrollmentMode!.toShortString();
    if (rank != null) data['rank'] = rank!.toShortString();
    if (modified != null) data['modified'] = Timestamp.fromDate(modified!);
    if (created != null) data['created'] = Timestamp.fromDate(created!);
    data['totalContents'] = totalContents ?? 0;
    data['userId'] = userId;
    if (user != null) {
      data['user'] = user!.toJson();
      data['userId'] = user!.id;
    }
    data['tags'] = tags ?? [];
    data['keywords'] = keywords ?? [];
    data['published'] = published;
    // if (contents != null) {
    //   data['contents'] = contents!.map((v) => v.toJson()).toList();
    // }
    return data;
  }

  Future<QuerySnapshot<AcademyModel>> firebaseGet({
    String orderBy = "created",
    bool descending = true,
    int limit = 10,
    DocumentSnapshot<AcademyModel>? startAfterDoc,
    DocumentSnapshot<AcademyModel>? startAtDoc,
    DocumentSnapshot<AcademyModel>? endAtDoc,
    DocumentSnapshot<AcademyModel>? endBeforeDoc,
  }) async {
    Query<AcademyModel> query = _fbref.orderBy(orderBy, descending: descending);
    var numOfNull = 0;
    if (startAfterDoc != null) {
      query = query.startAfterDocument(startAfterDoc);
      numOfNull++;
    }
    if (startAtDoc != null) {
      query = query.startAtDocument(startAtDoc);
      numOfNull++;
    }
    if (endBeforeDoc != null) {
      query = query.endBeforeDocument(endBeforeDoc);
      numOfNull++;
    }
    if (endAtDoc != null) {
      query = query.endAtDocument(endAtDoc);
      numOfNull++;
    }

    assert(numOfNull < 2, "you cannot use [startAfterDoc, startAtDoc, endAtDoc, endBeforeDoc] in the same time");
    return await query.get();
  }

  Future<void> firebaseSet() {
    return _fbref.doc(id).set(this);
  }

  Future<void> firebaseUpdate(Map<String, dynamic> data) async {
    return await _fbref.doc(id).update(data);
  }

  Future<void> firebaseDelete() async {
    return await _fbref.doc(id).delete();
  }

  Future<AcademyModel?> firebaseDoc() async {
    final data = await _fbref.doc(id).get();
    if (data.exists) {
      return data.data();
    }
    return null;
  }

  DocumentReference<AcademyModel> firebaseDocRef() {
    return _fbref.doc(id);
  }

  CollectionReference<AcademyModel> firebaseColRef() {
    return _fbref;
  }
}

enum AcademyEnrollmentMode {
  free,
  partialFree,
  paid,
}

enum AcademyRank {
  basic,
  intermediate,
  advance,
}

extension ParseToString on AcademyEnrollmentMode {
  String toShortString() {
    return toString().split('.').last;
  }
}

extension ParseToString2 on AcademyRank {
  String toShortString() {
    return toString().split('.').last;
  }
}
