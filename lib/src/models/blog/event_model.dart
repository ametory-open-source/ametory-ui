import 'package:cloud_firestore/cloud_firestore.dart';

import '../../../ametory_ui.dart';

class BlogEventModel {
  late CollectionReference<BlogEventModel> _fbref;
  String collectionName = "events";
  String? id;
  String? title;
  String? description;
  String? excerpt;
  String? permalink;
  String? coverCaption;
  List<String>? keywords;
  bool? published;
  double? latitude;
  double? longitude;
  String? onlineUrl;
  String? offlineLocation;
  List<String>? tags;

  UserModel? user;
  String? userId;
  String? cover;
  DateTime? startTime;
  DateTime? endTime;
  DateTime? modified;
  DateTime? created;
  List<UserModel>? organizers;
  List<UserModel>? members;
  List<ScheduleModel>? schedules;
  List<PhotoModel>? photos;

  BlogEventModel(
      {this.id,
      this.title,
      this.description,
      this.excerpt,
      this.permalink,
      this.keywords,
      this.tags,
      this.published,
      this.coverCaption,
      this.cover,
      this.user,
      this.userId,
      this.startTime,
      this.endTime,
      this.organizers,
      this.members,
      this.schedules,
      this.photos,
      this.latitude,
      this.longitude,
      this.onlineUrl,
      this.offlineLocation,
      this.created,
      this.modified}) {
    _fbref = FirebaseFirestore.instance.collection(collectionName).withConverter(
          fromFirestore: (snapshot, _) => BlogEventModel.fromJson(snapshot.data()!),
          toFirestore: (BlogEventModel data, _) => data.toJson(),
        );
  }

  BlogEventModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    description = json['description'];
    excerpt = json['excerpt'];
    permalink = json['permalink'];
    coverCaption = json['coverCaption'];
    cover = json['cover'];
    userId = json['userId'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    onlineUrl = json['onlineUrl'];
    offlineLocation = json['offlineLocation'];
    published = json['published'] ?? false;
    user = json['user'] != null ? UserModel.fromJson(json['user']) : null;
    if (json['start_time'] != null) {
      startTime = json['start_time'].toDate();
    }
    if (json['end_time'] != null) {
      endTime = json['end_time'].toDate();
    }
    if (json['organizers'] != null) {
      organizers = <UserModel>[];
      json['organizers'].forEach((v) {
        organizers!.add(UserModel.fromJson(v));
      });
    }
    if (json['members'] != null) {
      members = <UserModel>[];
      json['members'].forEach((v) {
        members!.add(UserModel.fromJson(v));
      });
    }
    if (json['schedules'] != null) {
      schedules = <ScheduleModel>[];
      json['schedules'].forEach((v) {
        schedules!.add(ScheduleModel.fromJson(v));
      });
    }
    if (json['photos'] != null) {
      photos = <PhotoModel>[];
      json['photos'].forEach((v) {
        photos!.add(PhotoModel.fromJson(v));
      });
    }
    if (json['created'] != null) created = json['created'].toDate();
    if (json['modified'] != null) {
      modified = json['modified'].toDate();
    }
    if (json['tags'] != null) {
      tags = [];
      json['tags'].forEach((e) {
        tags!.add(e.toString());
      });
    }
    if (json['keywords'] != null) {
      keywords = [];
      json['keywords'].forEach((e) {
        keywords!.add(e.toString());
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['description'] = description;
    data['userId'] = userId;
    if (user != null) {
      data['userId'] ??= user!.id;
      data['user'] = user!.toJson();
    }
    data['excerpt'] = excerpt;
    data['permalink'] = permalink;
    data['cover'] = cover;
    data['latitude'] = latitude;
    data['longitude'] = longitude;
    data['onlineUrl'] = onlineUrl;
    data['offlineLocation'] = offlineLocation;
    data['coverCaption'] = coverCaption;
    data['published'] = published ?? false;
    if (startTime != null) {
      data['start_time'] = Timestamp.fromDate(startTime!);
    }
    if (endTime != null) {
      data['end_time'] = Timestamp.fromDate(endTime!);
    }
    if (organizers != null) {
      data['organizers'] = organizers!.map((v) => v.toJson()).toList();
    }
    if (members != null) {
      data['members'] = members!.map((v) => v.toJson()).toList();
    }
    if (schedules != null) {
      data['schedules'] = schedules!.map((v) => v.toJson()).toList();
    }
    if (photos != null) {
      data['photos'] = photos!.map((v) => v.toJson()).toList();
    }
    if (modified != null) data['modified'] = Timestamp.fromDate(modified!);
    if (created != null) data['created'] = Timestamp.fromDate(created!);
    data['keywords'] = keywords ?? [];
    data['tags'] = tags ?? [];
    return data;
  }

  Future<QuerySnapshot<BlogEventModel>> firebaseGet({
    String orderBy = "created",
    bool descending = true,
    int limit = 10,
    DocumentSnapshot<BlogEventModel>? startAfterDoc,
    DocumentSnapshot<BlogEventModel>? startAtDoc,
    DocumentSnapshot<BlogEventModel>? endAtDoc,
    DocumentSnapshot<BlogEventModel>? endBeforeDoc,
  }) async {
    Query<BlogEventModel> query = _fbref.orderBy(orderBy, descending: descending);
    var numOfNull = 0;
    if (startAfterDoc != null) {
      query = query.startAfterDocument(startAfterDoc);
      numOfNull++;
    }
    if (startAtDoc != null) {
      query = query.startAtDocument(startAtDoc);
      numOfNull++;
    }
    if (endBeforeDoc != null) {
      query = query.endBeforeDocument(endBeforeDoc);
      numOfNull++;
    }
    if (endAtDoc != null) {
      query = query.endAtDocument(endAtDoc);
      numOfNull++;
    }

    assert(numOfNull < 2, "you cannot use [startAfterDoc, startAtDoc, endAtDoc, endBeforeDoc] in the same time");
    return await query.get();
  }

  Future<void> firebaseSet() {
    return _fbref.doc(id).set(this);
  }

  Future<void> firebaseUpdate(Map<String, dynamic> data) async {
    return await _fbref.doc(id).update(data);
  }

  Future<void> firebaseDelete() async {
    return await _fbref.doc(id).delete();
  }

  Future<BlogEventModel?> firebaseDoc() async {
    final data = await _fbref.doc(id).get();
    if (data.exists) {
      return data.data();
    }
    return null;
  }

  DocumentReference<BlogEventModel> firebaseDocRef() {
    return _fbref.doc(id);
  }

  CollectionReference<BlogEventModel> firebaseColRef() {
    return _fbref;
  }
}

class ScheduleModel {
  String? id;
  int? day;
  String? startTime;
  String? endTime;
  String? description;

  ScheduleModel({this.id, this.day, this.startTime, this.endTime, this.description});

  ScheduleModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    day = json['day'];
    if (json['start_time'] != null) {
      startTime = json['start_time'];
    }
    if (json['end_time'] != null) {
      endTime = json['end_time'];
    }
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['day'] = day;
    if (startTime != null) {
      data['start_time'] = startTime;
    }
    if (endTime != null) {
      data['end_time'] = endTime;
    }
    data['description'] = description;
    return data;
  }
}

class PhotoModel {
  String? id;
  String? url;
  String? caption;

  PhotoModel({this.id, this.url, this.caption});

  PhotoModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    url = json['url'];
    caption = json['caption'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['url'] = url;
    data['caption'] = caption;
    return data;
  }
}
