import 'package:ametory_ui/ametory_ui.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class PublicationModel {
  late CollectionReference<PublicationModel> _fbref;
  String collectionName = "publications";
  String? id;
  String? title;
  String? permalink;
  String? description;
  String? image;
  String? imageCaption;
  DateTime? modified;
  DateTime? created;
  int? totalContents;
  String? userId;
  UserModel? user;
  List<String>? tags;
  List<String>? keywords;
  bool? published;
  List<PublicationContent>? contents;

  PublicationModel(
      {this.id,
      this.title,
      this.permalink,
      this.description,
      this.image,
      this.imageCaption,
      this.modified,
      this.created,
      this.totalContents,
      this.userId,
      this.user,
      this.tags,
      this.keywords,
      this.published,
      this.contents}) {
    _fbref = FirebaseFirestore.instance.collection(collectionName).withConverter(
          fromFirestore: (snapshot, _) => PublicationModel.fromJson(snapshot.data()!),
          toFirestore: (PublicationModel data, _) => data.toJson(),
        );
  }

  PublicationModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    permalink = json['permalink'];
    description = json['description'];
    image = json['image'];
    imageCaption = json['imageCaption'];
    if (json['created'] != null) created = json['created'].toDate();
    if (json['modified'] != null) {
      modified = json['modified'].toDate();
    }
    totalContents = json['totalContents'] ?? 0;
    userId = json['userId'];
    if (json['user'] != null) {
      user = UserModel.fromJson(json['user']);
      userId = user!.id;
    } else {
      user = null;
    }
    if (json['tags'] != null) {
      tags = [];
      json['tags'].forEach((e) {
        tags!.add(e.toString());
      });
    }
    if (json['keywords'] != null) {
      keywords = [];
      json['keywords'].forEach((e) {
        keywords!.add(e.toString());
      });
    }
    published = json['published'];
    // if (json['contents'] != null) {
    //   contents = <PublicationContent>[];
    //   json['contents'].forEach((v) {
    //     contents!.add(PublicationContent.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['permalink'] = permalink;
    data['description'] = description;
    data['image'] = image;
    data['imageCaption'] = imageCaption;
    if (modified != null) data['modified'] = Timestamp.fromDate(modified!);
    if (created != null) data['created'] = Timestamp.fromDate(created!);
    data['totalContents'] = totalContents ?? 0;
    data['userId'] = userId;
    if (user != null) {
      data['user'] = user!.toJson();
      data['userId'] = user!.id;
    }
    data['tags'] = tags ?? [];
    data['keywords'] = keywords ?? [];
    data['published'] = published;
    // if (contents != null) {
    //   data['contents'] = contents!.map((v) => v.toJson()).toList();
    // }
    return data;
  }

  Future<QuerySnapshot<PublicationModel>> firebaseGet({
    String orderBy = "created",
    bool descending = true,
    int limit = 10,
    DocumentSnapshot<PublicationModel>? startAfterDoc,
    DocumentSnapshot<PublicationModel>? startAtDoc,
    DocumentSnapshot<PublicationModel>? endAtDoc,
    DocumentSnapshot<PublicationModel>? endBeforeDoc,
    List<Query<PublicationContent>>? queries,
  }) async {
    Query<PublicationModel> query = _fbref.orderBy(orderBy, descending: descending);
    var numOfNull = 0;
    if (startAfterDoc != null) {
      query = query.startAfterDocument(startAfterDoc);
      numOfNull++;
    }
    if (startAtDoc != null) {
      query = query.startAtDocument(startAtDoc);
      numOfNull++;
    }
    if (endBeforeDoc != null) {
      query = query.endBeforeDocument(endBeforeDoc);
      numOfNull++;
    }
    if (endAtDoc != null) {
      query = query.endAtDocument(endAtDoc);
      numOfNull++;
    }

    assert(numOfNull < 2, "you cannot use [startAfterDoc, startAtDoc, endAtDoc, endBeforeDoc] in the same time");
    return await query.get();
  }

  Future<void> firebaseSet() {
    return _fbref.doc(id).set(this);
  }

  Future<void> firebaseUpdate(Map<String, dynamic> data) async {
    return await _fbref.doc(id).update(data);
  }

  Future<void> firebaseDelete() async {
    return await _fbref.doc(id).delete();
  }

  Future<PublicationModel?> firebaseDoc() async {
    final data = await _fbref.doc(id).get();
    if (data.exists) {
      return data.data();
    }
    return null;
  }

  DocumentReference<PublicationModel> firebaseDocRef() {
    return _fbref.doc(id);
  }

  CollectionReference<PublicationModel> firebaseColRef() {
    return _fbref;
  }
}
