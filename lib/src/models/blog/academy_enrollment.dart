import 'package:ametory_ui/ametory_ui.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AcademyEnrollment {
  late CollectionReference<AcademyEnrollment> _fbref;
  String collectionName = "academy_enrollments";
  String? id;
  String? userId;
  UserModel? user;
  int? order;
  bool? locked;

  late String academyEnrollmentName;
  late String academyId;

  AcademyEnrollment({this.id, this.userId, this.user, this.academyEnrollmentName = "academies", required this.academyId}) {
    _fbref = FirebaseFirestore.instance.collection(academyEnrollmentName).doc(academyId).collection(collectionName).withConverter(
          fromFirestore: (snapshot, _) => AcademyEnrollment.fromJson(snapshot.data()!),
          toFirestore: (AcademyEnrollment data, _) => data.toJson(),
        );
  }

  AcademyEnrollment.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['userId'];
    if (json['user'] != null) {
      user = UserModel.fromJson(json['user']);
      userId = user!.id;
    } else {
      user = null;
    }
    order = json['order'] ?? 0;
    locked = json['locked'] ?? false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['userId'] = userId;
    if (user != null) {
      data['user'] = user!.toJson();
      data['userId'] = user!.id;
    }
    data['order'] = order ?? 0;
    data['locked'] = locked ?? false;
    return data;
  }

  Future<QuerySnapshot<AcademyEnrollment>> firebaseGet({
    String orderBy = "created",
    bool descending = true,
    int limit = 10,
    DocumentSnapshot<AcademyEnrollment>? startAfterDoc,
    DocumentSnapshot<AcademyEnrollment>? startAtDoc,
    DocumentSnapshot<AcademyEnrollment>? endAtDoc,
    DocumentSnapshot<AcademyEnrollment>? endBeforeDoc,
  }) async {
    Query<AcademyEnrollment> query = _fbref.orderBy(orderBy, descending: descending);
    var numOfNull = 0;
    if (startAfterDoc != null) {
      query = query.startAfterDocument(startAfterDoc);
      numOfNull++;
    }
    if (startAtDoc != null) {
      query = query.startAtDocument(startAtDoc);
      numOfNull++;
    }
    if (endBeforeDoc != null) {
      query = query.endBeforeDocument(endBeforeDoc);
      numOfNull++;
    }
    if (endAtDoc != null) {
      query = query.endAtDocument(endAtDoc);
      numOfNull++;
    }

    assert(numOfNull < 2, "you cannot use [startAfterDoc, startAtDoc, endAtDoc, endBeforeDoc] in the same time");
    return await query.get();
  }

  Future<void> firebaseSet() {
    return _fbref.doc(id).set(this);
  }

  Future<void> firebaseUpdate(Map<String, dynamic> data) async {
    return await _fbref.doc(id).update(data);
  }

  Future<void> firebaseDelete() async {
    return await _fbref.doc(id).delete();
  }

  Future<AcademyEnrollment?> firebaseDoc() async {
    final data = await _fbref.doc(id).get();
    if (data.exists) {
      return data.data();
    }
    return null;
  }

  DocumentReference<AcademyEnrollment> firebaseDocRef() {
    return _fbref.doc(id);
  }

  CollectionReference<AcademyEnrollment> firebaseColRef() {
    return _fbref;
  }
}
