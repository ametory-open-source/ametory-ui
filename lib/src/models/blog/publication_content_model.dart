import 'package:cloud_firestore/cloud_firestore.dart';

class PublicationContent {
  late CollectionReference<PublicationContent> _fbref;
  String collectionName = "contents";
  String? id;
  String? articleId;
  String? articleTitle;
  int? order;
  String? prevArticleId;
  String? prevArticleTitle;
  String? nextArticleId;
  String? nextArticleTitle;
  late String publicationCollectionName;
  late String publicationId;

  PublicationContent(
      {this.id,
      this.articleId,
      this.articleTitle,
      this.order,
      this.prevArticleId,
      this.prevArticleTitle,
      this.nextArticleId,
      this.nextArticleTitle,
      this.publicationCollectionName = "publications",
      required this.publicationId}) {
    _fbref = FirebaseFirestore.instance.collection(publicationCollectionName).doc(publicationId).collection(collectionName).withConverter(
          fromFirestore: (snapshot, _) => PublicationContent.fromJson(snapshot.data()!),
          toFirestore: (PublicationContent data, _) => data.toJson(),
        );
  }

  PublicationContent.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    articleId = json['articleId'];
    articleTitle = json['articleTitle'];
    order = json['order'] ?? 0;
    prevArticleId = json['prevArticleId'];
    prevArticleTitle = json['prevArticleTitle'];
    nextArticleId = json['nextArticleId'];
    nextArticleTitle = json['nextArticleTitle'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['articleId'] = articleId;
    data['articleTitle'] = articleTitle;
    data['order'] = order ?? 0;
    data['prevArticleId'] = prevArticleId;
    data['prevArticleTitle'] = prevArticleTitle;
    data['nextArticleId'] = nextArticleId;
    data['nextArticleTitle'] = nextArticleTitle;
    return data;
  }

  Future<QuerySnapshot<PublicationContent>> firebaseGet({
    String orderBy = "order",
    bool descending = false,
    int limit = 10,
    DocumentSnapshot<PublicationContent>? startAfterDoc,
    DocumentSnapshot<PublicationContent>? startAtDoc,
    DocumentSnapshot<PublicationContent>? endAtDoc,
    DocumentSnapshot<PublicationContent>? endBeforeDoc,
  }) async {
    Query<PublicationContent> query = _fbref.orderBy(orderBy, descending: descending);
    var numOfNull = 0;
    if (startAfterDoc != null) {
      query = query.startAfterDocument(startAfterDoc);
      numOfNull++;
    }
    if (startAtDoc != null) {
      query = query.startAtDocument(startAtDoc);
      numOfNull++;
    }
    if (endBeforeDoc != null) {
      query = query.endBeforeDocument(endBeforeDoc);
      numOfNull++;
    }
    if (endAtDoc != null) {
      query = query.endAtDocument(endAtDoc);
      numOfNull++;
    }

    assert(numOfNull < 2, "you cannot use [startAfterDoc, startAtDoc, endAtDoc, endBeforeDoc] in the same time");
    return await query.get();
  }

  Future<void> firebaseSet() {
    return _fbref.doc(id).set(this);
  }

  Future<void> firebaseUpdate(Map<String, dynamic> data) async {
    return await _fbref.doc(id).update(data);
  }

  Future<void> firebaseDelete() async {
    return await _fbref.doc(id).delete();
  }

  Future<PublicationContent?> firebaseDoc() async {
    final data = await _fbref.doc(id).get();
    if (data.exists) {
      return data.data();
    }
    return null;
  }

  DocumentReference<PublicationContent> firebaseDocRef() {
    return _fbref.doc(id);
  }

  CollectionReference<PublicationContent> firebaseColRef() {
    return _fbref;
  }
}
