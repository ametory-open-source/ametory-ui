import 'package:cloud_firestore/cloud_firestore.dart';

import '../../../ametory_ui.dart';

class BlogModel {
  late CollectionReference<BlogModel> _fbref;
  String collectionName = "blogs";

  String? id;
  String? userId;
  UserModel? user;
  String? title;
  String? excerpt;
  String? description;
  String? permalink;
  DateTime? modified;
  DateTime? created;
  String? image;
  String? imageCaption;
  bool? published;
  bool? commentActive;
  List<String>? tags;
  List<String>? keywords;
  int? totalLikes;
  int? totalComments;
  int? totalRead;

  BlogModel(
      {this.id,
      this.userId,
      this.user,
      this.title,
      this.excerpt,
      this.description,
      this.modified,
      this.created,
      this.permalink,
      this.image,
      this.imageCaption,
      this.published,
      this.commentActive,
      this.tags,
      this.totalLikes,
      this.totalComments,
      this.totalRead,
      this.keywords}) {
    _fbref = FirebaseFirestore.instance.collection(collectionName).withConverter(
          fromFirestore: (snapshot, _) => BlogModel.fromJson(snapshot.data()!),
          toFirestore: (BlogModel data, _) => data.toJson(),
        );
  }

  BlogModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['userId'];
    user = json['user'] != null ? UserModel.fromJson(json['user']) : null;
    title = json['title'];
    excerpt = json['excerpt'];
    description = json['description'];
    permalink = json['permalink'];
    published = json['published'] ?? false;
    commentActive = json['commentActive'] ?? false;
    imageCaption = json['imageCaption'];
    totalComments = json['totalComments'] ?? 0;
    totalLikes = json['totalLikes'] ?? 0;
    totalRead = json['totalRead'] ?? 0;
    image = json['image'] ?? "";
    if (json['created'] != null) created = json['created'].toDate();
    if (json['modified'] != null) {
      modified = json['modified'].toDate();
    }
    if (json['tags'] != null) {
      tags = [];
      json['tags'].forEach((e) {
        tags!.add(e.toString());
      });
    }
    if (json['keywords'] != null) {
      keywords = [];
      json['keywords'].forEach((e) {
        keywords!.add(e.toString());
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['userId'] = userId;
    if (user != null) {
      data['userId'] ??= user!.id;
      data['user'] = user!.toJson();
    }
    data['title'] = title;
    data['excerpt'] = excerpt;
    data['description'] = description;
    data['permalink'] = permalink;
    data['imageCaption'] = imageCaption;
    data['totalLikes'] = totalLikes ?? 0;
    data['totalComments'] = totalComments ?? 0;
    data['totalRead'] = totalRead ?? 0;
    if (modified != null) data['modified'] = Timestamp.fromDate(modified!);
    if (created != null) data['created'] = Timestamp.fromDate(created!);
    data['image'] = image ?? "";
    data['published'] = published ?? false;
    data['commentActive'] = commentActive ?? false;
    data['tags'] = tags ?? [];
    data['keywords'] = keywords ?? [];
    return data;
  }

  Future<QuerySnapshot<BlogModel>> firebaseGet({
    String orderBy = "created",
    bool descending = true,
    int limit = 10,
    DocumentSnapshot<BlogModel>? startAfterDoc,
    DocumentSnapshot<BlogModel>? startAtDoc,
    DocumentSnapshot<BlogModel>? endAtDoc,
    DocumentSnapshot<BlogModel>? endBeforeDoc,
  }) async {
    Query<BlogModel> query = _fbref.orderBy(orderBy, descending: descending);
    var numOfNull = 0;
    if (startAfterDoc != null) {
      query = query.startAfterDocument(startAfterDoc);
      numOfNull++;
    }
    if (startAtDoc != null) {
      query = query.startAtDocument(startAtDoc);
      numOfNull++;
    }
    if (endBeforeDoc != null) {
      query = query.endBeforeDocument(endBeforeDoc);
      numOfNull++;
    }
    if (endAtDoc != null) {
      query = query.endAtDocument(endAtDoc);
      numOfNull++;
    }

    assert(numOfNull < 2, "you cannot use [startAfterDoc, startAtDoc, endAtDoc, endBeforeDoc] in the same time");
    return await query.get();
  }

  Future<void> firebaseSet() {
    return _fbref.doc(id).set(this);
  }

  Future<void> firebaseUpdate(Map<String, dynamic> data) async {
    return await _fbref.doc(id).update(data);
  }

  Future<void> firebaseDelete() async {
    return await _fbref.doc(id).delete();
  }

  Future<BlogModel?> firebaseDoc() async {
    final data = await _fbref.doc(id).get();
    if (data.exists) {
      return data.data();
    }
    return null;
  }

  DocumentReference<BlogModel> firebaseDocRef() {
    return _fbref.doc(id);
  }

  CollectionReference<BlogModel> firebaseColRef() {
    return _fbref;
  }
}
