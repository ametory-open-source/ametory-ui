import 'package:cloud_firestore/cloud_firestore.dart';

class AcademyContent {
  late CollectionReference<AcademyContent> _fbref;
  String collectionName = "academy_contents";
  String? id;
  String? title;
  String? description;
  int? order;
  String? icon;
  double? duration;
  bool? isLocked;

  late String academyCollectionName;
  late String academyId;

  AcademyContent(
      {this.id,
      this.title,
      this.description,
      this.order,
      this.icon,
      this.duration,
      this.academyCollectionName = "academies",
      required this.academyId,
      this.isLocked}) {
    _fbref = FirebaseFirestore.instance.collection(academyCollectionName).doc(academyId).collection(collectionName).withConverter(
          fromFirestore: (snapshot, _) => AcademyContent.fromJson(snapshot.data()!),
          toFirestore: (AcademyContent data, _) => data.toJson(),
        );
  }

  AcademyContent.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    description = json['description'];
    order = json['order'] ?? 0;
    icon = json['icon'];
    duration = json['duration'];
    isLocked = json['isLocked'] ?? false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['description'] = description;
    data['order'] = order ?? 0;
    data['icon'] = icon;
    data['duration'] = duration;
    data['isLocked'] = isLocked ?? false;
    return data;
  }

  Future<QuerySnapshot<AcademyContent>> firebaseGet({
    String orderBy = "created",
    bool descending = true,
    int limit = 10,
    DocumentSnapshot<AcademyContent>? startAfterDoc,
    DocumentSnapshot<AcademyContent>? startAtDoc,
    DocumentSnapshot<AcademyContent>? endAtDoc,
    DocumentSnapshot<AcademyContent>? endBeforeDoc,
  }) async {
    Query<AcademyContent> query = _fbref.orderBy(orderBy, descending: descending);
    var numOfNull = 0;
    if (startAfterDoc != null) {
      query = query.startAfterDocument(startAfterDoc);
      numOfNull++;
    }
    if (startAtDoc != null) {
      query = query.startAtDocument(startAtDoc);
      numOfNull++;
    }
    if (endBeforeDoc != null) {
      query = query.endBeforeDocument(endBeforeDoc);
      numOfNull++;
    }
    if (endAtDoc != null) {
      query = query.endAtDocument(endAtDoc);
      numOfNull++;
    }

    assert(numOfNull < 2, "you cannot use [startAfterDoc, startAtDoc, endAtDoc, endBeforeDoc] in the same time");
    return await query.get();
  }

  Future<void> firebaseSet() {
    return _fbref.doc(id).set(this);
  }

  Future<void> firebaseUpdate(Map<String, dynamic> data) async {
    return await _fbref.doc(id).update(data);
  }

  Future<void> firebaseDelete() async {
    return await _fbref.doc(id).delete();
  }

  Future<AcademyContent?> firebaseDoc() async {
    final data = await _fbref.doc(id).get();
    if (data.exists) {
      return data.data();
    }
    return null;
  }

  DocumentReference<AcademyContent> firebaseDocRef() {
    return _fbref.doc(id);
  }

  CollectionReference<AcademyContent> firebaseColRef() {
    return _fbref;
  }
}
