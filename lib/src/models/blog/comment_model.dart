import 'package:cloud_firestore/cloud_firestore.dart';

import '../../../ametory_ui.dart';

class CommentModel {
  late CollectionReference<CommentModel> _fbref;
  String collectionName = "comments";

  String? id;
  String? articleId;
  String? articleTitle;
  String? message;
  String? type;
  DateTime? created;
  DateTime? modified;
  String? userId;
  UserModel? user;
  int? totalReplies;
  int? totalReports;
  int? totalLikes;
  List<CommentModel>? replies;
  List<String>? keywords;
  bool? published;

  CommentModel({
    this.id,
    this.articleId,
    this.articleTitle,
    this.message,
    this.created,
    this.modified,
    this.user,
    this.userId,
    this.replies,
    this.totalReplies,
    this.totalReports,
    this.totalLikes,
    this.keywords,
    this.published,
  }) {
    _fbref = FirebaseFirestore.instance.collection(collectionName).withConverter(
          fromFirestore: (snapshot, _) => CommentModel.fromJson(snapshot.data()!),
          toFirestore: (CommentModel data, _) => data.toJson(),
        );
  }

  CommentModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    articleId = json['articleId'];
    articleTitle = json['articleTitle'] ?? "";
    if (json['created'] != null) created = json['created'].toDate();
    if (json['modified'] != null) {
      modified = json['modified'].toDate();
    }
    message = json['message'];
    userId = json['userId'];
    totalReplies = json['totalReplies'] ?? 0;
    totalReports = json['totalReports'] ?? 0;
    totalLikes = json['totalLikes'] ?? 0;
    published = json['published'] ?? true;
    type = json['type'] ?? "blog";
    user = json['user'] != null ? UserModel.fromJson(json['user']) : null;
    if (json['replies'] != null) {
      replies = <CommentModel>[];
      json['replies'].forEach((v) {
        replies!.add(CommentModel.fromJson(v));
      });
    }
    if (json['keywords'] != null) {
      keywords = [];
      json['keywords'].forEach((e) {
        keywords!.add(e.toString());
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['articleId'] = articleId;
    data['articleTitle'] = articleTitle ?? "";
    if (modified != null) data['modified'] = Timestamp.fromDate(modified!);
    if (created != null) data['created'] = Timestamp.fromDate(created!);
    data['type'] = type ?? "blog";
    data['message'] = message;
    data['userId'] = userId;
    data['totalReplies'] = totalReplies ?? 0;
    data['totalLikes'] = totalLikes ?? 0;
    data['totalReports'] = totalReports ?? 0;
    data['published'] = published ?? true;
    if (user != null) {
      data['userId'] ??= user!.id;
      data['user'] = user!.toJson();
    }
    if (replies != null) {
      data['replies'] = replies!.map((v) => v.toJson()).toList();
    }
    data['keywords'] = keywords ?? [];
    return data;
  }

  Future<QuerySnapshot<CommentModel>> firebaseGet({
    String orderBy = "created",
    bool descending = true,
    int limit = 10,
    DocumentSnapshot<CommentModel>? startAfterDoc,
    DocumentSnapshot<CommentModel>? startAtDoc,
    DocumentSnapshot<CommentModel>? endAtDoc,
    DocumentSnapshot<CommentModel>? endBeforeDoc,
  }) async {
    Query<CommentModel> query = _fbref.orderBy(orderBy, descending: descending);
    var numOfNull = 0;
    if (startAfterDoc != null) {
      query = query.startAfterDocument(startAfterDoc);
      numOfNull++;
    }
    if (startAtDoc != null) {
      query = query.startAtDocument(startAtDoc);
      numOfNull++;
    }
    if (endBeforeDoc != null) {
      query = query.endBeforeDocument(endBeforeDoc);
      numOfNull++;
    }
    if (endAtDoc != null) {
      query = query.endAtDocument(endAtDoc);
      numOfNull++;
    }

    assert(numOfNull < 2, "you cannot use [startAfterDoc, startAtDoc, endAtDoc, endBeforeDoc] in the same time");
    return await query.get();
  }

  Future<void> firebaseSet() {
    return _fbref.doc(id).set(this);
  }

  Future<void> firebaseUpdate(Map<String, dynamic> data) async {
    return await _fbref.doc(id).update(data);
  }

  Future<void> firebaseDelete() async {
    return await _fbref.doc(id).delete();
  }

  Future<CommentModel?> firebaseDoc() async {
    final data = await _fbref.doc(id).get();
    if (data.exists) {
      return data.data();
    }
    return null;
  }

  DocumentReference<CommentModel> firebaseDocRef() {
    return _fbref.doc(id);
  }

  CollectionReference<CommentModel> firebaseColRef() {
    return _fbref;
  }
}
