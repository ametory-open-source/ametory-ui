import 'package:ametory_ui/src/models/blog/misc_content_model.dart';
import 'package:ametory_ui/src/models/user_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MiscModel {
  late CollectionReference<MiscModel> _fbref;
  String collectionName = "misc";
  String? id;
  DateTime? modified;
  DateTime? created;
  UserModel? user;
  UserModel? modifiedBy;
  String? name;
  String? title;
  String? cover;
  String? description;
  String? url;
  List<MiscContent>? contents;

  MiscModel({this.id, this.created, this.modified, this.user, this.modifiedBy, this.name, this.title, this.cover, this.description, this.url, this.contents}) {
    _fbref = FirebaseFirestore.instance.collection(collectionName).withConverter(
          fromFirestore: (snapshot, _) => MiscModel.fromJson(snapshot.data()!),
          toFirestore: (MiscModel data, _) => data.toJson(),
        );
  }

  MiscModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    if (json['created'] != null) created = json['created'].toDate();
    if (json['modified'] != null) {
      modified = json['modified'].toDate();
    }
    user = json['user'] != null ? UserModel.fromJson(json['user']) : null;
    modifiedBy = json['modifiedBy'] != null ? UserModel.fromJson(json['modifiedBy']) : null;
    name = json['name'];
    title = json['title'];
    cover = json['cover'];
    description = json['description'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    if (modified != null) data['modified'] = Timestamp.fromDate(modified!);
    if (created != null) data['created'] = Timestamp.fromDate(created!);
    if (user != null) {
      data['user'] = user!.toJson();
    }
    if (modifiedBy != null) {
      data['modifiedBy'] = modifiedBy!.toJson();
    }
    data['name'] = name;
    data['title'] = title;
    data['cover'] = cover;
    data['description'] = description;
    data['url'] = url;
    return data;
  }

  Future<QuerySnapshot<MiscModel>> firebaseGet({
    String orderBy = "created",
    bool descending = true,
    int limit = 10,
    DocumentSnapshot<MiscModel>? startAfterDoc,
    DocumentSnapshot<MiscModel>? startAtDoc,
    DocumentSnapshot<MiscModel>? endAtDoc,
    DocumentSnapshot<MiscModel>? endBeforeDoc,
  }) async {
    Query<MiscModel> query = _fbref.orderBy(orderBy, descending: descending);
    var numOfNull = 0;
    if (startAfterDoc != null) {
      query = query.startAfterDocument(startAfterDoc);
      numOfNull++;
    }
    if (startAtDoc != null) {
      query = query.startAtDocument(startAtDoc);
      numOfNull++;
    }
    if (endBeforeDoc != null) {
      query = query.endBeforeDocument(endBeforeDoc);
      numOfNull++;
    }
    if (endAtDoc != null) {
      query = query.endAtDocument(endAtDoc);
      numOfNull++;
    }

    assert(numOfNull < 2, "you cannot use [startAfterDoc, startAtDoc, endAtDoc, endBeforeDoc] in the same time");
    return await query.get();
  }

  Future<void> firebaseSet() {
    return _fbref.doc(id).set(this);
  }

  Future<void> firebaseUpdate(Map<String, dynamic> data) async {
    return await _fbref.doc(id).update(data);
  }

  Future<void> firebaseDelete() async {
    return await _fbref.doc(id).delete();
  }

  Future<MiscModel?> firebaseDoc() async {
    final data = await _fbref.doc(id).get();
    if (data.exists) {
      return data.data();
    }
    return null;
  }

  DocumentReference<MiscModel> firebaseDocRef() {
    return _fbref.doc(id);
  }

  CollectionReference<MiscModel> firebaseColRef() {
    return _fbref;
  }
}
