import '../../../ametory_ui.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class LikeModel {
  late CollectionReference<LikeModel> _fbref;
  String collectionName = "likes";
  String? id;
  String? articleId;
  DateTime? created;
  String? userId;
  UserModel? user;

  LikeModel({this.id, this.articleId, this.created, this.userId, this.user}) {
    _fbref = FirebaseFirestore.instance.collection(collectionName).withConverter(
          fromFirestore: (snapshot, _) => LikeModel.fromJson(snapshot.data()!),
          toFirestore: (LikeModel data, _) => data.toJson(),
        );
  }

  LikeModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    articleId = json['articleId'];
    if (json['created'] != null) created = json['created'].toDate();
    userId = json['userId'];
    user = json['user'] != null ? UserModel.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['articleId'] = articleId;
    if (created != null) data['created'] = Timestamp.fromDate(created!);
    data['userId'] = userId;
    if (user != null) {
      data['userId'] ??= user!.id;
      data['user'] = user!.toJson();
    }
    return data;
  }

  Future<QuerySnapshot<LikeModel>> firebaseGet({
    String orderBy = "created",
    bool descending = true,
    int limit = 10,
    DocumentSnapshot<LikeModel>? startAfterDoc,
    DocumentSnapshot<LikeModel>? startAtDoc,
    DocumentSnapshot<LikeModel>? endAtDoc,
    DocumentSnapshot<LikeModel>? endBeforeDoc,
  }) async {
    Query<LikeModel> query = _fbref.orderBy(orderBy, descending: descending);
    var numOfNull = 0;
    if (startAfterDoc != null) {
      query = query.startAfterDocument(startAfterDoc);
      numOfNull++;
    }
    if (startAtDoc != null) {
      query = query.startAtDocument(startAtDoc);
      numOfNull++;
    }
    if (endBeforeDoc != null) {
      query = query.endBeforeDocument(endBeforeDoc);
      numOfNull++;
    }
    if (endAtDoc != null) {
      query = query.endAtDocument(endAtDoc);
      numOfNull++;
    }

    assert(numOfNull < 2, "you cannot use [startAfterDoc, startAtDoc, endAtDoc, endBeforeDoc] in the same time");
    return await query.get();
  }

  Future<void> firebaseSet() {
    return _fbref.doc(id).set(this);
  }

  Future<void> firebaseUpdate(Map<String, dynamic> data) async {
    return await _fbref.doc(id).update(data);
  }

  Future<void> firebaseDelete() async {
    return await _fbref.doc(id).delete();
  }

  Future<LikeModel?> firebaseDoc() async {
    final data = await _fbref.doc(id).get();
    if (data.exists) {
      return data.data();
    }
    return null;
  }

  DocumentReference<LikeModel> firebaseDocRef() {
    return _fbref.doc(id);
  }

  CollectionReference<LikeModel> firebaseColRef() {
    return _fbref;
  }
}
