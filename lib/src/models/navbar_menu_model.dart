import 'package:flutter/material.dart';

class NavbarMenuModel {
  final String label;
  final Widget? icon;
  final bool isCurrent;
  final Function()? onTap;
  final List<NavbarMenuModel>? children;

  NavbarMenuModel(
      {required this.label,
      this.onTap,
      this.icon,
      this.children,
      this.isCurrent = false});
}
