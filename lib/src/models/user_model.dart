import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

class UserModel {
  late CollectionReference<UserModel> _fbref;
  String collectionName = "users";
  UserModel({
    required this.id,
    this.employeeID,
    this.name = "",
    this.position,
    this.rank,
    required this.avatar,
    required this.email,
    this.companyID,
    this.companyName,
    this.divisionGroupID,
    this.divisionGroupName,
    this.companyAvatar,
    this.phone,
    this.updatedAt,
    this.createdAt,
    this.token,
    this.bio,
    this.userType,
    this.customToken,
    this.refreshToken,
    this.isAdmin = false,
    this.isSuperAdmin = false,
    this.isDefault = false,
    this.language = "id",
  }) {
    _fbref = FirebaseFirestore.instance.collection(collectionName).withConverter(
          fromFirestore: (snapshot, _) => UserModel.fromJson(snapshot.data()!),
          toFirestore: (UserModel data, _) => data.toJson(),
        );
  }
  late final String id;
  String? employeeID;
  late String name;
  String? position;
  String? rank;
  String? avatar;
  late final String email;
  String? companyID;
  String? companyName;
  String? companyAvatar;
  String? divisionGroupID;
  String? divisionGroupName;
  String? token;
  String? userType;
  String? refreshToken;
  String? customToken;
  DateTime? createdAt;
  DateTime? updatedAt;
  String? phone;
  String? bio;
  late bool isAdmin;
  late bool isSuperAdmin;
  late bool isDefault;
  late String language;

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    employeeID = json["employee_id"];
    name = json["name"];
    position = json["position"];
    phone = json["phone"];
    rank = json["rank"];
    avatar = json["avatar"];
    userType = json["userType"];
    email = json["email"];
    companyID = json["company_id"];
    companyName = json["company_name"];
    companyAvatar = json["company_avatar"];
    bio = json["bio"];
    token = json["token"];
    customToken = json["custom_token"];
    refreshToken = json["refresh_token"];
    isAdmin = json["is_admin"];
    isSuperAdmin = json["is_super_admin"];
    isDefault = json["is_default"];
    divisionGroupID = json["division_group_id"];
    divisionGroupName = json["division_group_name"];
    language = json["language"] ?? "id";
    if (json['createdAt'] != null) createdAt = json['createdAt'].toDate();
    if (json['updatedAt'] != null) updatedAt = json['updatedAt'].toDate();
  }

  UserModel copyWith({
    String? id,
    String? employeeID,
    String? name,
    String? position,
    String? rank,
    String? avatar,
    String? userType,
    String? email,
    String? companyID,
    String? companyName,
    String? companyAvatar,
    String? phone,
    String? token,
    String? customToken,
    String? refreshToken,
    String? bio,
    bool? isAdmin,
    bool? isSuperAdmin,
    bool? isDefault,
    String? divisionGroupID,
    String? divisionGroupName,
    String? language,
    DateTime? createdAt,
    DateTime? updatedAt,
  }) {
    return UserModel(
        id: id ?? this.id,
        employeeID: employeeID ?? this.employeeID,
        name: name ?? this.name,
        position: position ?? this.position,
        rank: rank ?? this.rank,
        avatar: avatar ?? this.avatar,
        email: email ?? this.email,
        companyID: companyID ?? this.companyID,
        phone: phone ?? this.phone,
        userType: userType ?? this.userType,
        companyName: companyName ?? this.companyName,
        companyAvatar: companyAvatar ?? this.companyAvatar,
        token: token ?? this.token,
        bio: bio ?? this.bio,
        customToken: customToken ?? this.customToken,
        refreshToken: refreshToken ?? this.refreshToken,
        isAdmin: isAdmin ?? this.isAdmin,
        isSuperAdmin: isSuperAdmin ?? this.isSuperAdmin,
        isDefault: isDefault ?? this.isDefault,
        divisionGroupID: divisionGroupID ?? this.divisionGroupID,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        language: language ?? this.language,
        divisionGroupName: divisionGroupName ?? this.divisionGroupName);
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data["id"] = id;
    data["employee_id"] = employeeID;
    data["name"] = name;
    data["position"] = position;
    data["userType"] = userType;
    data["rank"] = rank;
    data["avatar"] = avatar;
    data["email"] = email;
    data["company_id"] = companyID;
    data["phone"] = phone;
    data["company_name"] = companyName;
    data["company_avatar"] = companyAvatar;
    data["token"] = token;
    data["bio"] = bio;
    data["custom_token"] = customToken;
    data["refresh_token"] = refreshToken;
    data["is_admin"] = isAdmin;
    data["is_super_admin"] = isSuperAdmin;
    data["is_default"] = isDefault;
    data["division_group_id"] = divisionGroupID;
    data["division_group_name"] = divisionGroupName;
    data["language"] = language;
    if (updatedAt != null) data['updatedAt'] = Timestamp.fromDate(updatedAt!);
    if (createdAt != null) data['createdAt'] = Timestamp.fromDate(createdAt!);

    return data;
  }

  @override
  String toString() {
    final Map<String, dynamic> data = toJson();
    return jsonEncode(data);
  }

  Future<QuerySnapshot<UserModel>> firebaseGet({
    String orderBy = "created",
    bool descending = true,
    int limit = 10,
    DocumentSnapshot<UserModel>? startAfterDoc,
    DocumentSnapshot<UserModel>? startAtDoc,
    DocumentSnapshot<UserModel>? endAtDoc,
    DocumentSnapshot<UserModel>? endBeforeDoc,
  }) async {
    Query<UserModel> query = _fbref.orderBy(orderBy, descending: descending);
    var numOfNull = 0;
    if (startAfterDoc != null) {
      query = query.startAfterDocument(startAfterDoc);
      numOfNull++;
    }
    if (startAtDoc != null) {
      query = query.startAtDocument(startAtDoc);
      numOfNull++;
    }
    if (endBeforeDoc != null) {
      query = query.endBeforeDocument(endBeforeDoc);
      numOfNull++;
    }
    if (endAtDoc != null) {
      query = query.endAtDocument(endAtDoc);
      numOfNull++;
    }

    assert(numOfNull < 2, "you cannot use [startAfterDoc, startAtDoc, endAtDoc, endBeforeDoc] in the same time");
    return await query.get();
  }

  Future<void> firebaseSet() {
    return _fbref.doc(id).set(this);
  }

  Future<void> firebaseUpdate(Map<String, dynamic> data) async {
    return await _fbref.doc(id).update(data);
  }

  Future<void> firebaseDelete() async {
    return await _fbref.doc(id).delete();
  }

  Future<UserModel?> firebaseDoc() async {
    final data = await _fbref.doc(id).get();
    if (data.exists) {
      return data.data();
    }
    return null;
  }

  DocumentReference<UserModel> firebaseDocRef() {
    return _fbref.doc(id);
  }

  CollectionReference<UserModel> firebaseColRef() {
    return _fbref;
  }
}
