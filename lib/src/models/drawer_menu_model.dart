import 'package:flutter/material.dart';

import 'menu_model.dart';

class DrawerMenuModel implements MenuModel {
  @override
  List<Widget>? actions;

  @override
  Widget? icon;

  @override
  Widget? menu;

  @override
  Function()? onTap;

  EdgeInsets? padding;

  @override
  String? tooltip;

  @override
  Widget? label;

  bool isActive;

  @override
  List<MenuModel>? children;

  DrawerMenuModel({this.actions, this.icon, this.menu, this.onTap, this.padding, this.label, this.children, this.isActive = false, this.tooltip});
}
