import 'package:ametory_ui/ametory_ui.dart';

class ChatNotifModel {
  final String? title;
  final String? body;
  final String? roomID;
  final String? chatID;
  final UserModel? userData;
  final List<String>? sendTo;

  ChatNotifModel({
    this.title,
    this.body,
    this.roomID,
    this.chatID,
    this.userData,
    this.sendTo,
  });
}
