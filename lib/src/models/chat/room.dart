import 'package:cloud_firestore/cloud_firestore.dart';
import 'chat.dart';

class MyRoom {
  String? id;
  String? companyID;
  String? avatar;
  String? name;
  String? description;
  bool? isPrivate;
  List<String>? userIDs;
  List<String>? adminIDs;
  List<String>? readedIDs;
  List<MyChatUser>? users;
  List<MyChatUser>? userWritings;
  String? createdBy;
  String? lastMessage;
  DateTime? lastMessageDate;

  MyRoom({
    this.id,
    this.companyID,
    this.avatar,
    this.name,
    this.description,
    this.isPrivate,
    this.userIDs,
    this.lastMessageDate,
    this.lastMessage,
    this.createdBy,
    this.users,
    this.userWritings,
  });

  MyRoom.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    companyID = json['company_id'] ?? "";
    avatar = json['avatar'];
    name = json['name'];
    description = json['description'];
    isPrivate = json['is_private'];
    createdBy = json['created_by'];
    userIDs = <String>[];
    if (json['user_ids'] != null) {
      json['user_ids'].forEach((v) {
        userIDs!.add(v);
      });
    }
    adminIDs = <String>[];
    if (json['admin_ids'] != null) {
      json['admin_ids'].forEach((v) {
        adminIDs!.add(v);
      });
    }
    readedIDs = <String>[];
    if (json['readed_ids'] != null) {
      json['readed_ids'].forEach((v) {
        readedIDs!.add(v);
      });
    }
    users = <MyChatUser>[];
    if (json['users'] != null) {
      json['users'].forEach((v) {
        users!.add(MyChatUser.fromJson(v));
      });
    }
    userWritings = <MyChatUser>[];
    if (json['user_writings'] != null) {
      json['user_writings'].forEach((v) {
        userWritings!.add(MyChatUser.fromJson(v));
      });
    }
    lastMessageDate = json['last_message_date'].toDate();
    lastMessage = json['last_message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['company_id'] = companyID ?? "";
    data['avatar'] = avatar;
    data['name'] = name;
    data['description'] = description;
    data['is_private'] = isPrivate;
    if (userIDs != null) {
      data['user_ids'] = userIDs!.map((v) => v).toList();
    } else {
      data['user_ids'] = [];
    }
    if (adminIDs != null) {
      data['admin_ids'] = adminIDs!.map((v) => v).toList();
    } else {
      data['admin_ids'] = [];
    }
    if (readedIDs != null) {
      data['readed_ids'] = readedIDs!.map((v) => v).toList();
    } else {
      data['readed_ids'] = [];
    }
    if (users != null) {
      data['users'] = users!.map((v) => v.toJson()).toList();
    } else {
      data['users'] = [];
    }
    if (userWritings != null) {
      data['user_writings'] = userWritings!.map((v) => v.toJson()).toList();
    } else {
      data['user_writings'] = [];
    }
    data['last_message'] = lastMessage;
    data['created_by'] = createdBy;
    if (lastMessageDate != null) {
      data['last_message_date'] = Timestamp.fromDate(lastMessageDate!);
    } else {
      data['last_message_date'] = Timestamp.fromDate(DateTime.now().toLocal());
    }

    return data;
  }
}
