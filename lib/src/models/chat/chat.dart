class MyChatMessage {
  String id;
  String text;

  MyChatUser? user;

  List<MyChatMedia>? medias;

  List<MyQuickReply>? quickReplies;

  Map<String, dynamic>? customProperties;

  DateTime? createdAt;

  List<MyChatUser>? mentions;

  MyMessageStatus? status;

  MyChatMessage? replyTo;

  MyChatMessage(
      {this.id = '',
      this.text = '',
      this.medias,
      this.quickReplies,
      this.customProperties,
      this.mentions,
      this.status,
      this.replyTo,
      this.user,
      this.createdAt});

// MyChatMessage({
//     required this.user,
//     required this.createdAt,
//     this.text = '',
//     this.medias,
//     this.quickReplies,
//     this.customProperties,
//     this.mentions,
//     this.status = MessageStatus.none,
//     this.replyTo,
//   });

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'id': id,
      'user': user?.toJson(),
      'createdAt': createdAt?.toUtc().toIso8601String(),
      'text': text,
      'medias': medias?.map((MyChatMedia media) => media.toJson()).toList(),
      'quickReplies': quickReplies?.map((MyQuickReply quickReply) => quickReply.toJson()).toList(),
      'customProperties': customProperties,
      'mentions': mentions?.map((MyChatUser user) => user.toJson()).toList(),
      'status': status.toString(),
      'replyTo': replyTo?.toJson(),
    };
  }

  factory MyChatMessage.fromJson(Map<String, dynamic> jsonData) {
    return MyChatMessage(
      id: jsonData['id']?.toString() ?? '',
      user: MyChatUser.fromJson(jsonData['user'] as Map<String, dynamic>),
      createdAt: DateTime.parse(jsonData['createdAt'].toString()).toLocal(),
      text: jsonData['text']?.toString() ?? '',
      medias: jsonData['medias'] != null
          ? (jsonData['medias'] as List<dynamic>).map((dynamic media) => MyChatMedia.fromJson(media as Map<String, dynamic>)).toList()
          : <MyChatMedia>[],
      quickReplies: jsonData['quickReplies'] != null
          ? (jsonData['quickReplies'] as List<dynamic>).map((dynamic quickReply) => MyQuickReply.fromJson(quickReply as Map<String, dynamic>)).toList()
          : <MyQuickReply>[],
      customProperties: jsonData['customProperties'] as Map<String, dynamic>,
      mentions: jsonData['mentions'] != null
          ? (jsonData['mentions'] as List<dynamic>).map((dynamic mention) => MyChatUser.fromJson(mention as Map<String, dynamic>)).toList()
          : <MyChatUser>[],
      status: MyMessageStatus.parse(jsonData['status'].toString()),
      replyTo: jsonData['replyTo'] != null ? MyChatMessage.fromJson(jsonData['replyTo'] as Map<String, dynamic>) : null,
    );
  }
}

class MyCustomFeature {
  String? id;
  String? parentID;
  String? title;
  String? subtitle;
  String? date;
  String? endDate;
  String? pictureUrl;
  String? eventType;
  String? companyID;
  late bool isCustomFeature;

  MyCustomFeature({
    this.id,
    this.parentID,
    this.title,
    this.subtitle,
    this.date,
    this.endDate,
    this.pictureUrl,
    this.eventType,
    this.companyID,
    this.isCustomFeature = true,
  });

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'id': id,
      'parent_id': parentID,
      'title': title,
      'subtitle': subtitle,
      'date': date,
      'end_date': endDate,
      'picture_url': pictureUrl,
      'event_type': eventType,
      'company_id': companyID,
      'is_custom_feature': isCustomFeature,
    };
  }

  MyCustomFeature.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    parentID = json['parent_id'];
    title = json['title'];
    subtitle = json['subtitle'];
    date = json['date'];
    endDate = json['end_date'];
    pictureUrl = json['picture_url'];
    eventType = json['event_type'];
    companyID = json['company_id'];
    isCustomFeature = json['is_custom_feature'];
  }
}

class MyChatUser {
  MyChatUser({
    required this.id,
    this.profileImage,
    this.customProperties,
    this.firstName,
    this.lastName,
  });

  factory MyChatUser.fromJson(Map<String, dynamic> jsonData) {
    return MyChatUser(
      id: jsonData['id'].toString(),
      profileImage: jsonData['profileImage']?.toString(),
      firstName: jsonData['firstName']?.toString(),
      lastName: jsonData['lastName']?.toString(),
      customProperties: jsonData['customProperties'] as Map<String, dynamic>,
    );
  }

  String id;

  String? profileImage;

  Map<String, dynamic>? customProperties;

  String? firstName;

  String? lastName;

  String getFullName() {
    return (firstName ?? '') + (firstName != null && lastName != null ? ' ${lastName!}' : lastName ?? '');
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'id': id,
      'profileImage': profileImage,
      'firstName': firstName,
      'lastName': lastName,
      'customProperties': customProperties,
    };
  }
}

class MyMessageStatus {
  const MyMessageStatus._internal(this._value);
  final String _value;

  @override
  String toString() => _value;

  static MyMessageStatus parse(String value) {
    switch (value) {
      case 'none':
        return MyMessageStatus.none;
      case 'read':
        return MyMessageStatus.read;
      case 'received':
        return MyMessageStatus.received;
      case 'pending':
        return MyMessageStatus.pending;
      default:
        return MyMessageStatus.none;
    }
  }

  static const MyMessageStatus none = MyMessageStatus._internal('none');
  static const MyMessageStatus read = MyMessageStatus._internal('read');
  static const MyMessageStatus received = MyMessageStatus._internal('received');
  static const MyMessageStatus pending = MyMessageStatus._internal('pending');
}

class MyChatMedia {
  MyChatMedia({
    required this.url,
    required this.fileName,
    required this.type,
    this.isUploading = false,
    this.uploadedDate,
    this.customProperties,
  });

  factory MyChatMedia.fromJson(Map<String, dynamic> jsonData) {
    return MyChatMedia(
      url: jsonData['url'].toString(),
      fileName: jsonData['fileName'].toString(),
      type: MyMediaType.parse(jsonData['type'].toString()),
      isUploading: jsonData['isUploading'] == true,
      uploadedDate: jsonData['uploadedDate'] != null ? DateTime.parse(jsonData['uploadedDate'].toString()).toLocal() : null,
      customProperties: jsonData['customProperties'] as Map<String, dynamic>,
    );
  }

  String url;

  String fileName;

  MyMediaType type;

  bool isUploading;

  DateTime? uploadedDate;

  Map<String, dynamic>? customProperties;

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'url': url,
      'type': type.toString(),
      'fileName': fileName,
      'isUploading': isUploading,
      'uploadedDate': uploadedDate?.toUtc().toIso8601String(),
      'customProperties': customProperties,
    };
  }
}

class MyMediaType {
  const MyMediaType._internal(this._value);
  final String _value;

  @override
  String toString() => _value;

  static MyMediaType parse(String value) {
    switch (value) {
      case 'image':
        return MyMediaType.image;
      case 'video':
        return MyMediaType.video;
      case 'file':
        return MyMediaType.file;
      default:
        throw UnsupportedError('$value is not a valid MyMediaType');
    }
  }

  static const MyMediaType image = MyMediaType._internal('image');
  static const MyMediaType video = MyMediaType._internal('video');
  static const MyMediaType file = MyMediaType._internal('file');
}

class MyQuickReply {
  MyQuickReply({
    required this.title,
    this.value,
    this.customProperties,
  });

  factory MyQuickReply.fromJson(Map<String, dynamic> jsonData) {
    return MyQuickReply(
      title: jsonData['title'].toString(),
      value: jsonData['value']?.toString(),
      customProperties: jsonData['customProperties'] as Map<String, dynamic>,
    );
  }

  String title;

  String? value;

  Map<String, dynamic>? customProperties;

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'title': title,
      'value': value,
      'customProperties': customProperties,
    };
  }
}
