class ComboBoxData {
  final String label;
  final dynamic data;

  ComboBoxData({
    required this.label,
    this.data,
  });

  @override
  String toString() {
    return label.toString();
  }
}
