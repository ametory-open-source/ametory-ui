class MediaModel {
  String? id;
  String? url;
  String? caption;
  String? extention;

  MediaModel({this.id, this.url, this.caption, this.extention});
}
