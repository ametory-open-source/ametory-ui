import 'package:ametory_ui/src/utils/helper/layout.dart';
import 'package:flutter/material.dart';

class TopBar extends StatelessWidget {
  const TopBar(
      {Key? key,
      this.leading,
      this.content,
      this.trailing,
      this.isAuth = false,
      this.height,
      this.color,
      this.elevation,
      this.decoration,
      this.spacing,
      this.padding})
      : super(key: key);

  final Widget? leading;
  final Widget? content;
  final Widget? trailing;
  final bool isAuth;
  final double? height;
  final Color? color;
  final double? elevation;
  final BoxDecoration? decoration;
  final EdgeInsets? padding;
  final double? spacing;

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: elevation ?? 3,
      child: Container(
        padding: padding ?? const EdgeInsets.all(10),
        decoration: decoration ?? BoxDecoration(color: color ?? Colors.white),
        height: height ?? 55,
        width: double.infinity,
        child: Row(children: [
          leading ?? Container(),
          hspace(spacing ?? 5),
          Expanded(child: content ?? Container()),
          hspace(spacing ?? 5),
          trailing ?? Container(),
        ]),
      ),
    );
  }
}
