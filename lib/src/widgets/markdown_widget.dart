import 'dart:convert';

// import 'package:ametory_framework/helper/utils.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:markdown/markdown.dart' as md;
import 'package:ametory_ui/ametory_ui.dart' as ui;
import 'package:url_launcher/url_launcher_string.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:webviewx_plus/webviewx_plus.dart';

class MarkdownParserWidget extends StatelessWidget {
  final bool selectable;
  final Function()? onTap;

  const MarkdownParserWidget({
    Key? key,
    required this.data,
    this.onTap,
    this.shrinkWrap = true,
    bool fitContent = true,
    bool softLineBreak = false,
    this.selectable = true,
  }) : super(
          key: key,
        );

  final bool shrinkWrap;
  final String data;

  @override
  Widget build(BuildContext context) {
    List<String> lines = const LineSplitter().convert(data);
    // print(lines);
    final md.Document document = md.Document(
      extensionSet: md.ExtensionSet.gitHubFlavored,
      encodeHtml: true,
    );
    final List<md.Node> astNodes = document.parseLines(lines);

    return CustomMarkdownBuilder(data: astNodes, selectable: selectable);
  }
}

class CustomMarkdownBuilder extends StatefulWidget {
  final bool selectable;
  final List<md.Node> data;
  final Color? linkText;
  final double playerWidth;
  final double playerHeight;
  final double imageWidth;
  final String? fontFamily;

  const CustomMarkdownBuilder({
    Key? key,
    this.selectable = true,
    required this.data,
    this.linkText,
    this.imageWidth = 800,
    this.playerWidth = 800,
    this.playerHeight = 600,
    this.fontFamily,
  }) : super(key: key);

  @override
  State<CustomMarkdownBuilder> createState() => _CustomMarkdownBuilderState();
}

class _CustomMarkdownBuilderState extends State<CustomMarkdownBuilder>
    implements md.NodeVisitor {
  @override
  void initState() {
    newElements.clear();
    for (final md.Node node in widget.data) {
      // assert(_blocks.length == 1);
      node.accept(this);
    }
    super.initState();
  }

  final List<String> _kBlockTags = <String>[
    'p',
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
    'blockquote',
    'pre',
    'hr'
  ];
  final List<String> _kTableTags = <String>[
    'table',
    'thead',
    'tbody',
    'tr',
    'td',
    'th'
  ];

  final List<String> _kListTags = <String>['ul', 'ol'];
  final List<String> _others = <String>['img'];
  // bool _isBlockTag(String? tag) => _kBlockTags.contains(tag);

  // bool _isListTag(String tag) => _kListTags.contains(tag);
  List<MarkdownElement> newElements = [];

  @override
  void visitElementAfter(md.Element element) {}

  @override
  bool visitElementBefore(md.Element element) {
    if (element.tag == "blockquote") {
      newElements.add(MarkdownElement("blockquote", [element]));
    } else if (element.tag == "p") {
      if (newElements.isNotEmpty &&
          newElements.last.elements.first.tag == "blockquote") {
        return true;
      }

      List<md.Element> newList = [];
      for (var e in element.children ?? []) {
        if (e is md.Text) {
          newList.add(md.Element.text("p", e.textContent));
        }
        if (e is md.Element) {
          newList.add(e);
        }
      }
      newElements.add(MarkdownElement("paragraph", newList));
    } else if (_kBlockTags.contains(element.tag)) {
      newElements.add(MarkdownElement("block", [element]));
    } else if (_kListTags.contains(element.tag)) {
      newElements.add(MarkdownElement("list", [element]));
    } else if (_others.contains(element.tag)) {
      newElements.add(MarkdownElement("other", [element]));
    } else if (_kTableTags.contains(element.tag)) {
      if (element.tag == "table") {
        newElements.add(MarkdownElement("table", [], cells: []));
      }
      if (element.tag == "th") {
        if (newElements.last.cells!.isEmpty) {
          newElements.last.cells!.add([element]);
        } else {
          newElements.last.cells!.last.add(element);
        }
      }
      if (element.tag == "td") {
        if (newElements.last.cells!.last.length ==
            newElements.last.cells!.first.length) {
          newElements.last.cells!.add([element]);
        } else {
          newElements.last.cells!.last.add(element);
        }
      }
      // newElements.add(MarkdownElement("table", [element]));
    } else {
      // print("element ${element.tag}");
      // newElements.add(MarkdownElement("other", [element]));
    }

    return true;
  }

  @override
  void visitText(md.Text text) {}

  @override
  Widget build(BuildContext context) {
    return SelectableRegion(
      selectionControls: materialTextSelectionControls,
      focusNode: FocusNode(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: newElements
            .map((e) => Container(
                margin: const EdgeInsets.only(bottom: 10),
                child: _renderElement(e)))
            .toList(),
      ),
    );
  }

  Widget _renderElement(MarkdownElement mdElement) {
    if (mdElement.type == "paragraph") {
      if (getYouTubeUrl(mdElement.elements.first.textContent)) {
        return WebViewX(
            width: widget.playerWidth,
            height: widget.playerHeight,
            initialContent: """
<iframe width="${widget.playerWidth}" height="${widget.playerHeight}" src="https://www.youtube.com/embed/${convertUrlToId(mdElement.elements.first.textContent)}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
""",
            initialSourceType: SourceType.html);
      }
      return Text.rich(TextSpan(
          text: mdElement.elements.first.textContent,
          style: ui.MyTypo.bodyText1
              .copyWith(height: 1.5, fontFamily: widget.fontFamily),
          children: mdElement.elements.skip(1).map((e) {
            if (e.tag == "a") {
              return TextSpan(
                  text: e.textContent,
                  style: ui.MyTypo.bodyText1.copyWith(
                      height: 1.5,
                      color: widget.linkText ?? ui.MyColor.primary,
                      fontFamily: widget.fontFamily),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      launchUrlString(e.attributes['href']!);
                    });
            }
            return TextSpan(text: e.textContent);
          }).toList()));
    }

    if (mdElement.type == "table") {
      var headerWidth = {};
      var index = 0;
      for (var _ in mdElement.cells!.first) {
        headerWidth[index] = const FixedColumnWidth(300);

        index++;
      }
      return SizedBox(
        child: ui.MyTable(
          showToolbar: true,
          headerWidth: const {
            0: FixedColumnWidth(200),
            1: FixedColumnWidth(200),
          },
          headers: mdElement.cells!.first.map((e) => e.textContent).toList(),
          body: mdElement.cells!
              .skip(1)
              .map((e) => e.map((e) {
                    if (e.children!.first is md.Element) {
                      return MouseRegion(
                        cursor: SystemMouseCursors.click,
                        child: InkWell(
                          onTap: () {
                            launchUrlString((e.children!.first as md.Element)
                                    .attributes["href"] ??
                                "");
                          },
                          child: Text(
                            e.textContent,
                            style: ui.MyTypo.bodyText1.copyWith(
                                height: 1.5,
                                color: widget.linkText ?? ui.MyColor.primary,
                                fontFamily: widget.fontFamily),
                          ),
                        ),
                      );
                    }
                    return Text(e.textContent);
                  }).toList())
              .toList(),
        ),
      );
    }

    return Row(
      children: mdElement.elements.map(_renderChildElement).toList(),
    );
  }

  Widget _renderChildElement(md.Element element) {
    if (element.tag == "h1") {
      return Text(element.textContent,
          style: ui.MyTypo.heading1
              .copyWith(height: 1.5, fontFamily: widget.fontFamily));
    }
    if (element.tag == "h2") {
      return Text(element.textContent,
          style: ui.MyTypo.heading2
              .copyWith(height: 1.5, fontFamily: widget.fontFamily));
    }
    if (element.tag == "h3") {
      return Text(element.textContent,
          style: ui.MyTypo.heading3
              .copyWith(height: 1.5, fontFamily: widget.fontFamily));
    }
    if (element.tag == "h4") {
      return Text(element.textContent,
          style: ui.MyTypo.heading4
              .copyWith(height: 1.5, fontFamily: widget.fontFamily));
    }
    if (element.tag == "p") {
      return Text(
        element.textContent,
        style: ui.MyTypo.bodyText1
            .copyWith(height: 1.5, fontFamily: widget.fontFamily),
      );
    }
    if (element.tag == "ul") {
      return Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: (element.children ?? [])
              .map((e) => Padding(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ui.hspace(20),
                        const Padding(
                          padding: EdgeInsets.only(top: 6.0),
                          child: Icon(
                            Icons.circle,
                            size: 6,
                          ),
                        ),
                        ui.hspace(10),
                        Expanded(
                          child: Text(
                            e.textContent,
                            style: ui.MyTypo.bodyText1.copyWith(
                                height: 1.5, fontFamily: widget.fontFamily),
                          ),
                        )
                      ],
                    ),
                  ))
              .toList(),
        ),
      );
    }
    if (element.tag == "ol") {
      return Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: (element.children ?? []).map((e) {
            var index = element.children!.indexOf(e);
            return Padding(
              padding: const EdgeInsets.only(bottom: 10.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ui.hspace(20),
                  Text(
                    "${index + 1}. ",
                    style: ui.MyTypo.bodyText1
                        .copyWith(height: 1.5, fontFamily: widget.fontFamily),
                  ),
                  ui.hspace(10),
                  Expanded(
                    child: Text(
                      e.textContent,
                      style: ui.MyTypo.bodyText1
                          .copyWith(height: 1.5, fontFamily: widget.fontFamily),
                    ),
                  )
                ],
              ),
            );
          }).toList(),
        ),
      );
    }
    if (element.tag == "a") {
      return MouseRegion(
        cursor: SystemMouseCursors.click,
        child: InkWell(
          onTap: () {
            launchUrlString(element.attributes["href"] ?? "");
          },
          child: Text(
            element.textContent,
            style: ui.MyTypo.bodyText1.copyWith(
                height: 1.5,
                color: widget.linkText ?? ui.MyColor.primary,
                fontFamily: widget.fontFamily),
          ),
        ),
      );
    }
    if (element.tag == "img") {
      return Expanded(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              constraints: BoxConstraints(maxWidth: widget.imageWidth),
              child: Tooltip(
                  message: "${element.attributes['alt']}",
                  child: element.attributes['src']!.contains(".svg")
                      ? SvgPicture.network(element.attributes['src']!)
                      : Image.network(element.attributes['src']!))),
          Text(
            "${element.attributes['alt']}",
            style: ui.MyTypo.smallText.copyWith(
                fontSize: 10, height: 1.5, fontFamily: widget.fontFamily),
          )
        ],
      ));
    }

    if (element.tag == "li") {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(top: 4.0),
            child: Icon(
              Icons.circle,
              size: 4,
            ),
          ),
          ui.hspace(5),
          Expanded(
            child: Text(
              element.textContent,
              style: ui.MyTypo.bodyText1
                  .copyWith(height: 1.5, fontFamily: widget.fontFamily),
            ),
          ),
        ],
      );
    }
    if (element.tag == "pre") {
      return Expanded(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            constraints: BoxConstraints(maxWidth: widget.imageWidth),
            width: widget.imageWidth,
            decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.1),
                borderRadius: BorderRadius.circular(10)),
            padding: const EdgeInsets.all(10),
            child: Text(
              element.textContent,
              style: ui.MyTypo.smallText.copyWith(
                  height: 1.5,
                  color: Colors.black87,
                  fontFamily: GoogleFonts.spaceMono().fontFamily),
            ),
          ),
        ],
      ));
    }
    if (element.tag == "blockquote") {
      return Expanded(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            constraints: BoxConstraints(maxWidth: widget.imageWidth),
            decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.1),
                borderRadius: BorderRadius.circular(10)),
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                Text(
                  "\"",
                  style: ui.MyTypo.heading1.copyWith(
                      color: Colors.grey, fontFamily: widget.fontFamily),
                ),
                Expanded(
                  child: Text(
                    element.textContent,
                    style: ui.MyTypo.smallText.copyWith(
                        height: 1.5,
                        color: Colors.black87,
                        fontFamily: widget.fontFamily),
                  ),
                )
              ],
            ),
          ),
        ],
      ));
    }
    return Text(
      "${element.tag} => ${element.textContent}",
      style: ui.MyTypo.smallText.copyWith(
          height: 1.5, color: Colors.red, fontFamily: widget.fontFamily),
    );
  }

  bool getYouTubeUrl(String content) {
    RegExp regExp = RegExp(
        r'((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$');
    String? matches = regExp.stringMatch(content);
    if (matches == null) {
      return false;
    }
    // print(matches);
    // final String youTubeUrl = matches;
    return true;
  }

  // extractUrl(String content) {
  //   RegExp regExp = RegExp(r'^\[([\w\s\d]+)\]\(((?:\/|https?:\/\/)[\w\d./?=#]+)\)$');
  //   var matches = regExp.allMatches(content);
  // }

  String? convertUrlToId(String url, {bool trimWhitespaces = true}) {
    if (!url.contains("http") && (url.length == 11)) return url;
    if (trimWhitespaces) url = url.trim();

    for (var exp in [
      RegExp(
          r"^https:\/\/(?:www\.|m\.)?youtube\.com\/watch\?v=([_\-a-zA-Z0-9]{11}).*$"),
      RegExp(
          r"^https:\/\/(?:www\.|m\.)?youtube(?:-nocookie)?\.com\/embed\/([_\-a-zA-Z0-9]{11}).*$"),
      RegExp(r"^https:\/\/youtu\.be\/([_\-a-zA-Z0-9]{11}).*$")
    ]) {
      Match? match = exp.firstMatch(url);
      if (match != null && match.groupCount >= 1) return match.group(1);
    }

    return null;
  }
}

class MarkdownElement {
  final String type;
  final List<md.Element> elements;
  final List<List<md.Element>>? cells;

  MarkdownElement(this.type, this.elements, {this.cells});
}
