import 'package:ametory_ui/src/models/menu_model.dart';
import 'package:fluentui_icons/fluentui_icons.dart';
import 'package:flutter/material.dart';

import '../models/main_menu_model.dart';
import '../utils/helper/layout.dart';

class MainMenu extends StatefulWidget {
  final List<MenuModel> menu;
  final bool isCollapsed;
  final bool isCollapsing;
  final double iconWidth;
  final Color? sidebarActiveMenuColor;
  final BorderRadius? sidebarActiveBorderRadius;
  final bool showLabelCollapsed;
  const MainMenu(
      {Key? key,
      required this.menu,
      this.showLabelCollapsed = false,
      this.iconWidth = 60,
      this.sidebarActiveMenuColor,
      this.isCollapsed = false,
      this.isCollapsing = false,
      this.sidebarActiveBorderRadius})
      : super(key: key);

  @override
  State<MainMenu> createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  List<bool> parentCollapsed = [];
  @override
  void initState() {
    // parentCollapsed.first = true;

    super.initState();
  }

  _renderChildren(List<MenuModel> e) {
    return Column(
      children: e.map((e) {
        if (e is MainMenuModel) {
          return _renderMenu(e);
        }
        return Container();
      }).toList(),
    );
  }

  Widget _renderMenu(MainMenuModel e) {
    Widget icon = e.icon ?? Container();
    if (widget.showLabelCollapsed &&
        e.label != null &&
        (widget.isCollapsed || widget.isCollapsing)) {
      icon = SizedBox(
        width: widget.iconWidth,
        child: Column(
          children: [e.icon ?? Container(), vspace(5), e.label!],
        ),
      );
    }
    return Container(
      padding: (widget.isCollapsed || widget.isCollapsing)
          ? const EdgeInsets.symmetric(horizontal: 5)
          : null,
      decoration: BoxDecoration(
        borderRadius: widget.sidebarActiveBorderRadius,
        color: e.isActive
            ? (widget.sidebarActiveMenuColor ?? Colors.black.withOpacity(0.03))
            : null,
      ),
      child: Tooltip(
        waitDuration: const Duration(milliseconds: 1500),
        verticalOffset: 0,
        message: e.tooltip,
        child: InkWell(
          onTap: e.onTap,
          child: Padding(
            padding: e.padding ?? const EdgeInsets.symmetric(vertical: 10),
            child: Row(children: [
              SizedBox(
                  width: e.icon != null ? widget.iconWidth : null, child: icon),
              if (!widget.isCollapsed && !widget.isCollapsing) hspace(5),
              if (!widget.isCollapsed && !widget.isCollapsing)
                Expanded(child: e.menu!),
              if (!widget.isCollapsed && !widget.isCollapsing) hspace(5),
              if (!widget.isCollapsed &&
                  !widget.isCollapsing &&
                  e.actions != null)
                Row(
                  children: e.actions!,
                ),
            ]),
          ),
        ),
      ),
    );
  }

  openMenu(int index, bool val) {
    setState(() {
      parentCollapsed[index] = val;
    });
  }

  @override
  void didUpdateWidget(covariant MainMenu oldWidget) {
    if (widget.menu.isNotEmpty) {
      parentCollapsed = List.generate(widget.menu.length, (index) => false);
      parentCollapsed.first = true;
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: widget.menu.map((e) {
        if (e is MainMenuModel) {
          final index = widget.menu.indexOf(e);
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(child: _renderMenu(e)),
                  if (!widget.isCollapsed &&
                      !widget.isCollapsing &&
                      e.children != null)
                    InkWell(
                      onTap: () {
                        openMenu(index, !parentCollapsed[index]);
                      },
                      child: Icon(
                        parentCollapsed[index]
                            ? FluentSystemIcons.ic_fluent_chevron_down_regular
                            : FluentSystemIcons.ic_fluent_chevron_right_regular,
                        size: 14,
                      ),
                    ),
                  // if (!widget.isCollapsed && !widget.isCollapsing) hspace(10),
                ],
              ),
              if (!widget.isCollapsed &&
                  !widget.isCollapsing &&
                  e.children != null &&
                  parentCollapsed[index])
                _renderChildren(e.children!),
            ],
          );
        }
        return Container();
      }).toList(),
    );
  }
}
