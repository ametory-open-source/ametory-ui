import 'package:ametory_ui/src/models/calendar/day_model.dart';
import 'package:flutter/material.dart';

import '../../../ametory_ui.dart';
import '../../utils/helper/date.dart';

class DayWidget extends StatelessWidget {
  final DayModel day;
  final DateTime selectedMonth;
  final double? fontSize;
  final FontWeight? fontWeight;
  final String? fontFamily;
  final Color? color;
  final Color? holidayColor;
  final Color? todayColor;
  final double? todayFontSize;
  final bool isSelected;
  final Function(DayModel)? onTap;
  const DayWidget(
      {Key? key,
      required this.day,
      this.fontSize,
      this.fontWeight,
      this.fontFamily,
      this.color,
      this.holidayColor,
      this.todayFontSize,
      this.todayColor,
      this.isSelected = false,
      this.onTap,
      required this.selectedMonth})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double eventDotSize = ResponsiveWidget.isSmallScreen(context) ? 5 : 10;
    final now = DateTime.now();

    bool isToday = isThisday(now, day.date!);
    bool isOutOfMonth = !isThisMonth(selectedMonth, day.date!);

    bool isHoliday = false;
    if (day.events != null &&
        day.events!.where((e) => e.isHoliday).isNotEmpty) {
      isHoliday = true;
    }
    if (day.date!.weekday == 7) isHoliday = true;
    Color dayColor = Colors.black.withOpacity(0.7);
    double? defaultFontSize = 12;
    FontWeight? defaultFontWeight = FontWeight.bold;
    if (fontSize != null) defaultFontSize = fontSize!;
    if (isToday) dayColor = Colors.white;
    if (isToday) defaultFontSize = todayFontSize ?? 14;
    if (isHoliday) {
      dayColor = holidayColor ?? MyColor.danger;
    }
    BoxDecoration? decoration;
    if (isToday) {
      decoration = BoxDecoration(
        shape: BoxShape.circle,
        color: MyColor.info.shade300,
      );
    }
    if (isSelected) {
      decoration = BoxDecoration(
        shape: BoxShape.circle,
        color: MyColor.info.shade100,
      );
    }
    if (isOutOfMonth) {
      defaultFontSize = 10;
      defaultFontWeight = FontWeight.normal;
    }

    if (!day.enabled) {
      defaultFontSize = 9;
      defaultFontWeight = FontWeight.normal;
      dayColor = Colors.black12;
    }
    return Flexible(
      flex: 1,
      child: InkWell(
        onTap: !day.enabled
            ? null
            : onTap == null
                ? null
                : () => onTap!(day),
        child: Stack(
          children: [
            Container(
              width: double.infinity,
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    decoration: decoration,
                    width: 40,
                    height: 40,
                    child: Center(
                      child: Text(
                        "${day.date!.day}",
                        style: TextStyle().copyWith(
                            fontFamily: fontFamily ?? MyTypo.fontFamily,
                            fontSize: defaultFontSize,
                            fontWeight: defaultFontWeight,
                            color: dayColor),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            if (day.events != null)
              Positioned(
                bottom: 10,
                left: 0,
                right: 0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: day.events!
                      .sublist(0, day.events!.length > 5 ? 5 : null)
                      .map((e) => Container(
                            margin: EdgeInsets.all(1),
                            width: eventDotSize,
                            height: eventDotSize,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: e.isHoliday
                                    ? MyColor.danger
                                    : MyColor.primary),
                          ))
                      .toList(),
                ),
              )
          ],
        ),
      ),
    );
  }
}
