import 'package:ametory_ui/src/styles/color.dart';
import 'package:ametory_ui/src/styles/typo.dart';
import 'package:ametory_ui/src/utils/helper/layout.dart';
import 'package:flutter/material.dart';

import '../../models/calendar/day_of_week_model.dart';

class WeeksWidget extends StatelessWidget {
  final List<DayOfWeekModel>? dayOfWeeks;
  final Color? weekendColor;
  final Color? weekdayColor;
  final bool useAbbreviation;
  final double? fontSize;
  final FontWeight? fontWeight;
  final String? fontFamily;
  final bool showWeekNumber;
  WeeksWidget(
      {Key? key,
      this.dayOfWeeks,
      this.useAbbreviation = false,
      this.showWeekNumber = false,
      this.weekendColor,
      this.weekdayColor,
      this.fontFamily,
      this.fontWeight,
      this.fontSize})
      : super(key: key);

  final List<DayOfWeekModel> _dayOfWeeks = [
    DayOfWeekModel(name: "Minggu", abbreviation: "M", isWeekend: true),
    DayOfWeekModel(name: "Senin", abbreviation: "S", isWeekend: false),
    DayOfWeekModel(name: "Selasa", abbreviation: "S", isWeekend: false),
    DayOfWeekModel(name: "Rabu", abbreviation: "R", isWeekend: false),
    DayOfWeekModel(name: "Kamis", abbreviation: "K", isWeekend: false),
    DayOfWeekModel(name: "Jumat", abbreviation: "J", isWeekend: false),
    DayOfWeekModel(name: "Sabtu", abbreviation: "S", isWeekend: false),
  ];
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        if (showWeekNumber) hspace(30),
        Expanded(
          child: Row(
              children: (dayOfWeeks ?? _dayOfWeeks)
                  .map((e) => Flexible(
                          child: Container(
                        width: double.infinity,
                        padding: EdgeInsets.all(10),
                        child: Text(
                          useAbbreviation ? e.abbreviation! : e.name!,
                          style: TextStyle().copyWith(
                              fontFamily: fontFamily ?? MyTypo.fontFamily,
                              fontSize: fontSize ?? 12,
                              fontWeight: fontWeight ?? FontWeight.bold,
                              color: e.isWeekend!
                                  ? weekendColor ?? MyColor.danger
                                  : weekdayColor ?? Colors.black45),
                          textAlign: TextAlign.center,
                        ),
                      )))
                  .toList()),
        ),
      ],
    );
  }
}
