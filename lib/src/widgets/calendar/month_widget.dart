import 'package:ametory_ui/src/styles/typo.dart';
import 'package:ametory_ui/src/utils/helper/date.dart';
import 'package:flutter/material.dart';

class MonthWidget extends StatelessWidget {
  final Widget? prev;
  final Widget? next;
  final DateTime day;
  final TextStyle? style;
  final List<String>? months;
  final Function()? onTap;
  MonthWidget(
      {Key? key,
      this.prev,
      this.next,
      this.onTap,
      required this.day,
      this.months = const [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ],
      this.style})
      : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    final now = DateTime.now();
    String year = "";
    if (!isThisYear(now, day)) {
      year = " ${day.year}";
    }


    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          prev ?? Container(),
          Expanded(
            child: GestureDetector(
              onTap: onTap,
              child: Text(
                "${months![day.month - 1]}$year",
                style: style ?? MyTypo.heading3,
                textAlign: TextAlign.center,
              ),
            ),
          ),
          next ?? Container(),
        ],
      ),
    );
  }
}
