import 'package:flutter/material.dart';

import '../../ametory_ui.dart';

class DialogWidget extends StatelessWidget {
  final BuildContext context;
  final Function()? onSubmit;
  final String? title;
  final Function()? onCancel;
  final Widget? child;
  final EdgeInsets? padding;
  final double? width;
  final double? height;
  final String okText;
  final String cancelText;
  final ButtonStyle? cancelStyle;
  final ButtonStyle? submitStyle;
  final bool disableFooter;
  final Color? backgroundColor;
  final Color? textColor;
  final bool fixedHeight;
  final BoxConstraints? constraints;

  const DialogWidget(
    this.context, {
    Key? key,
    this.onSubmit,
    this.title,
    this.onCancel,
    this.child,
    this.width,
    this.cancelStyle,
    this.backgroundColor,
    this.constraints,
    this.textColor,
    this.submitStyle,
    this.cancelText = "Batal",
    this.okText = "Kirim",
    this.height,
    this.disableFooter = false,
    this.fixedHeight = false,
    this.padding,
  }) : super(key: key);

  show() {
    showDialog(context: context, builder: build);
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: backgroundColor ?? Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Container(
        constraints: constraints,
        padding: padding ?? const EdgeInsets.all(20),
        width: width ?? 600,
        height: height ?? 400,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (title != null)
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    title!,
                    style: MyTypo.heading3.copyWith(color: textColor),
                  ),
                  InkWell(
                      onTap: onCancel ?? () => Navigator.of(context).pop(),
                      child: Icon(
                        Icons.close,
                        color: textColor ?? Colors.black54,
                      ))
                ],
              ),
            if (title != null) vspace(10),
            if (title != null)
              Container(
                padding: const EdgeInsets.symmetric(vertical: 10),
                height: 1,
                color: Colors.black.withOpacity(0.03),
              ),
            if (title != null) vspace(10),
            Expanded(
                child: (child != null)
                    ? fixedHeight
                        ? child!
                        : SingleChildScrollView(child: child!)
                    : Container()),
            if (!disableFooter) vspace(20),
            if (!disableFooter)
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (onSubmit == null) Container(),
                  TextButton(
                    onPressed: onCancel ?? () => Navigator.of(context).pop(),
                    style: cancelStyle ?? MyStyle.buttonRed,
                    child: Text(cancelText),
                  ),
                  if (onSubmit != null)
                    TextButton(
                      onPressed: onSubmit!,
                      style: submitStyle ?? MyStyle.buttonDefault,
                      child: Text(okText),
                    ),
                ],
              ),
          ],
        ),
      ),
    );
  }
}
