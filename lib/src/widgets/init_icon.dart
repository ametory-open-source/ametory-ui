import 'package:flutter/material.dart';

extension on String {
  String initials() {
    String result = "";
    List<String> words = split(" ");
    for (var element in words) {
      if (element.isNotEmpty && result.length < 2) {
        result += element[0];
      }
    }

    return result.trim().toUpperCase();
  }
}

class Initicon extends StatelessWidget {
  final String? text;
  final Color backgroundColor;

  /// Font color by default is smart. If background color is dark, the font color is white, else is black.
  final Color? color;
  final Color? borderColor;
  final double size;
  final double borderWidth;
  final BorderRadiusGeometry? borderRadius;
  final double elevation;
  final String? backgroundImageUrl;

  const Initicon({
    Key? key,
    this.text,
    this.backgroundColor = Colors.grey,
    this.color,
    this.backgroundImageUrl,
    this.size = 45,
    this.borderWidth = 3,
    this.borderRadius,
    this.borderColor,
    this.elevation = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: elevation,
      color: (backgroundImageUrl != null && backgroundImageUrl!.isNotEmpty)
          ? null
          : backgroundColor,
      borderRadius: borderRadius ?? BorderRadius.circular(150),
      clipBehavior: Clip.antiAlias,
      child: Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
            border: borderColor != null
                ? Border.all(width: 3, color: borderColor!)
                : null,
            borderRadius:
                borderColor != null ? BorderRadius.circular(size / 2) : null),
        child: backgroundImageUrl != null && backgroundImageUrl!.isNotEmpty
            ? CircleAvatar(
                backgroundColor: color ??
                    (HSLColor.fromColor(backgroundColor).lightness < 0.8
                        ? Colors.white
                        : Colors.black87),
                backgroundImage: NetworkImage(backgroundImageUrl!))
            : Center(
                child: Text(
                  text!.initials(),
                  style: TextStyle(
                    color: color ??
                        (HSLColor.fromColor(backgroundColor).lightness < 0.8
                            ? Colors.white
                            : Colors.black87),
                    fontSize: size / (text!.initials().length == 2 ? 2.5 : 1.8),
                  ),
                ),
              ),
      ),
    );
  }
}
