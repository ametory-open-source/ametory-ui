import 'package:flutter/material.dart';
import 'package:seo_renderer/seo_renderer.dart';

import '../markdown_widget.dart';

class BlogMarkdown extends StatelessWidget {
  final String text;
  final bool selectable;
  final String fontFamily;
  final int seoHeadingStyle;
  final TextStyle? a;
  final TextStyle? p;
  final TextStyle? h1;
  final TextStyle? h2;
  final TextStyle? h3;
  final TextStyle? h4;
  final TextStyle? h5;
  final TextStyle? h6;
  final Function(String, String?, String)? onTapLink;
  final Function()? onTapText;

  const BlogMarkdown(
    this.text, {
    Key? key,
    this.selectable = true,
    this.seoHeadingStyle = 0,
    required this.fontFamily,
    this.a,
    this.p,
    this.h1,
    this.h2,
    this.h3,
    this.h4,
    this.h5,
    this.h6,
    this.onTapLink,
    this.onTapText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextRendererStyle? _headingStyle;
    switch (seoHeadingStyle) {
      case 1:
        _headingStyle = TextRendererStyle.header1;
        break;
      case 2:
        _headingStyle = TextRendererStyle.header2;
        break;
      case 3:
        _headingStyle = TextRendererStyle.header3;
        break;
      case 4:
        _headingStyle = TextRendererStyle.header4;
        break;
      case 5:
        _headingStyle = TextRendererStyle.header5;
        break;
      case 6:
        _headingStyle = TextRendererStyle.header6;
        break;
      default:
        _headingStyle = TextRendererStyle.paragraph;
    }
    return TextRenderer(
      text: text,
      style: _headingStyle,
      child: MarkdownParserWidget(
        data: text,
      ),
      //  MarkdownBody(
      //     selectable: selectable,
      //     styleSheet: MarkdownStyleSheet(
      //       a: a ?? TextStyle().copyWith(fontFamily: fontFamily, height: 1.8, fontSize: 18),
      //       p: p ?? TextStyle().copyWith(fontFamily: fontFamily, height: 1.8, fontSize: 18),
      //       h1: h1 ?? MyTypo.heading2.copyWith(fontSize: 32),
      //       h2: h2 ?? MyTypo.heading2.copyWith(fontSize: 24),
      //       h3: h3 ?? MyTypo.heading2.copyWith(fontSize: 18),
      //       h4: h4 ?? MyTypo.heading2.copyWith(fontSize: 16),
      //       h5: h5 ?? MyTypo.heading2.copyWith(fontSize: 12),
      //       h6: h6 ?? MyTypo.heading2.copyWith(fontSize: 10),
      //     ),
      //     onTapLink: onTapLink,
      //     onTapText: onTapText,
      //     extensionSet: md.ExtensionSet(
      //       md.ExtensionSet.gitHubFlavored.blockSyntaxes,
      //       [md.EmojiSyntax(), ...md.ExtensionSet.gitHubFlavored.inlineSyntaxes],
      //     ),
      //     data: text),
    );
  }
}
