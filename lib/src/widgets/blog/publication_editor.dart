import 'dart:async';
import 'dart:typed_data';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:ametory_ui/ametory_ui.dart' as ui;
import 'package:html2md/html2md.dart' as html2md;
import 'package:html_editor_enhanced_forked_by_ametory/html_editor.dart';
import 'package:image_picker/image_picker.dart';
import 'package:markdown/markdown.dart' as md;
import 'package:material_tag_editor/tag_editor.dart';
import 'package:uuid/uuid.dart';
import 'package:recase/recase.dart';
import 'package:fluentui_icons/fluentui_icons.dart';

import '../../utils/helper/string.dart';

class PublicationEditor extends StatefulWidget {
  final ui.PublicationModel? data;
  final HtmlEditorController? controller;
  final FutureOr<bool> Function(PlatformFile file, InsertFileType type)?
      mediaUploadInterceptor;
  final Function(Uint8List data, String filename, String mimeType)?
      onMediaUploadPickFile;
  final Function(ui.PublicationModel)? onSave;
  final Function(ui.PublicationContent)? onDelete;
  final Function(XFile?)? onPickFeatureImage;
  final Function()? onAddContent;
  final void Function(int, int) onReorder;
  final ui.UserModel? userData;
  final ButtonStyle? buttonStyle;
  final String? featureImage;
  final List<ui.BlogModel> contents;
  final bool isLoadingContent;
  const PublicationEditor(
      {Key? key,
      this.data,
      this.controller,
      this.mediaUploadInterceptor,
      this.onMediaUploadPickFile,
      this.onSave,
      this.onPickFeatureImage,
      this.buttonStyle,
      this.featureImage,
      this.isLoadingContent = false,
      this.userData,
      required this.contents,
      this.onAddContent,
      required this.onReorder,
      this.onDelete})
      : assert(userData != null, "User data cannot be null"),
        super(key: key);

  @override
  State<PublicationEditor> createState() => _PublicationEditorState();
}

class _PublicationEditorState extends State<PublicationEditor> {
  Timer? timeHandle;
  TextEditingController titleController = TextEditingController();
  TextEditingController excerptController = TextEditingController();
  TextEditingController imageCaptionController = TextEditingController();
  TextEditingController permalinkController = TextEditingController();
  ScrollController scrollController = ScrollController();
  String result = '';
  late HtmlEditorController controller;

  bool isLoading = false;
  bool isLoadingContent = false;
  String isLoadingText = "Loading ...";
  String featureImage = "";
  List<String> tags = [];
  bool isPublished = false;
  bool commentActive = false;
  ui.PublicationModel? article;
  List<ui.BlogModel> contents = [];
  String content = "";
  @override
  void initState() {
    if (widget.controller != null) {
      controller = widget.controller!;
    } else {
      controller = HtmlEditorController();
    }
    print("CREATE SCREEN LOADED");
    if (widget.featureImage != null) {
      featureImage = widget.featureImage!;
    }
    if (widget.data != null) {
      article = widget.data;
      titleController.text = article!.title!;
      imageCaptionController.text = article?.imageCaption ?? "";
      permalinkController.text = article?.permalink ?? "";
      featureImage = article?.image ?? "";
      tags = article?.tags ?? [];
      isPublished = article!.published!;
      content = md.markdownToHtml(article?.description ?? "");
    }
    super.initState();
  }

  @override
  void dispose() {
    timeHandle?.cancel();
    super.dispose();
  }

  @override
  void didUpdateWidget(PublicationEditor oldWidget) {
    if (widget.featureImage != oldWidget.featureImage) {
      featureImage = widget.featureImage!;
      setState(() {});
    }
    if (widget.contents != oldWidget.contents) {
      contents = widget.contents;
      print("contents $contents");
      setState(() {});
    }
    if (widget.data != oldWidget.data) {
      article = widget.data;
      print("article $article");
      setState(() {});
    }
    if (widget.isLoadingContent != oldWidget.isLoadingContent) {
      isLoadingContent = widget.isLoadingContent;
      setState(() {});
    }
    super.didUpdateWidget(oldWidget);
  }

  _onSave() async {
    if (titleController.text.isEmpty) {
      ui.flush(context, "Judul tidak boleh kosong");
      return;
    }

    final content = await controller.getText();
    ui.PublicationModel? data;

    if (article != null) {
      data = ui.PublicationModel(
          id: article!.id,
          title: titleController.text,
          permalink: permalinkController.text,
          description: html2md.convert(content),
          tags: tags,
          imageCaption: imageCaptionController.text,
          published: isPublished,
          user: article!.user,
          created: article!.created,
          modified: DateTime.now(),
          keywords: generateKeywords([titleController.text]),
          image: featureImage);
    } else {
      data = ui.PublicationModel(
          id: const Uuid().v4(),
          title: titleController.text,
          permalink: permalinkController.text,
          description: html2md.convert(content),
          tags: tags,
          imageCaption: imageCaptionController.text,
          published: isPublished,
          user: widget.userData,
          created: DateTime.now(),
          modified: DateTime.now(),
          keywords: generateKeywords([titleController.text]),
          image: featureImage);
    }

    if (widget.onSave != null) {
      widget.onSave!(data);
    }
  }

  Widget _renderListContents() {
    if (article == null) return Container();
    if (article!.contents == null) return Container();
    return ReorderableListView(
        shrinkWrap: true,
        onReorder: widget.onReorder,
        children: [
          ...article!.contents!
              .map((e) => Card(
                    key: Key(e.id!),
                    child: ListTile(
                      title: Text(e.articleTitle!),
                      leading: InkWell(
                          onTap: () => widget.onDelete!(e),
                          child:
                              Icon(FluentSystemIcons.ic_fluent_delete_regular)),
                    ),
                  ))
              .toList()
        ]);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: double.infinity,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(),
                      TextButton(
                          onPressed: () async {
                            _onSave();
                          },
                          style: widget.buttonStyle ?? ui.MyStyle.buttonDefault,
                          child: const Text("Simpan & Kirim")),
                    ],
                  ),
                  ui.vspace(10),
                  ui.Input(
                    hintText: "Judul ...",
                    backgroundColor: Colors.white,
                    controller: titleController,
                    textFieldStyle: ui.MyTypo.heading1
                        .copyWith(fontWeight: FontWeight.w300),
                    onChanged: (val) {
                      final pm = ReCase(titleController.text);
                      permalinkController.text =
                          pm.paramCase.replaceAll(",", "");
                    },
                  ),
                  Expanded(
                      child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(),
                            TextButton.icon(
                                onPressed: widget.onAddContent,
                                icon: const Icon(FluentSystemIcons
                                    .ic_fluent_add_circle_regular),
                                label: const Text("Tambah Konten"))
                          ],
                        ),
                        ui.vspace(20),
                        _renderListContents(),
                      ],
                    ),
                  )),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Container(
                color: Colors.black.withOpacity(0.03),
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 300,
                      child: HtmlEditor(
                        controller: controller,
                        htmlEditorOptions: HtmlEditorOptions(
                          hint: 'Deskripsi...',
                          shouldEnsureVisible: true,
                          initialText: content,
                        ),
                        htmlToolbarOptions: HtmlToolbarOptions(
                          defaultToolbarButtons: const [
                            StyleButtons(),
                            FontSettingButtons(
                                fontSizeUnit: false,
                                fontName: false,
                                fontSize: false),
                            FontButtons(
                                clearAll: false,
                                subscript: false,
                                superscript: false),
                            // ColorButtons(),
                            ListButtons(listStyles: false),
                            ParagraphButtons(
                                textDirection: false,
                                lineHeight: false,
                                caseConverter: false),
                            InsertButtons(
                                video: false,
                                audio: false,
                                table: true,
                                hr: true,
                                otherFile: false)
                          ],
                          toolbarPosition:
                              ToolbarPosition.aboveEditor, //by default
                          toolbarType:
                              ToolbarType.nativeScrollable, //by default
                          onButtonPressed: (ButtonType type, bool? status,
                              Function? updateStatus) {
                            return true;
                          },
                          onDropdownChanged: (DropdownType type,
                              dynamic changed,
                              Function(dynamic)? updateSelectedItem) {
                            return true;
                          },
                          mediaLinkInsertInterceptor:
                              (String url, InsertFileType type) {
                            return true;
                          },
                          mediaUploadInterceptor: widget
                                  .mediaUploadInterceptor ??
                              (PlatformFile file, InsertFileType type) async {
                                if (widget.onMediaUploadPickFile != null) {
                                  widget.onMediaUploadPickFile!(file.bytes!,
                                      file.name, "image/${file.extension}");
                                  return false;
                                }
                                // final res = await uploadToStorage(file.bytes!, file.name, "image/${file.extension}");
                                // // print(getStorageUrl(context, res));
                                // controller.insertNetworkImage(getStorageUrl(context, res), filename: file.name);
                                return true;
                              },
                        ),
                        otherOptions: const OtherOptions(height: 550),
                        callbacks: Callbacks(
                            onBeforeCommand: (String? currentHtml) {},
                            onChangeContent: (String? changed) {},
                            onChangeCodeview: (String? changed) {},
                            onChangeSelection: (EditorSettings settings) {},
                            onDialogShown: () {},
                            onEnter: () {},
                            onFocus: () {
                              if (controller.characterCount == 0) {
                                controller.execCommand('fontName',
                                    argument: 'Georgia');
                              }
                            },
                            onBlur: () {},
                            onBlurCodeview: () {},
                            onInit: () {},
                            onImageUploadError: (FileUpload? file,
                                String? base64Str, UploadError error) {
                              print("$error");
                              print(base64Str ?? '');
                              if (file != null) {
                                print(file.name);
                                print("${file.size}");
                                print(file.type);
                              }
                            },
                            onKeyDown: (int? keyCode) {},
                            onKeyUp: (int? keyCode) {},
                            onMouseDown: () {},
                            onMouseUp: () {},
                            onNavigationRequestMobile: (String url) {
                              print(url);
                              return NavigationActionPolicy.ALLOW;
                            },
                            onPaste: () {},
                            onScroll: () {}),
                        plugins: [
                          SummernoteAtMention(
                              getSuggestionsMobile: (String value) {
                                var mentions = <String>[
                                  'test1',
                                  'test2',
                                  'test3'
                                ];
                                return mentions
                                    .where((element) => element.contains(value))
                                    .toList();
                              },
                              mentionsWeb: ['test1', 'test2', 'test3'],
                              onSelect: (String value) {
                                print(value);
                              }),
                        ],
                      ),
                    ),
                    ui.vspace(20),
                    ui.UploadPhotoWidget(
                        shape: BoxShape.rectangle,
                        url: featureImage,
                        onPickImage: widget.onPickFeatureImage),
                    ui.vspace(20),
                    ui.Input(
                      controller: imageCaptionController,
                      minLine: 1,
                      maxLine: 3,
                      hintText: "Ket Gambar ....",
                      backgroundColor: Colors.white,
                    ),
                    ui.vspace(20),
                    ui.Input(
                      controller: permalinkController,
                      hintText: "permalink ....",
                      backgroundColor: Colors.white,
                    ),
                    ui.vspace(20),
                    ui.vspace(20),
                    ui.SelectBox(
                      controller: TextEditingController(
                          text: !isPublished ? "Draft" : "Terbitkan"),
                      onTap: (val) {
                        setState(() {
                          isPublished = (val == "published");
                        });
                      },
                      items: [
                        PopupMenuItem(
                          value: 'published',
                          child: Text(
                            'Terbitkan',
                            style: ui.MyTypo.bodyText1,
                          ),
                        ),
                        PopupMenuItem(
                          value: 'draft',
                          child: Text(
                            'Draft',
                            style: ui.MyTypo.bodyText1,
                          ),
                        ),
                      ],
                    ),
                    ui.vspace(20),
                    Text("Tags", style: ui.MyTypo.heading4),
                    TagEditor(
                        length: tags.length,
                        delimiters: [','],
                        hasAddButton: false,
                        inputDecoration: InputDecoration(
                          // filled: true,
                          border: InputBorder.none,
                          hintText: 'pisahkan dengan koma (,)',
                          hintStyle: ui.MyTypo.smallText,
                          // fillColor: Colors.white
                        ),
                        onTagChanged: (newValue) {
                          setState(() {
                            tags.add(newValue);
                          });
                        },
                        tagBuilder: (context, index) => Chip(
                              labelPadding: const EdgeInsets.only(left: 8.0),
                              label: Text(tags[index]),
                              deleteIcon: const Icon(
                                Icons.close,
                                size: 18,
                              ),
                              onDeleted: () {
                                print("$index");
                              },
                            ))
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
