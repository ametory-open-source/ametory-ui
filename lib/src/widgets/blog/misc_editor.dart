import 'dart:async';
import 'dart:typed_data';

import 'package:ametory_ui/ametory_ui.dart' as ui;
import 'package:file_picker/file_picker.dart';
import 'package:fluentui_icons/fluentui_icons.dart';
import 'package:flutter/material.dart';
import 'package:html2md/html2md.dart' as html2md;
import 'package:html_editor_enhanced_forked_by_ametory/html_editor.dart';
import 'package:image_picker/image_picker.dart';
import 'package:markdown/markdown.dart' as md;
import 'package:uuid/uuid.dart';

class MiscEditor extends StatefulWidget {
  final ui.MiscModel? data;
  final HtmlEditorController? controller;
  final FutureOr<bool> Function(PlatformFile file, InsertFileType type)?
      mediaUploadInterceptor;
  final Function(Uint8List data, String filename, String mimeType)?
      onMediaUploadPickFile;
  final Function(ui.MiscModel)? onSave;
  final Function(XFile?)? onPickFeatureImage;
  final ui.UserModel? userData;
  final ButtonStyle? buttonStyle;
  final String? featureImage;
  final Function()? onAddContent;
  final Function(ui.MiscContent)? onEditContent;
  final Function(ui.MiscContent)? onDelete;
  final void Function(int, int) onReorder;
  final List<ui.MiscContent> contents;
  final bool isLoadingContent;

  const MiscEditor(
      {Key? key,
      this.data,
      this.controller,
      this.mediaUploadInterceptor,
      this.onMediaUploadPickFile,
      this.onSave,
      this.onPickFeatureImage,
      this.buttonStyle,
      this.featureImage,
      this.userData,
      this.onAddContent,
      this.onDelete,
      this.isLoadingContent = false,
      required this.contents,
      required this.onReorder,
      this.onEditContent})
      : assert(userData != null, "User data cannot be null"),
        super(key: key);

  @override
  State<MiscEditor> createState() => _MiscEditorState();
}

class _MiscEditorState extends State<MiscEditor> {
  TextEditingController titleController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController urlController = TextEditingController();
  ScrollController scrollController = ScrollController();
  String result = '';
  late HtmlEditorController controller;

  bool isLoading = false;
  bool isLoadingContent = false;
  String isLoadingText = "Loading ...";
  String featureImage = "";
  ui.MiscModel? article;
  String content = "";
  List<ui.MiscContent> contents = [];
  @override
  void initState() {
    if (widget.controller != null) {
      controller = widget.controller!;
    } else {
      controller = HtmlEditorController();
    }
    print("CREATE SCREEN LOADED");
    if (widget.featureImage != null) {
      featureImage = widget.featureImage!;
    }
    if (widget.data != null) {
      article = widget.data;
      titleController.text = article!.title ?? "";
      nameController.text = article!.name ?? "";
      urlController.text = article!.url ?? "";
      featureImage = article?.cover ?? "";
      content = md.markdownToHtml(article?.description ?? "");
    }
    super.initState();
  }

  @override
  void didUpdateWidget(MiscEditor oldWidget) {
    if (widget.featureImage != oldWidget.featureImage) {
      featureImage = widget.featureImage!;
      setState(() {});
    }
    if (widget.contents != oldWidget.contents) {
      contents = widget.contents;
      print("contents $contents");
      setState(() {});
    }
    if (widget.data != oldWidget.data) {
      article = widget.data;
      print("article $article");
      setState(() {});
    }
    if (widget.isLoadingContent != oldWidget.isLoadingContent) {
      isLoadingContent = widget.isLoadingContent;
      setState(() {});
    }
    super.didUpdateWidget(oldWidget);
  }

  Widget _renderListContents() {
    if (article == null) return Container();
    if (article!.contents == null) return Container();
    return ReorderableListView(
        shrinkWrap: true,
        onReorder: widget.onReorder,
        children: [
          ...article!.contents!
              .map((e) => Card(
                    key: Key(e.id!),
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                            children: [
                              InkWell(
                                  onTap: () => widget.onDelete!(e),
                                  child: Icon(FluentSystemIcons
                                      .ic_fluent_delete_regular)),
                              ui.vspace(10),
                              InkWell(
                                  onTap: () => widget.onEditContent!(e),
                                  child: Icon(FluentSystemIcons
                                      .ic_fluent_edit_regular)),
                            ],
                          ),
                          if (e.cover != null) ui.hspace(10),
                          if (e.cover != null)
                            ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: Image.network(
                                e.cover!,
                                width: 160,
                              ),
                            ),
                          ui.hspace(10),
                          Expanded(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              if (e.title != null) Text(e.title!),
                              if (e.title != null) ui.vspace(5),
                              if (e.subtitle != null)
                                Text(
                                  e.subtitle!,
                                  style: ui.MyTypo.smallText,
                                ),
                              if (e.subtitle != null) ui.vspace(5),
                              if (e.cover != null) ui.vspace(5),
                              if (e.description != null) Text(e.description!),
                            ],
                          ))
                        ],
                      ),
                    ),
                  ))
              .toList()
        ]);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: double.infinity,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(),
                      TextButton(
                          onPressed: () async {
                            if (titleController.text.isEmpty) {
                              ui.flush(context, "Judul tidak boleh kosong");
                              return;
                            }

                            final content = await controller.getText();
                            ui.MiscModel? data;

                            if (article != null) {
                              data = ui.MiscModel(
                                  id: article!.id,
                                  title: titleController.text,
                                  name: nameController.text,
                                  url: urlController.text,
                                  description: html2md.convert(content),
                                  user: article!.user,
                                  modifiedBy: widget.userData,
                                  created: article!.created,
                                  modified: DateTime.now(),
                                  cover: featureImage);
                            } else {
                              data = ui.MiscModel(
                                  id: const Uuid().v4(),
                                  title: titleController.text,
                                  name: nameController.text,
                                  url: urlController.text,
                                  description: html2md.convert(content),
                                  user: widget.userData,
                                  created: DateTime.now(),
                                  modified: DateTime.now(),
                                  cover: featureImage);
                            }

                            if (widget.onSave != null) {
                              widget.onSave!(data);
                            }
                          },
                          style: widget.buttonStyle ?? ui.MyStyle.buttonDefault,
                          child: const Text("Simpan & Kirim")),
                    ],
                  ),
                  ui.Input(
                    hintText: "Judul ...",
                    backgroundColor: Colors.white,
                    controller: titleController,
                    textFieldStyle: ui.MyTypo.heading1
                        .copyWith(fontWeight: FontWeight.w300),
                    onChanged: (val) {},
                  ),
                  Expanded(
                      child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(),
                            TextButton.icon(
                                onPressed: widget.onAddContent,
                                icon: const Icon(FluentSystemIcons
                                    .ic_fluent_add_circle_regular),
                                label: const Text("Tambah Konten"))
                          ],
                        ),
                        ui.vspace(20),
                        _renderListContents(),
                      ],
                    ),
                  )),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Container(
                color: Colors.black.withOpacity(0.03),
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 300,
                      child: HtmlEditor(
                        controller: controller,
                        htmlEditorOptions: HtmlEditorOptions(
                          hint: 'Tulis artikel disini...',
                          shouldEnsureVisible: true,
                          initialText: content,
                        ),
                        htmlToolbarOptions: HtmlToolbarOptions(
                          defaultToolbarButtons: const [
                            StyleButtons(),
                            FontSettingButtons(
                                fontSizeUnit: false,
                                fontName: false,
                                fontSize: false),
                            FontButtons(
                                clearAll: false,
                                subscript: false,
                                superscript: false),
                            // ColorButtons(),
                            ListButtons(listStyles: false),
                            ParagraphButtons(
                                textDirection: false,
                                lineHeight: false,
                                caseConverter: false),
                            InsertButtons(
                                video: false,
                                audio: false,
                                table: true,
                                hr: true,
                                otherFile: false)
                          ],
                          toolbarPosition:
                              ToolbarPosition.aboveEditor, //by default
                          toolbarType:
                              ToolbarType.nativeScrollable, //by default
                          onButtonPressed: (ButtonType type, bool? status,
                              Function? updateStatus) {
                            return true;
                          },
                          onDropdownChanged: (DropdownType type,
                              dynamic changed,
                              Function(dynamic)? updateSelectedItem) {
                            return true;
                          },
                          mediaLinkInsertInterceptor:
                              (String url, InsertFileType type) {
                            return true;
                          },
                          mediaUploadInterceptor: widget
                                  .mediaUploadInterceptor ??
                              (PlatformFile file, InsertFileType type) async {
                                if (widget.onMediaUploadPickFile != null) {
                                  widget.onMediaUploadPickFile!(file.bytes!,
                                      file.name, "image/${file.extension}");
                                  return false;
                                }
                                // final res = await uploadToStorage(file.bytes!, file.name, "image/${file.extension}");
                                // // print(getStorageUrl(context, res));
                                // controller.insertNetworkImage(getStorageUrl(context, res), filename: file.name);
                                return true;
                              },
                        ),
                        otherOptions: const OtherOptions(height: 550),
                        callbacks: Callbacks(
                            onBeforeCommand: (String? currentHtml) {},
                            onChangeContent: (String? changed) {},
                            onChangeCodeview: (String? changed) {},
                            onChangeSelection: (EditorSettings settings) {},
                            onDialogShown: () {},
                            onEnter: () {},
                            onFocus: () {
                              if (controller.characterCount == 0) {
                                controller.execCommand('fontName',
                                    argument: 'Georgia');
                              }
                            },
                            onBlur: () {},
                            onBlurCodeview: () {},
                            onInit: () {},
                            onImageUploadError: (FileUpload? file,
                                String? base64Str, UploadError error) {
                              print("${error}");
                              print(base64Str ?? '');
                              if (file != null) {
                                print(file.name);
                                print("${file.size}");
                                print(file.type);
                              }
                            },
                            onKeyDown: (int? keyCode) {},
                            onKeyUp: (int? keyCode) {},
                            onMouseDown: () {},
                            onMouseUp: () {},
                            onNavigationRequestMobile: (String url) {
                              print(url);
                              return NavigationActionPolicy.ALLOW;
                            },
                            onPaste: () {},
                            onScroll: () {}),
                        plugins: [
                          SummernoteAtMention(
                              getSuggestionsMobile: (String value) {
                                var mentions = <String>[
                                  'test1',
                                  'test2',
                                  'test3'
                                ];
                                return mentions
                                    .where((element) => element.contains(value))
                                    .toList();
                              },
                              mentionsWeb: ['test1', 'test2', 'test3'],
                              onSelect: (String value) {
                                print(value);
                              }),
                        ],
                      ),
                    ),
                    ui.vspace(20),
                    ui.Input(
                      controller: nameController,
                      hintText: "Nama ....",
                      backgroundColor: Colors.white,
                    ),
                    ui.vspace(20),
                    ui.Input(
                      controller: urlController,
                      hintText: "URL ....",
                      backgroundColor: Colors.white,
                    ),
                    ui.vspace(20),
                    ui.UploadPhotoWidget(
                        shape: BoxShape.rectangle,
                        url: featureImage,
                        onPickImage: widget.onPickFeatureImage),
                    ui.vspace(20),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
