import 'package:ametory_ui/src/styles/typo.dart';
import 'package:ametory_ui/src/utils/helper/layout.dart';
import 'package:ametory_ui/src/widgets/blog/image.dart';
import 'package:ametory_ui/src/widgets/blog/link.dart';
import 'package:ametory_ui/src/widgets/blog/text.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../models/blog/blog_model.dart';
import '../../utils/helper/responsive.dart';
import 'blog_response_widget.dart';

class BlogListWidget extends StatelessWidget {
  final BlogModel data;
  final Function()? onTap;
  final Function(String)? onTapTag;
  final bool isLast;

  const BlogListWidget({
    Key? key,
    required this.data,
    this.onTap,
    this.isLast = false,
    this.onTapTag,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlogLink(
      text: data.title!,
      href: "/blog/${data.permalink}/${data.id}",
      child: Center(
        child: Container(
          constraints: const BoxConstraints(maxWidth: 800),
          child: InkWell(
            hoverColor: Colors.transparent,
            focusColor: Colors.transparent,
            onTap: onTap,
            child: Container(
              margin: EdgeInsets.only(bottom: isLast ? 0 : 20),
              padding: const EdgeInsets.only(bottom: 40),
              decoration: BoxDecoration(border: isLast ? null : const Border(bottom: BorderSide(color: Colors.black12, width: 0.5))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  BlogText(
                    DateFormat("dd MMM yyyy").format(data.created!),
                    textStyle: MyTypo.smallText.copyWith(color: Colors.grey),
                  ),
                  ConstrainedBox(
                    constraints: const BoxConstraints(maxWidth: 700),
                    child: BlogText(
                      data.title!,
                      seoHeadingStyle: 3,
                      textStyle: MyTypo.heading2,
                      selectable: false,
                    ),
                  ),
                  vspace(20),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 3,
                        child: BlogText(
                          data.excerpt!,
                          textStyle: const TextStyle().copyWith(height: 1.8, fontFamily: "Georgia", color: const Color(0xFF555555)),
                        ),
                      ),
                      hspace(ResponsiveWidget.isSmallScreen(context) ? 10 : 40),
                      Flexible(
                        flex: 1,
                        child: Padding(
                          padding: EdgeInsets.all(ResponsiveWidget.isSmallScreen(context) ? 10 : 40.0),
                          child: AspectRatio(
                            aspectRatio: 1,
                            child: BlogImage(
                              altText: data.title!,
                              src: data.image!,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  vspace(40),
                  Row(
                      children: data.tags!
                          .map((tag) => BlogLink(
                                text: tag,
                                href: "/tag/$tag",
                                child: InkWell(
                                  hoverColor: Colors.transparent,
                                  focusColor: Colors.transparent,
                                  onTap: onTapTag == null ? null : () => onTapTag!(tag),
                                  child: Container(
                                    margin: const EdgeInsets.only(right: 10),
                                    padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(15), color: Colors.black12),
                                    child: BlogText(
                                      tag,
                                      textStyle: MyTypo.bodyText1,
                                      selectable: false,
                                    ),
                                  ),
                                ),
                              ))
                          .toList()),
                  vspace(10),
                  BlogResponseWidget(
                    data: data,
                    onTapComment: () {},
                    onTapLike: () {},
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
