import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ametory_ui/ametory_ui.dart' as ui;

class BlogCardWidget extends StatelessWidget {
  final ui.BlogModel data;
  final Function()? onTap;
  final Function(String)? onTapMenu;
  final double? elevation;
  final Widget? child;
  const BlogCardWidget({Key? key, this.onTap, this.onTapMenu, this.child, required this.data, this.elevation}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      hoverColor: Colors.transparent,
      focusColor: Colors.transparent,
      child: Card(
        elevation: elevation,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Container(
            width: 300,
            height: 360,
            child: Column(
              children: [
                Expanded(
                    child: Stack(
                  children: [
                    ClipRRect(
                      borderRadius: const BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                      child: Image.network(
                        data.image!.isEmpty ? "https://dummyimage.com/640x360/000/fff" : data.image!,
                        fit: BoxFit.cover,
                        width: double.infinity,
                      ),
                    ),
                    Positioned(
                        bottom: 10,
                        right: 10,
                        child: Tooltip(
                          message: data.user!.name,
                          child: ui.Initicon(
                            borderColor: Colors.white,
                            borderWidth: 5,
                            backgroundImageUrl: data.user?.avatar ?? "",
                            text: data.user!.name,
                          ),
                        )),
                    if (data.published!)
                      Positioned(
                          top: 10,
                          left: 10,
                          child: Tooltip(
                            message: data.user!.name,
                            child: Chip(
                              label: Text(
                                "Published",
                                style: ui.MyTypo.smallText.copyWith(color: Colors.white),
                              ),
                              backgroundColor: Colors.green,
                            ),
                          )),
                    Positioned(
                        top: 10,
                        right: 10,
                        child: Container(
                          padding: const EdgeInsets.all(5),
                          decoration: const BoxDecoration(shape: BoxShape.circle, color: Colors.white54),
                          child: InkWell(
                            hoverColor: Colors.transparent,
                            child: const Icon(
                              Icons.more_vert_outlined,
                              size: 14,
                            ),
                            onTapDown: (details) async {
                              String? res = await ui.popUpMenu(context, offset: details.globalPosition, items: [
                                PopupMenuItem(
                                  value: "view",
                                  child: Text(
                                    "View",
                                    style: ui.MyTypo.bodyText1,
                                  ),
                                ),
                                PopupMenuItem(
                                  value: "edit",
                                  child: Text(
                                    "Edit",
                                    style: ui.MyTypo.bodyText1,
                                  ),
                                ),
                                PopupMenuItem(
                                  value: "delete",
                                  child: Text(
                                    "Hapus",
                                    style: ui.MyTypo.bodyText1,
                                  ),
                                ),
                              ]);
                              if (res != null) {
                                if (onTapMenu != null) onTapMenu!(res);
                              }
                            },
                          ),
                        )),
                  ],
                )),
                Expanded(
                    child: Container(
                  width: double.infinity,
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            DateFormat("dd MMM yyyy").format(data.created!),
                            style: ui.MyTypo.smallText.copyWith(color: Colors.grey),
                          ),
                          Text(
                            data.title!,
                            style: ui.MyTypo.heading3,
                          )
                        ],
                      ),
                      child ?? Container(),
                    ],
                  ),
                ))
              ],
            )),
      ),
    );
  }
}
