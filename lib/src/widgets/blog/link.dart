import 'package:flutter/material.dart';
import 'package:seo_renderer/renderers/link_renderer/link_renderer_vm.dart';

class BlogLink extends StatelessWidget {
  final String text;
  final String href;
  final Widget child;
  const BlogLink(
      {Key? key, required this.text, required this.href, required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LinkRenderer(
      text: text,
      href: href,
      child: child,
    );
  }
}
