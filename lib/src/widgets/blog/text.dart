import 'package:ametory_ui/ametory_ui.dart';
import 'package:flutter/material.dart';
import 'package:seo_renderer/seo_renderer.dart';
import 'dart:ui' as ui show TextHeightBehavior;

class BlogText extends StatelessWidget {
  final String text;
  final bool selectable;
  final TextStyle? textStyle;
  final StrutStyle? strutStyle;
  final TextAlign? textAlign;
  final TextDirection? textDirection;
  final bool? softWrap;
  final TextOverflow? overflow;
  final double? textScaleFactor;
  final int? maxLines;
  final String? semanticsLabel;
  final TextWidthBasis? textWidthBasis;
  final ui.TextHeightBehavior? textHeightBehavior;
  final Color? selectionColor;
  final int seoHeadingStyle;

  const BlogText(this.text,
      {Key? key,
      this.selectable = true,
      this.textStyle,
      this.strutStyle,
      this.textAlign,
      this.textDirection,
      this.softWrap,
      this.overflow,
      this.textScaleFactor,
      this.maxLines,
      this.semanticsLabel,
      this.textWidthBasis,
      this.textHeightBehavior,
      this.seoHeadingStyle = 0,
      this.selectionColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextRendererStyle? _headingStyle;
    switch (seoHeadingStyle) {
      case 1:
        _headingStyle = TextRendererStyle.header1;
        break;
      case 2:
        _headingStyle = TextRendererStyle.header2;
        break;
      case 3:
        _headingStyle = TextRendererStyle.header3;
        break;
      case 4:
        _headingStyle = TextRendererStyle.header4;
        break;
      case 5:
        _headingStyle = TextRendererStyle.header5;
        break;
      case 6:
        _headingStyle = TextRendererStyle.header6;
        break;
      default:
        _headingStyle = TextRendererStyle.paragraph;
    }
    return TextRenderer(
      text: text,
      style: _headingStyle,
      child: selectable
          ? SelectableText(
              text,
              style: textStyle ?? MyTypo.bodyText1,
              strutStyle: strutStyle,
              textAlign: textAlign,
              textDirection: textDirection,
              textScaleFactor: textScaleFactor,
              maxLines: maxLines,
              semanticsLabel: semanticsLabel,
              textWidthBasis: textWidthBasis,
              textHeightBehavior: textHeightBehavior,
            )
          : Text(
              text,
              style: textStyle ?? MyTypo.bodyText1,
              strutStyle: strutStyle,
              textAlign: textAlign,
              textDirection: textDirection,
              softWrap: softWrap,
              overflow: overflow,
              textScaleFactor: textScaleFactor,
              maxLines: maxLines,
              semanticsLabel: semanticsLabel,
              textWidthBasis: textWidthBasis,
              textHeightBehavior: textHeightBehavior,
              selectionColor: selectionColor,
            ),
    );
  }
}
