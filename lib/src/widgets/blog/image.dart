import 'package:flutter/material.dart';
import 'package:seo_renderer/seo_renderer.dart';

class BlogImage extends StatelessWidget {
  final String altText;
  final String src;
  final ImageFrameBuilder? frameBuilder;
  final ImageLoadingBuilder? loadingBuilder;
  final ImageErrorWidgetBuilder? errorBuilder;
  final double? width;
  final double? height;
  final Color? color;
  final Animation<double>? opacity;
  final FilterQuality filterQuality;
  final BlendMode? colorBlendMode;
  final BoxFit? fit;
  final AlignmentGeometry alignment;
  final ImageRepeat repeat;
  final Rect? centerSlice;
  final bool matchTextDirection;
  final bool gaplessPlayback;
  final String? semanticLabel;
  final bool excludeFromSemantics;
  final bool isAntiAlias;

  const BlogImage(
      {Key? key,
      required this.altText,
      required this.src,
      this.frameBuilder,
      this.loadingBuilder,
      this.errorBuilder,
      this.width,
      this.height,
      this.color,
      this.opacity,
      this.filterQuality = FilterQuality.low,
      this.colorBlendMode,
      this.fit,
      this.alignment = Alignment.center,
      this.repeat = ImageRepeat.noRepeat,
      this.centerSlice,
      this.matchTextDirection = false,
      this.gaplessPlayback = false,
      this.semanticLabel,
      this.excludeFromSemantics = false,
      this.isAntiAlias = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ImageRenderer(
      alt: altText,
      child: Image.network(
        src,
        frameBuilder: frameBuilder,
        loadingBuilder: loadingBuilder,
        errorBuilder: errorBuilder,
        width: width,
        height: height,
        color: color,
        opacity: opacity,
        filterQuality: filterQuality,
        colorBlendMode: colorBlendMode,
        fit: fit,
        alignment: alignment,
        repeat: repeat,
        centerSlice: centerSlice,
        matchTextDirection: matchTextDirection,
        gaplessPlayback: gaplessPlayback,
        semanticLabel: semanticLabel,
        excludeFromSemantics: excludeFromSemantics,
        isAntiAlias: isAntiAlias,
      ),
    );
  }
}
