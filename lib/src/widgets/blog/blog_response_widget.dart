import 'package:flutter/material.dart';
import 'package:fluentui_icons/fluentui_icons.dart';

import '../../../ametory_ui.dart';

class BlogResponseWidget extends StatelessWidget {
  final BlogModel data;
  final Function()? onTapLike;
  final Function()? onTapComment;
  final double? iconSize;
  final String? likeTooltip;
  final String? commentTooltip;

  const BlogResponseWidget({
    Key? key,
    required this.data,
    this.onTapLike,
    this.onTapComment,
    this.iconSize,
    this.likeTooltip,
    this.commentTooltip,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Tooltip(
          message: likeTooltip ?? "Suka artikel ini",
          child: InkWell(
            hoverColor: Colors.transparent,
            focusColor: Colors.transparent,
            onTap: onTapLike,
            child: Row(
              children: [
                if (data.totalLikes! > 0) Text("${data.totalLikes!}"),
                hspace(5),
                Icon(
                  FluentSystemIcons.ic_fluent_thumb_like_regular,
                  color: Colors.black87,
                  size: iconSize,
                ),
              ],
            ),
          ),
        ),
        hspace(20),
        Tooltip(
          message: commentTooltip ?? (data.commentActive! ? "Beri komentar artikel" : "Komentar dinonaktifkan"),
          child: InkWell(
            hoverColor: Colors.transparent,
            focusColor: Colors.transparent,
            onTap: data.commentActive! ? onTapComment : null,
            child: Row(
              children: [
                if (data.totalComments! > 0) Text("${data.totalComments!}"),
                hspace(5),
                Icon(
                  FluentSystemIcons.ic_fluent_comment_regular,
                  color: data.commentActive! ? Colors.black87 : Colors.black12,
                  size: iconSize,
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
