import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../models/blog/blog_model.dart';
import '../form/input.dart';

class SearchContentWidget extends StatefulWidget {
  final Function(BlogModel)? onTap;
  SearchContentWidget({Key? key, this.onTap}) : super(key: key);

  @override
  State<SearchContentWidget> createState() => _SearchContentWidgetState();
}

class _SearchContentWidgetState extends State<SearchContentWidget> {
  Timer? timeHandle;
  List<BlogModel> contents = [];
  bool isLoadingContent = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    timeHandle?.cancel();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant SearchContentWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  searchContent(String search) async {
    setState(() {
      isLoadingContent = true;
    });

    final res = await FirebaseFirestore.instance
        .collection("blogs")
        .withConverter(
          fromFirestore: (snapshot, _) => BlogModel.fromJson(snapshot.data()!),
          toFirestore: (BlogModel data, _) => data.toJson(),
        )
        .where("keywords", arrayContainsAny: search.split(" ").map((e) => e.toLowerCase()).toList())
        .get();

    setState(() {
      contents = res.docs.map((e) => e.data()).toList();
      isLoadingContent = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Input(
            hintText: "Search Content...",
            onChanged: (p) {
              if (timeHandle != null) {
                timeHandle?.cancel();
              }
              timeHandle = Timer(const Duration(seconds: 1), () async {
                searchContent(p);
              });
            },
          ),
          if (isLoadingContent)
            SizedBox(
              height: 200,
              child: Center(
                child: CupertinoActivityIndicator(),
              ),
            ),
          ...contents.map((e) => Card(
                child: ListTile(
                    onTap: () {
                      if (widget.onTap != null) widget.onTap!(e);
                    },
                    title: Text(e.title!)),
              ))
        ],
      ),
    );
  }
}
