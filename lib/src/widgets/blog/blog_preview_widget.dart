import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher_string.dart';

import '../../../ametory_ui.dart';

class BlogPreviewWidget extends StatelessWidget {
  final BlogModel? data;
  final Function()? onTap;
  final Widget? child;
  final bool disableDate;
  final bool disableAuthor;
  final bool disableTitle;

  const BlogPreviewWidget({
    Key? key,
    this.data,
    this.onTap,
    this.child,
    this.disableDate = false,
    this.disableTitle = false,
    this.disableAuthor = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return data == null
        ? Container()
        : SingleChildScrollView(
            child: Center(
              child: Container(
                constraints: BoxConstraints(maxWidth: ResponsiveWidget.isSmallScreen(context) ? MediaQuery.of(context).size.width - 40 : 800),
                child: Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: [
                  vspace(40),
                  if (!disableDate)
                    BlogText(
                      DateFormat("dd MMM yyyy").format(data!.created!),
                      textStyle: MyTypo.smallText.copyWith(color: Colors.grey),
                    ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      if (disableTitle) Container(),
                      if (!disableTitle)
                        Expanded(
                          child: BlogText(
                            data!.title!,
                            seoHeadingStyle: 3,
                            textStyle: MyTypo.heading1,
                            selectable: false,
                          ),
                        ),
                      if (data!.user != null && !disableAuthor)
                        Tooltip(
                          message: data!.user!.name,
                          child: Initicon(
                            size: ResponsiveWidget.isSmallScreen(context) ? 30 : 45,
                            backgroundImageUrl: data!.user?.avatar ?? "",
                            text: data!.user!.name,
                          ),
                        ),
                    ],
                  ),
                  vspace(20),
                  Wrap(
                      children: data!.tags!
                          .map((tag) => BlogLink(
                                text: tag,
                                href: "/tag/$tag",
                                child: InkWell(
                                  hoverColor: Colors.transparent,
                                  focusColor: Colors.transparent,
                                  onTap: onTap,
                                  child: Container(
                                    margin: const EdgeInsets.only(right: 10),
                                    padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(15), color: Colors.black12),
                                    child: BlogText(
                                      tag,
                                      textStyle: MyTypo.bodyText1,
                                      selectable: false,
                                    ),
                                  ),
                                ),
                              ))
                          .toList()),
                  if (data!.image != null && data!.image!.isNotEmpty) vspace(40),
                  if (data!.image != null && data!.image!.isNotEmpty)
                    SizedBox(
                      width: double.infinity,
                      child: BlogImage(
                        altText: data!.title!,
                        src: data!.image!,
                        fit: BoxFit.cover,
                      ),
                    ),
                  if (data!.imageCaption != null) vspace(5),
                  if (data!.imageCaption != null)
                    BlogText(
                      data!.imageCaption!,
                      selectable: true,
                      seoHeadingStyle: 6,
                      textStyle: TextStyle().copyWith(fontStyle: FontStyle.italic),
                    ),
                  if (data!.image != null) vspace(40),
                  vspace(20),
                  BlogMarkdown(
                    data!.description!,
                    fontFamily: "Georgia",
                    onTapLink: (text, href, title) {
                      launchUrlString(href!);
                    },
                  ),
                  vspace(40),
                  child ?? Container()
                ]),
              ),
            ),
          );
  }
}
