import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class MediaPlayer extends StatelessWidget {
  final Function(VideoPlayerController)? onTap;
  final String videoUrl;
  const MediaPlayer({Key? key, required this.videoUrl, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = VideoPlayerController.networkUrl(Uri.parse(videoUrl),
        videoPlayerOptions: VideoPlayerOptions(
          allowBackgroundPlayback: true,
        ));
    controller.initialize().then((_) {
      // if (!controller.value.isPlaying) controller.play();
    });

    return Stack(
      children: [
        VideoPlayer(controller),
        InkWell(onTap: () {
          if (onTap != null) {
            onTap!(controller);
          }
        }),
      ],
    );
  }
}
