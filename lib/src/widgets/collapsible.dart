import 'package:ametory_ui/ametory_ui.dart';
import 'package:fluentui_icons/fluentui_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class Collapsible extends StatefulWidget {
  final double expandedHeight;
  final int index;
  final String label;
  final TextStyle? labelStyle;
  final double? elevation;
  final double? borderWidth;
  final Color? borderColor;
  final Color? iconColor;
  final IconData? collapsedIcon;
  final IconData? expandedIcon;
  final Widget? child;
  final double? borderRadius;
  final Color? labelBackgroundColor;
  final Color? backgroundColor;
  final bool isCollapsed;
  final bool isGrouped;
  final Duration? duration;
  CollapsibleMode mode;
  Function(bool, bool)? onCollapsed;

  Collapsible(
      {Key? key,
      this.expandedHeight = 200,
      this.label = "",
      this.child,
      this.elevation,
      this.borderRadius,
      this.borderWidth,
      this.borderColor,
      this.collapsedIcon,
      this.expandedIcon,
      this.isCollapsed = true,
      this.isGrouped = false,
      this.mode = CollapsibleMode.single,
      this.iconColor,
      this.duration,
      this.labelBackgroundColor,
      this.backgroundColor,
      this.labelStyle,
      this.onCollapsed,
      this.index = 0})
      : super(key: key);

  @override
  State<Collapsible> createState() => _CollapsibleState();

  Collapsible copyWith({
    double? expandedHeight,
    int? index,
    String? label,
    TextStyle? labelStyle,
    double? elevation,
    double? borderWidth,
    Color? borderColor,
    Color? iconColor,
    IconData? collapsedIcon,
    IconData? expandedIcon,
    Widget? child,
    double? borderRadius,
    Color? labelBackgroundColor,
    Color? backgroundColor,
    bool? isCollapsed,
    bool? isGrouped,
    Duration? duration,
    CollapsibleMode? mode,
    Function(bool, bool)? onCollapsed,
  }) {
    return Collapsible(
      expandedHeight: expandedHeight ?? this.expandedHeight,
      index: index ?? this.index,
      label: label ?? this.label,
      labelStyle: labelStyle ?? this.labelStyle,
      elevation: elevation ?? this.elevation,
      borderWidth: borderWidth ?? this.borderWidth,
      borderColor: borderColor ?? this.borderColor,
      iconColor: iconColor ?? this.iconColor,
      collapsedIcon: collapsedIcon ?? this.collapsedIcon,
      expandedIcon: expandedIcon ?? this.expandedIcon,
      borderRadius: borderRadius ?? this.borderRadius,
      labelBackgroundColor: labelBackgroundColor ?? this.labelBackgroundColor,
      backgroundColor: backgroundColor ?? this.backgroundColor,
      isCollapsed: isCollapsed ?? this.isCollapsed,
      isGrouped: isGrouped ?? this.isGrouped,
      duration: duration ?? this.duration,
      mode: mode ?? this.mode,
      onCollapsed: onCollapsed ?? this.onCollapsed,
      child: child ?? this.child,
    );
  }
}

class _CollapsibleState extends State<Collapsible> {
  bool isCollapsed = false;
  bool isCollapsing = false;
  late CollapsibleMode mode;

  @override
  void initState() {
    isCollapsed = widget.isCollapsed;
    mode = widget.mode;
    super.initState();
  }

  @override
  void didUpdateWidget(Collapsible oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.isCollapsed != oldWidget.isCollapsed) {
      SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
        setState(() {
          isCollapsed = widget.isCollapsed;
          isCollapsing = true;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    BorderRadius wrapperRadius = BorderRadius.circular(widget.borderRadius ?? 10);
    BorderRadius labelRadius = BorderRadius.only(
        topLeft: Radius.circular(widget.borderRadius ?? 10),
        topRight: Radius.circular(widget.borderRadius ?? 10),
        bottomLeft: Radius.circular(!isCollapsed || isCollapsing ? 0 : (widget.borderRadius ?? 10)),
        bottomRight: Radius.circular(!isCollapsed || isCollapsing ? 0 : (widget.borderRadius ?? 10)));
    switch (mode) {
      case CollapsibleMode.middle:
        wrapperRadius = BorderRadius.circular(0);
        labelRadius = BorderRadius.circular(0);
        break;
      case CollapsibleMode.first:
        wrapperRadius = BorderRadius.only(
          topLeft: Radius.circular(widget.borderRadius ?? 10),
          topRight: Radius.circular(widget.borderRadius ?? 10),
        );
        labelRadius = BorderRadius.only(
            topLeft: Radius.circular(widget.borderRadius ?? 10),
            topRight: Radius.circular(widget.borderRadius ?? 10),
            bottomLeft: const Radius.circular(0),
            bottomRight: const Radius.circular(0));
        break;
      case CollapsibleMode.last:
        wrapperRadius = BorderRadius.only(
          bottomLeft: Radius.circular(widget.borderRadius ?? 10),
          bottomRight: Radius.circular(widget.borderRadius ?? 10),
        );
        labelRadius = BorderRadius.only(
            bottomLeft: Radius.circular(!isCollapsed || isCollapsing ? 0 : (widget.borderRadius ?? 10)),
            bottomRight: Radius.circular(!isCollapsed || isCollapsing ? 0 : (widget.borderRadius ?? 10)),
            topLeft: const Radius.circular(0),
            topRight: const Radius.circular(0));
        break;
      default:
    }
    return Card(
      margin: const EdgeInsets.all(0),
      elevation: widget.elevation ?? 0.5,
      shape: RoundedRectangleBorder(borderRadius: wrapperRadius),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(width: widget.borderWidth ?? 0.5, color: widget.borderColor ?? Colors.black.withOpacity(0.12)),
            borderRadius: wrapperRadius),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: labelRadius,
              child: Container(
                decoration: BoxDecoration(
                    color: widget.labelBackgroundColor ?? Colors.black.withOpacity(0.03),
                    border: Border(
                      bottom: mode == CollapsibleMode.single || mode == CollapsibleMode.last || !isCollapsed
                          ? BorderSide(width: widget.borderWidth ?? 0.5, color: widget.borderColor ?? Colors.black.withOpacity(0.12))
                          : BorderSide.none,
                    )),
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(widget.label, style: widget.labelStyle ?? MyTypo.bodyText1.copyWith(fontWeight: FontWeight.w600)),
                    InkWell(
                        onTap: () {
                          if (widget.isGrouped) {
                            if (widget.onCollapsed != null) {
                              widget.onCollapsed!(!isCollapsed, isCollapsing);
                            }
                          } else {
                            setState(() {
                              isCollapsing = true;
                              isCollapsed = !isCollapsed;
                            });
                          }
                        },
                        child: AnimatedRotation(
                          turns: isCollapsed ? 0 : 0.25,
                          duration: widget.duration ?? const Duration(milliseconds: 300),
                          onEnd: () {
                            setState(() {
                              isCollapsing = false;
                              if (!widget.isGrouped) {
                                if (widget.onCollapsed != null) {
                                  widget.onCollapsed!(isCollapsed, isCollapsing);
                                }
                              }
                            });
                          },
                          child: Icon(
                            widget.expandedIcon ?? FluentSystemIcons.ic_fluent_chevron_right_regular,
                            size: 14,
                            color: widget.iconColor ?? Colors.grey,
                          ),
                        ))
                  ],
                ),
              ),
            ),
            AnimatedContainer(
                color: widget.backgroundColor,
                onEnd: () {
                  setState(() {
                    isCollapsing = false;
                  });
                },
                duration: widget.duration ?? const Duration(milliseconds: 300),
                height: isCollapsed ? 0 : widget.expandedHeight,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: widget.child ?? Container(),
                ))
          ],
        ),
      ),
    );
  }
}

enum CollapsibleMode { single, first, middle, last }

class CollapsibleGroup extends StatefulWidget {
  final bool isToggleCollapse;
  final List<Collapsible> children;
  const CollapsibleGroup({Key? key, required this.children, this.isToggleCollapse = true}) : super(key: key);

  @override
  State<CollapsibleGroup> createState() => _CollapsibleGroupState();
}

class _CollapsibleGroupState extends State<CollapsibleGroup> {
  List<bool> collapsed = [];
  late List<Collapsible> children;
  @override
  void initState() {
    children = widget.children;

    collapsed = List.generate(widget.children.length, (index) => true);
    super.initState();
  }

  onCollapsed(int index, bool isCollapsed, bool isCollapsing) {
    collapsed[index] = isCollapsed;
    if (widget.isToggleCollapse) {
      for (var i = 0; i < collapsed.length; i++) {
        if ((index != i)) {
          collapsed[i] = true;
        }
      }
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ...children.map((e) {
          var index = widget.children.indexOf(e);

          if (widget.children.first == e) {
            e.mode = CollapsibleMode.first;
          } else if (widget.children.last == e) {
            e.mode = CollapsibleMode.last;
          } else {
            e.mode = CollapsibleMode.middle;
          }

          e.onCollapsed = (isCollapsed, isCollapsing) => onCollapsed(index, isCollapsed, isCollapsing);
          return e.copyWith(index: index, isCollapsed: collapsed[index], isGrouped: true);
        }).toList()
      ],
    );
  }
}
