import 'package:file_picker/file_picker.dart';
import 'package:image_picker/image_picker.dart';

class MediaPicker {
  static Future<XFile?> get image async {
    final ImagePicker picker = ImagePicker();
    return await picker.pickImage(source: ImageSource.gallery);
  }

  static Future<XFile?> get camera async {
    final ImagePicker picker = ImagePicker();
    return await picker.pickImage(source: ImageSource.camera);
  }

  static Future<XFile?> get video async {
    final ImagePicker picker = ImagePicker();
    return await picker.pickVideo(source: ImageSource.gallery);
  }

  static Future<XFile?> get videoRecord async {
    final ImagePicker picker = ImagePicker();
    return await picker.pickVideo(source: ImageSource.camera);
  }

  static Future<FilePickerResult?> get file async {
    return await FilePicker.platform.pickFiles(allowMultiple: false);
  }

  static Future<FilePickerResult?> get fileMultiple async {
    return await FilePicker.platform.pickFiles(allowMultiple: true);
  }
}
