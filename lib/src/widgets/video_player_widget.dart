import 'package:video_player/video_player.dart';
import 'package:flutter/material.dart';

class VideoPlayeWidget extends StatefulWidget {
  final String videoUrl;

  const VideoPlayeWidget({Key? key, required this.videoUrl}) : super(key: key);
  @override
  State<VideoPlayeWidget> createState() => _VideoPlayeWidgetState();
}

class _VideoPlayeWidgetState extends State<VideoPlayeWidget> {
  VideoPlayerController? _controller;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.networkUrl(Uri.parse(widget.videoUrl) , videoPlayerOptions: VideoPlayerOptions(allowBackgroundPlayback: true))
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
      });

    WidgetsBinding.instance.addPostFrameCallback((_) => _controller?.play());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: Colors.transparent, iconTheme: const IconThemeData(color: Colors.white)),
      backgroundColor: Colors.black,
      body: SafeArea(
        child: Stack(
          children: [
            Center(
              child: _controller!.value.isInitialized
                  ? AspectRatio(
                      aspectRatio: _controller!.value.aspectRatio,
                      child: VideoPlayer(_controller!),
                    )
                  : Container(),
            ),
            Positioned(
                bottom: 10,
                left: 0,
                right: 0,
                child: FloatingActionButton(
                  onPressed: () {
                    setState(() {
                      _controller!.value.isPlaying ? _controller?.pause() : _controller?.play();
                    });
                  },
                  child: Icon(
                    _controller!.value.isPlaying ? Icons.pause : Icons.play_arrow,
                  ),
                ))
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller?.dispose();
  }
}
