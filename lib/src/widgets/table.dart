import 'package:fluentui_icons/fluentui_icons.dart';
import 'package:flutter/material.dart';

import '../../ametory_ui.dart';

class MyTable extends StatefulWidget {
  final List<String> headers;
  final BoxDecoration? headerDecoration;
  final BoxDecoration? bodyDecoration;
  final dynamic headerWidth;
  final List<List<Widget>> body;
  final TableCellVerticalAlignment valign;
  final Map<int, TableColumnWidth>? columnWidths;
  final TextStyle? headerStyle;
  final Color? eventColor;
  final Color? oddColor;
  final int totalPages;
  final int totalRecords;
  final int prev;
  final int next;
  final int page;
  final int limit;
  final EdgeInsets? headerPadding;
  final EdgeInsets? bodyPadding;
  final List<TableCell>? beforeBody;
  final List<TableCell>? footer;
  final String paginationLabel;
  final String title;
  final Function()? onSearchClear;
  final Function(int)? onTapPagination;
  final Function(String)? onSearchSubmitted;
  final Function(String)? onSearchChanged;
  final Function(List<int>)? onTapEditCollection;
  final Function(List<int>)? onTapDeleteCollection;
  final Function(int)? onTapEdit;
  final Function(int)? onTapDelete;
  final bool headerAsFooter;
  final bool showPagination;
  final bool showResult;
  final bool showToolbar;
  final TextEditingController? searchController;
  const MyTable({
    Key? key,
    required this.headers,
    this.valign = TableCellVerticalAlignment.middle,
    this.columnWidths,
    this.headerWidth,
    this.headerStyle,
    this.bodyDecoration,
    this.headerDecoration,
    required this.body,
    this.eventColor,
    this.oddColor,
    this.totalPages = 1,
    this.totalRecords = 1,
    this.prev = 0,
    this.next = 1,
    this.page = 1,
    this.limit = 20,
    this.onTapPagination,
    this.paginationLabel = "",
    this.title = "",
    this.headerPadding,
    this.bodyPadding,
    this.beforeBody,
    this.footer,
    this.headerAsFooter = false,
    this.showPagination = false,
    this.showResult = false,
    this.showToolbar = false,
    this.onSearchClear,
    this.onSearchSubmitted,
    this.onSearchChanged,
    this.onTapEditCollection,
    this.onTapDeleteCollection,
    this.onTapEdit,
    this.onTapDelete,
    this.searchController,
  }) : super(key: key);

  @override
  State<MyTable> createState() => _MyTableState();
}

class _MyTableState extends State<MyTable> {
  TextEditingController searchController = TextEditingController();
  bool isEdited = false;
  List<bool> selected = [];

  @override
  void initState() {
    selected = List.generate(widget.body.length, (index) => false);
    super.initState();
  }

  TableRow header() {
    return TableRow(
        decoration: widget.headerDecoration ??
            BoxDecoration(
                color: Colors.black.withOpacity(0.03), borderRadius: const BorderRadius.only(topLeft: Radius.circular(5), topRight: Radius.circular(5))),
        children: [
          ...widget.headers
              .map((e) => TableCell(
                      child: Padding(
                    padding: widget.headerPadding ?? const EdgeInsets.all(10.0),
                    child: Text(
                      e,
                      style: widget.headerStyle ?? MyTypo.heading4,
                    ),
                  )))
              .toList(),
        ]);
  }

  @override
  Widget build(BuildContext context) {
    Map<int, TableColumnWidth> columnWidths = {
      0: const FixedColumnWidth(60),
      1: const FixedColumnWidth(200),
    };
    if (widget.headerWidth is Map<int, TableColumnWidth>) {
      columnWidths = widget.headerWidth;
    }
    if (widget.headerWidth is List<double>?) {
      if (widget.headerWidth != null) {
        for (var i = 0; i < widget.headerWidth!.length; i++) {
          columnWidths[i] = FixedColumnWidth(widget.headerWidth![i]);
        }
      }
    }
    return Column(
      children: [
        if (!widget.showToolbar)
          Container(
            padding: const EdgeInsets.all(15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  widget.title,
                  style: MyTypo.heading4,
                ),
                Row(
                  children: [
                    Input(
                      controller: widget.searchController ?? searchController,
                      padding: const EdgeInsets.fromLTRB(10, 7, 10, 7),
                      textFieldStyle: MyTypo.bodyText1.copyWith(color: Colors.black87),
                      height: 30,
                      width: 300,
                      hintText: "Search ...",
                      onChanged: (val) {
                        if (widget.onSearchChanged != null) widget.onSearchChanged!(val);
                        setState(() {});
                      },
                      onClear: widget.onSearchClear ??
                          (searchController.text.isEmpty
                              ? null
                              : () {
                                  searchController.clear();
                                }),
                      onSubmitted: (search) => widget.onSearchSubmitted!(search),
                    ),
                    isEdited
                        ? IconButton(
                            splashRadius: 20,
                            onPressed: () {
                              if (widget.onTapEditCollection != null) {
                                List<Map<String, dynamic>> selected = [];
                                for (var i = 0; i < selected.length; i++) {
                                  selected.add({"index": i, "val": selected[i]});
                                }
                                widget.onTapEditCollection!(selected.where((e) => e["val"]).map((e) => e["index"] as int).toList());
                                setState(() {
                                  isEdited = false;
                                });
                              }
                            },
                            icon: const Icon(
                              FluentSystemIcons.ic_fluent_edit_regular,
                              color: Colors.grey,
                              size: 14,
                            ))
                        : Container(),
                    isEdited
                        ? IconButton(
                            splashRadius: 20,
                            onPressed: () {
                              if (widget.onTapDeleteCollection != null) {
                                List<Map<String, dynamic>> selected0 = [];
                                for (var i = 0; i < selected.length; i++) {
                                  selected0.add({"index": i, "val": selected[i]});
                                }
                                widget.onTapDeleteCollection!(selected0.where((e) => e["val"]).map((e) => e["index"] as int).toList());
                                setState(() {
                                  isEdited = false;
                                });
                              }
                            },
                            icon: const Icon(
                              FluentSystemIcons.ic_fluent_delete_filled,
                              color: Colors.grey,
                              size: 14,
                            ))
                        : Container()
                  ],
                ),
              ],
            ),
          ),
        Table(
          defaultVerticalAlignment: widget.valign,
          columnWidths: columnWidths,
          children: [
            header(),
            if (widget.beforeBody != null) TableRow(children: [...widget.beforeBody!]),
            ...widget.body.map((row) {
              final index = widget.body.indexWhere((r) => r == row);
              bool isEven = index % 2 == 0;
              return TableRow(
                  decoration: widget.bodyDecoration != null
                      ? widget.bodyDecoration!.copyWith(color: !isEven ? widget.eventColor : widget.oddColor)
                      : BoxDecoration(
                          color: !isEven ? widget.eventColor ?? Colors.black.withOpacity(0.03) : widget.oddColor,
                          border: Border(bottom: BorderSide(color: Colors.black.withOpacity(0.07)))),
                  children: row.map((e) {
                    var indexCell = row.indexOf(e);
                    return GestureDetector(
                      onLongPress: () {
                        setState(() {
                          isEdited = !isEdited;
                        });
                      },
                      onTap: () {
                        if (isEdited) {
                          setState(() {
                            selected[index] = !selected[index];
                          });
                        }
                      },
                      child: Padding(
                        padding: widget.bodyPadding ?? const EdgeInsets.all(10.0),
                        child: isEdited && indexCell == 0
                            ? Transform.scale(
                                scale: 0.7,
                                child: Checkbox(
                                  value: selected[index],
                                  onChanged: (bool? value) {
                                    setState(() {
                                      selected[index] = value!;
                                    });
                                  },
                                ),
                              )
                            : isEdited && indexCell == row.length - 1
                                ? Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      e,
                                      Row(
                                        children: [
                                          IconButton(
                                              splashRadius: 20,
                                              onPressed: () {
                                                if (widget.onTapEdit != null) {
                                                  widget.onTapEdit!(index);
                                                  setState(() {
                                                    isEdited = false;
                                                  });
                                                }
                                              },
                                              icon: const Icon(
                                                FluentSystemIcons.ic_fluent_edit_regular,
                                                color: Colors.grey,
                                                size: 14,
                                              )),
                                          IconButton(
                                              splashRadius: 20,
                                              onPressed: () {
                                                if (widget.onTapDelete != null) {
                                                  widget.onTapDelete!(index);
                                                  setState(() {
                                                    isEdited = false;
                                                  });
                                                }
                                              },
                                              icon: const Icon(
                                                FluentSystemIcons.ic_fluent_delete_filled,
                                                color: Colors.grey,
                                                size: 14,
                                              ))
                                        ],
                                      )
                                    ],
                                  )
                                : e,
                      ),
                    );
                  }).toList());
            }).toList(),
            if (widget.footer != null) TableRow(children: [...widget.footer!]),
            if (widget.footer == null && widget.headerAsFooter) header(),
          ],
        ),
        if (widget.showPagination)
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: renderPagination(),
          )
      ],
    );
  }

  Widget renderPagination() {
    return MyPagination(
        limit: widget.limit,
        next: widget.next,
        page: widget.page,
        prev: widget.prev,
        totalPages: widget.totalPages,
        totalRecords: widget.totalRecords,
        showResult: widget.showResult,
        onTapPagination: (val) {
          if (widget.onTapPagination == null) return;
          widget.onTapPagination!(val);
          setState(() {
            isEdited = false;
          });
        });
  }
}
