import 'package:flutter/material.dart';

// class WidgetSize extends StatelessWidget {
//   final Widget child;
//   final Function onChange;
//   WidgetSize({Key? key, required this.child, required this.onChange}) : super(key: key);

//   Size? oldSize;
//   var widgetKey = GlobalKey();
//   void postFrameCallback(_) {
//     var context = widgetKey.currentContext;
//     if (context == null) return;

//     var newSize = context.size;
//     if (oldSize == newSize) return;

//     oldSize = newSize;
//     onChange(newSize);
//   }

//   @override
//   Widget build(BuildContext context) {
//     WidgetsBinding.instance.addPostFrameCallback(postFrameCallback);
//     return Container();
//   }
// }

class WidgetSize extends StatefulWidget {
  final Widget? child;
  final Function onChange;

  const WidgetSize({
    Key? key,
    required this.onChange,
    required this.child,
  }) : super(key: key);

  @override
  State<WidgetSize> createState() => _WidgetSizeState();
}

class _WidgetSizeState extends State<WidgetSize> {
  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback(postFrameCallback);
    return Container(
      key: widgetKey,
      child: widget.child ?? Container(),
    );
  }

  var widgetKey = GlobalKey();
  Size? oldSize;

  void postFrameCallback(_) {
    try {
      var context = widgetKey.currentContext;
      if (context == null) return;
      if (context.size == null) return;

      var newSize = context.size;

      if (oldSize == newSize) return;

      oldSize = newSize;
      widget.onChange(newSize);
    } catch (e) {
      // error handler
    }
  }
}
