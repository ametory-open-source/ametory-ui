import 'package:ametory_ui/src/models/menu_model.dart';
import 'package:ametory_ui/src/widgets/main_menu.dart';
import 'package:flutter/material.dart';

class SideBar extends StatefulWidget {
  final List<MenuModel>? menuModel;
  final bool isAuth;
  final bool isCollapsed;
  final bool showLabelCollapsed;
  final Color? expandedIconColor;
  final Color? collapsedIconColor;
  final Color? backgroundColor;
  final double? width;
  final double? elevation;
  final Duration? duration;
  final Curve? curve;
  final EdgeInsets? padding;
  final double collapsedWidth;
  final Color? sidebarActiveMenuColor;
  final BorderRadius? sidebarActiveBorderRadius;
  final Widget? sidebarTrailing;
  final Widget? sidebarLeading;
  final Function(bool, bool)? onCollapsedTap;
  final Function(bool, bool)? onCollapsedTapDown;
  const SideBar(
      {Key? key,
      this.isCollapsed = false,
      this.showLabelCollapsed = false,
      this.isAuth = false,
      this.expandedIconColor,
      this.collapsedIconColor,
      this.backgroundColor,
      this.elevation,
      this.width,
      this.duration,
      this.curve,
      this.sidebarActiveMenuColor,
      this.sidebarActiveBorderRadius,
      this.padding,
      this.collapsedWidth = 80,
      this.menuModel,
      this.sidebarTrailing,
      this.sidebarLeading,
      this.onCollapsedTapDown,
      this.onCollapsedTap})
      : super(key: key);

  @override
  State<SideBar> createState() => _SideBarState();
}

class _SideBarState extends State<SideBar> {
  late bool isCollapsed;
  bool isCollapsing = false;

  @override
  void initState() {
    isCollapsed = widget.isCollapsed;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: widget.elevation ?? 3,
      child: AnimatedContainer(
        onEnd: () {
          setState(() {
            isCollapsing = false;
            if (widget.onCollapsedTap != null) {
              widget.onCollapsedTap!(isCollapsed, isCollapsing);
            }
          });
        },
        curve: widget.curve ?? Curves.fastOutSlowIn,
        padding: widget.padding ?? const EdgeInsets.all(10),
        height: double.infinity,
        color: widget.backgroundColor ?? Colors.white,
        width: isCollapsed ? widget.collapsedWidth : widget.width ?? 240,
        duration: widget.duration ?? const Duration(milliseconds: 300),
        child: Column(
          children: [
            widget.sidebarLeading ?? Container(),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (widget.menuModel != null)
                    MainMenu(
                      sidebarActiveMenuColor: widget.sidebarActiveMenuColor,
                      sidebarActiveBorderRadius: widget.sidebarActiveBorderRadius,
                      showLabelCollapsed: widget.showLabelCollapsed,
                      menu: widget.menuModel!,
                      isCollapsed: isCollapsed,
                      isCollapsing: isCollapsing,
                    )
                ],
              ),
            ),
            widget.sidebarTrailing ?? Container(),
            SizedBox(
              height: 55,
              child: InkWell(
                onTap: () => setState(() {
                  isCollapsed = !isCollapsed;
                  isCollapsing = !isCollapsing;
                  if (widget.onCollapsedTapDown != null) {
                    widget.onCollapsedTapDown!(isCollapsed, isCollapsing);
                  }
                }),
                child: Icon(
                  isCollapsed ? Icons.chevron_right : Icons.chevron_left,
                  color: isCollapsed ? widget.collapsedIconColor : widget.expandedIconColor,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
