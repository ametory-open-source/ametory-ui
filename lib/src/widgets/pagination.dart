import 'package:flutter/material.dart';

import '../../ametory_ui.dart';

class MyPagination extends StatelessWidget {
  final int totalPages;
  final int totalRecords;
  final int prev;
  final int next;
  final int page;
  final int limit;
  final String paginationLabel;
  final bool showResult;
  final Function(int)? onTapPagination;
  final double? buttonWidth;
  final BoxDecoration? activeDecoration;
  final BoxDecoration? decoration;
  final TextStyle? activeTextStyle;
  final TextStyle? textStyle;
  const MyPagination(
      {Key? key,
      required this.totalPages,
      required this.totalRecords,
      required this.prev,
      required this.next,
      required this.page,
      required this.limit,
      this.onTapPagination,
      this.paginationLabel = "",
      this.showResult = true,
      this.buttonWidth,
      this.activeDecoration,
      this.decoration,
      this.activeTextStyle,
      this.textStyle})
      : super(key: key);

  Widget _renderButton({Function()? onTap, bool isCurrent = false, int? selectedPage, Widget? icon}) {
    return InkWell(
      hoverColor: Colors.transparent,
      onTap: onTap ?? (selectedPage == null ? null : () => onTapPagination!(selectedPage)),
      child: Material(
          elevation: 1,
          child: Container(
            constraints: BoxConstraints(minWidth: buttonWidth ?? 32, minHeight: buttonWidth ?? 32),
            decoration: isCurrent && activeDecoration != null
                ? activeDecoration
                : !isCurrent && activeDecoration != null
                    ? decoration
                    : const BoxDecoration(
                        color: Colors.white,
                      ),
            padding: const EdgeInsets.all(5),
            child: Center(
                child: icon ??
                    Text(selectedPage == null ? "..." : selectedPage.toString(),
                        style: isCurrent && activeTextStyle != null
                            ? activeTextStyle
                            : !(isCurrent) && textStyle != null
                                ? textStyle
                                : MyTypo.bodyText1.copyWith(fontSize: (isCurrent) ? 14 : 12, fontWeight: (isCurrent) ? FontWeight.w600 : null),
                        textAlign: TextAlign.center)),
          )),
    );
  }

  Widget _renderPaginationLimit() {
    List<Widget> newPage = [];
    newPage.add(_renderButton(
      selectedPage: 1,
      isCurrent: page == 1,
    ));
    if (page > 2 && !(page > totalPages - 3)) {
      newPage.add(_renderButton());
      newPage.add(_renderButton(
        selectedPage: page - 1,
        isCurrent: page == page - 1,
      ));

      newPage.add(_renderButton(
        selectedPage: page,
        isCurrent: true,
      ));

      newPage.add(_renderButton(
        selectedPage: page + 1,
        isCurrent: page == page + 1,
      ));
      newPage.add(_renderButton());
    } else if (page > totalPages - 3) {
      newPage.add(_renderButton(
        selectedPage: 2,
        isCurrent: page == 2,
      ));
      newPage.add(_renderButton());
      newPage.add(_renderButton(
        selectedPage: totalPages - 2,
        isCurrent: page == totalPages - 2,
      ));
      newPage.add(_renderButton(
        selectedPage: totalPages - 1,
        isCurrent: page == totalPages - 1,
      ));
    } else {
      if (page != 1) {
        newPage.add(_renderButton(
          selectedPage: page,
          isCurrent: true,
        ));
      }

      newPage.add(_renderButton(
        selectedPage: page + 1,
        isCurrent: page == page + 1,
      ));
      newPage.add(_renderButton());
      newPage.add(_renderButton(
        selectedPage: totalPages - 1,
        isCurrent: page == totalPages - 1,
      ));
    }

    newPage.add(_renderButton(
      selectedPage: totalPages,
      isCurrent: page == totalPages,
    ));

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: newPage,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        totalPages == 1
            ? Container()
            : Row(children: [
                if (prev > 1)
                  _renderButton(
                      icon: const Icon(
                        Icons.chevron_left_rounded,
                        size: 12,
                      ),
                      onTap: () => onTapPagination!(prev)),
                if (totalPages < 11)
                  ...List.generate(totalPages, (index) {
                    // bool isCurrent = page == index + 1;
                    return _renderButton(
                      selectedPage: index + 1,
                      isCurrent: page == index + 1,
                    );
                  }),
                if (totalPages >= 11) _renderPaginationLimit(),
                if (page < totalPages)
                  _renderButton(
                      icon: const Icon(
                        Icons.chevron_right_rounded,
                        size: 12,
                      ),
                      onTap: () => onTapPagination!(next))
              ]),
        if (showResult)
          Text(
            "$paginationLabel$totalRecords",
            style: MyTypo.smallText,
          )
      ],
    );
  }
}
