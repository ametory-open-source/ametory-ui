import 'package:fluentui_icons/fluentui_icons.dart';
import 'package:flutter/material.dart';

import '../../../ametory_ui.dart';

class CheckBoxWidget extends StatelessWidget {
  final String label;
  final double? labelFontSize;
  final bool isChecked;
  final bool isDeleted;
  final double? iconSize;
  final Widget? leading;
  final TextStyle? style;
  final Function()? onTap;
  const CheckBoxWidget(
      {Key? key,
      required this.label,
      this.labelFontSize,
      this.iconSize,
      this.style,
      this.leading,
      this.isChecked = false,
      this.isDeleted = false,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        leading ?? Container(),
        Expanded(
          child: Text(
            label,
            style: style ?? MyTypo.bodyText1.copyWith(fontSize: labelFontSize),
          ),
        ),
        GestureDetector(
          onTap: onTap,
          child: !isChecked
              ? Icon(
                  FluentSystemIcons.ic_fluent_checkbox_unchecked_filled,
                  color: Colors.grey,
                  size: iconSize,
                )
              : Icon(
                  FluentSystemIcons.ic_fluent_checkbox_checked_filled,
                  color: Colors.green,
                  size: iconSize,
                ),
        )
      ],
    );
  }
}
