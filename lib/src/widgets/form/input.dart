import 'package:flutter/material.dart';

import '../../../ametory_ui.dart';

class Input extends StatelessWidget {
  final String? label;
  final double? labelSize;
  final String? hintText;
  final String? description;
  final String? errorText;
  final TextEditingController? controller;
  final FocusNode? focusNode;
  final Widget? leading;
  final Widget? trailing;
  final bool obscureText;
  final bool validate;
  final double? minHeight;
  final double? height;
  final int? maxLength;
  final double? width;
  final double? fontSize;
  final TextInputType? keyboardType;
  final BorderRadius? borderRadius;
  final TextStyle? hintStyle;
  final TextStyle? textFieldStyle;
  final TextAlign textAlign;
  final TextStyle? labelStyle;
  final EdgeInsets? padding;
  final Widget? child;
  final int? maxLine;
  final int? minLine;
  final Color? backgroundColor;
  final Color? disabledColor;
  final Color? labelColor;
  final Function()? onTap;
  final Function(TapDownDetails)? onTapDown;
  final Function()? onClear;
  final Function()? onEditingComplete;
  final Function(String)? onSubmitted;
  final Function(String)? onChanged;
  final bool? enabled;
  final bool? autofocus;
  final BoxBorder? border;
  const Input({
    Key? key,
    this.label,
    this.description,
    this.textFieldStyle,
    this.controller,
    this.focusNode,
    this.hintText,
    this.borderRadius,
    this.maxLine,
    this.height,
    this.width,
    this.minLine,
    this.labelSize,
    this.labelColor,
    this.labelStyle,
    this.border,
    this.keyboardType,
    this.obscureText = false,
    this.validate = true,
    this.textAlign = TextAlign.start,
    this.errorText,
    this.leading,
    this.autofocus,
    this.trailing,
    this.enabled,
    this.minHeight,
    this.backgroundColor,
    this.fontSize,
    this.hintStyle,
    this.onEditingComplete,
    this.onTap,
    this.onTapDown,
    this.onClear,
    this.onSubmitted,
    this.onChanged,
    this.child,
    this.padding,
    this.disabledColor,
    this.maxLength,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (label != null)
          Text(
            label!,
            style: labelStyle ??
                MyTypo.heading4
                    .copyWith(fontSize: labelSize, color: labelColor),
          ),
        if (label != null) vspace(10),
        if (errorText != null && !validate)
          Text(
            errorText!,
            style: MyTypo.heading4.copyWith(color: Colors.red),
          ),
        if (errorText != null && !validate) vspace(5),
        GestureDetector(
          onTap: onTap,
          onTapDown: onTapDown,
          child: Stack(
            children: [
              child ??
                  Container(
                    constraints: BoxConstraints(minHeight: minHeight ?? 30),
                    height: height,
                    padding: padding ?? const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    width: width ?? double.infinity,
                    decoration: BoxDecoration(
                        borderRadius: borderRadius ?? BorderRadius.circular(10),
                        color: enabled == null || enabled!
                            ? backgroundColor ?? Colors.black.withOpacity(0.02)
                            : disabledColor ?? Colors.black12,
                        border: border ??
                            (validate ? null : Border.all(color: Colors.red))),
                    child: Row(
                      crossAxisAlignment: maxLine != null
                          ? CrossAxisAlignment.start
                          : CrossAxisAlignment.center,
                      children: [
                        if (leading != null)
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: leading,
                          ),
                        Expanded(
                          child: TextField(
                            focusNode: focusNode,
                            autofocus: autofocus ?? false,
                            enabled: enabled,
                            onEditingComplete: onEditingComplete,
                            onSubmitted: onSubmitted,
                            onChanged: onChanged,
                            minLines: minLine,
                            maxLines: maxLine ?? 1,
                            controller: controller,
                            obscureText: obscureText,
                            keyboardType: keyboardType,
                            style: textFieldStyle ?? MyTypo.bodyText1,
                            textAlign: textAlign,
                            maxLength: maxLength,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              // filled: true,
                              // fillColor: Colors.red,
                              hintText: hintText,
                              hintStyle: hintStyle ??
                                  const TextStyle().copyWith(color: Colors.black45),
                            ),
                          ),
                        ),
                        trailing ?? Container()
                      ],
                    ),
                  ),
              if (onClear != null)
                Positioned(
                  top: 0,
                  bottom: 0,
                  right: (trailing != null) ? 60 : 3,
                  child: GestureDetector(
                      onTap: onClear,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          "assets/images/icons/cross.png",
                          color: Colors.black54,
                          width: 14,
                          package: "ametory_ui",
                        ),
                      )),
                )
            ],
          ),
        ),
        if (description != null) vspace(5),
        if (description != null)
          Text(
            description!,
            style: MyTypo.smallText,
          )
      ],
    );
  }
}
