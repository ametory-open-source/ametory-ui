import 'package:ametory_ui/src/styles/color.dart';
import 'package:flutter/material.dart';

class SimpleInputBox extends StatelessWidget {
  final Widget? leading;
  final Widget? trailing;
  final Color? backgroundColor;
  final Color? inputColor;
  final Color? borderColor;
  final TextEditingController? controller;
  final FocusNode? focusNode;
  final String? errorText;
  final TextInputType? keyboardType;
  final TextInputAction? textInputAction;
  final String? hintText;
  final TextStyle? hintStyle;
  final TextStyle? textFieldStyle;
  final bool obscureText;
  final Icon? prefixIcon;
  final Function()? onClear;
  final BorderRadius? borderRadius;
  final bool validate;
  final int? minLines;
  final int? maxLines;
  final Function()? onTap;
  final Function(String)? onSubmitted;
  final Function(String)? onChanged;
  final bool enabled;
  final TextAlign? textAlign;
  const SimpleInputBox(
      {Key? key,
      this.leading,
      this.trailing,
      this.backgroundColor,
      this.textFieldStyle,
      this.borderColor,
      this.inputColor,
      this.controller,
      this.focusNode,
      this.errorText,
      this.hintText,
      this.minLines,
      this.textAlign,
      this.textInputAction,
      this.maxLines,
      this.onClear,
      this.onTap,
      this.onSubmitted,
      this.hintStyle,
      required this.obscureText,
      required this.validate,
      this.keyboardType,
      this.onChanged,
      this.enabled = true,
      this.prefixIcon,
      this.borderRadius})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: backgroundColor ?? Colors.white,
          child: Container(
            constraints:
                minLines != null ? const BoxConstraints(minHeight: 60) : null,
            width: double.infinity,
            decoration: BoxDecoration(
                borderRadius: borderRadius ?? BorderRadius.circular(5),
                color: inputColor ?? Colors.black12,
                border: validate
                    ? borderColor != null
                        ? Border.all(color: borderColor!)
                        : null
                    : Border.all(color: MyColor.danger)),
            child: Row(
              children: [
                if (leading != null) leading!,
                Expanded(
                  child: GestureDetector(
                    onTap: onTap,
                    child: TextField(
                      focusNode: focusNode,
                      onSubmitted: onSubmitted,
                      textAlign: textAlign ?? TextAlign.left,
                      enabled: enabled,
                      controller: controller,
                      obscureText: obscureText,
                      keyboardType: keyboardType,
                      textInputAction: textInputAction,
                      minLines: minLines,
                      maxLines: maxLines,
                      onChanged: onChanged,
                      enableSuggestions: false,
                      autocorrect: false,
                      style: textFieldStyle ??
                          const TextStyle(
                            color: Colors.black87,
                            fontSize: 12,
                            // fontWeight: FontWeight.bold,
                          ),
                      decoration: InputDecoration(
                        prefixIcon: prefixIcon,
                        isDense: true,
                        contentPadding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 14),
                        border: InputBorder.none,
                        hintStyle: hintStyle,
                        hintText: hintText,
                      ),
                    ),
                  ),
                ),
                if (trailing != null) trailing!,
              ],
            ),
          ),
        ),
        if (onClear != null)
          Positioned(
            top: 0,
            bottom: 0,
            right: (trailing != null) ? 60 : 10,
            child: GestureDetector(
                onTap: onClear,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.asset(
                    "assets/images/icons/cross.png",
                    color: Colors.grey,
                    width: 16,
                  ),
                )),
          )
      ],
    );
  }
}
