import 'package:ametory_ui/ametory_ui.dart';
import 'package:fluentui_icons/fluentui_icons.dart';
import 'package:flutter/material.dart';

class SelectBox<T> extends StatelessWidget {
  final String? label;
  final double? labelSize;
  final String? hintText;
  final String? description;
  final String? errorText;
  final TextEditingController? controller;
  final FocusNode? focusNode;
  final Widget? leading;
  final Widget? trailing;
  final bool validate;
  final double? minHeight;
  final double? height;
  final double? width;
  final double? fontSize;
  final TextInputType? keyboardType;
  final BorderRadius? borderRadius;
  final TextStyle? hintStyle;
  final TextStyle? textFieldStyle;
  final TextStyle? labelStyle;
  final EdgeInsets? padding;
  final List<PopupMenuEntry<T>> items;
  final Color? disabledColor;
  final Function(T?)? onTap;
  const SelectBox({
    Key? key,
    this.controller,
    this.onTap,
    required this.items,
    this.disabledColor,
    this.label,
    this.hintText,
    this.hintStyle,
    this.labelSize,
    this.description,
    this.errorText,
    this.focusNode,
    this.leading,
    this.trailing,
    this.validate = true,
    this.minHeight,
    this.height,
    this.width,
    this.fontSize,
    this.keyboardType,
    this.borderRadius,
    this.textFieldStyle,
    this.labelStyle,
    this.padding,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Input(
        enabled: false,
        label: label,
        hintText: hintText,
        hintStyle: hintStyle,
        labelSize: labelSize,
        description: description,
        errorText: errorText,
        focusNode: focusNode,
        leading: leading,
        trailing: trailing ??
            const Icon(
              FluentSystemIcons.ic_fluent_chevron_down_regular,
              size: 16,
              color: Colors.grey,
            ),
        validate: validate,
        minHeight: minHeight,
        height: height,
        width: width,
        fontSize: fontSize,
        keyboardType: keyboardType,
        borderRadius: borderRadius,
        textFieldStyle: textFieldStyle,
        labelStyle: labelStyle,
        padding: padding,
        disabledColor: disabledColor ?? const Color(0xFFFEFEFE),
        onTapDown: (details) async {
          if (onTap != null) {
            T? res = await popUpMenu<T>(context,
                offset: details.globalPosition, items: items);
            onTap!(res);
          }
        },
        controller: controller);
  }
}
