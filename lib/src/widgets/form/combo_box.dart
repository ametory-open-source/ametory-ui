import 'package:flutter/material.dart';

import '../../../ametory_ui.dart';

class ComboBox extends StatelessWidget {
  final String? label;
  final double? labelSize;
  final String? hintText;
  final String? description;
  final String? errorText;
  final TextEditingController? controller;
  final FocusNode? focusNode;
  final Widget? leading;
  final Widget? trailing;
  final bool obscureText;
  final bool showOption;
  final bool validate;
  final double? minHeight;
  final double? height;
  final double? fontSize;
  final TextInputType? keyboardType;
  final TextStyle? hintStyle;
  final EdgeInsets? padding;
  final Widget? child;
  final int? maxLine;
  final int? minLine;
  final Function(ComboBoxData)? onSelect;
  final Function()? onTap;
  final Function()? onClear;
  final Function()? onEditingComplete;
  final Function(String)? onSubmitted;
  final Function(String)? onChanged;
  final bool? enabled;
  final List<ComboBoxData> data;
  const ComboBox(
      {Key? key,
      this.label,
      this.description,
      this.controller,
      this.focusNode,
      this.hintText,
      this.showOption = false,
      this.maxLine,
      this.height,
      this.onSelect,
      this.minLine,
      this.labelSize,
      this.keyboardType,
      this.obscureText = false,
      this.validate = true,
      this.errorText,
      this.leading,
      this.trailing,
      this.enabled,
      this.minHeight,
      this.fontSize,
      this.hintStyle,
      this.onEditingComplete,
      this.onTap,
      this.onClear,
      this.onSubmitted,
      this.onChanged,
      this.child,
      required this.data,
      this.padding})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(controller!.text);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (label != null)
          Text(
            label!,
            style: MyTypo.bodyText1.copyWith(fontSize: labelSize),
          ),
        if (label != null) vspace(10),
        if (errorText != null && !validate)
          Text(
            errorText!,
            style: MyTypo.bodyText1.copyWith(color: MyColor.danger),
          ),
        if (errorText != null && !validate) vspace(5),
        GestureDetector(
          onTap: onTap,
          child: Stack(
            children: [
              Column(
                children: [
                  child ??
                      Container(
                        constraints: BoxConstraints(minHeight: minHeight ?? 60),
                        height: height,
                        padding: padding ??
                            const EdgeInsets.fromLTRB(15, 10, 15, 10),
                        width: double.infinity,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: enabled == null || enabled!
                                ? Colors.black.withOpacity(0.02)
                                : Colors.black.withOpacity(0.07),
                            border: validate
                                ? null
                                : Border.all(color: MyColor.danger)),
                        child: Row(
                          children: [
                            if (leading != null) leading!,
                            Expanded(
                              child: TextField(
                                onChanged: onChanged,
                                enabled: enabled,
                                onEditingComplete: onEditingComplete,
                                onSubmitted: onSubmitted,
                                minLines: minLine,
                                maxLines: maxLine ?? 1,
                                controller: controller,
                                obscureText: obscureText,
                                keyboardType: keyboardType,
                                style: MyTypo.bodyText1
                                    .copyWith(fontSize: fontSize ?? 24),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  // filled: true,
                                  // fillColor: Colors.red,
                                  hintText: hintText,
                                  hintStyle: hintStyle ??
                                      const TextStyle()
                                          .copyWith(color: Colors.grey),
                                ),
                              ),
                            ),
                            if (trailing != null) trailing!,
                          ],
                        ),
                      ),
                  if (showOption) vspace(10),
                  if (showOption)
                    Container(
                      padding: const EdgeInsets.all(10),
                      width: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white,
                          border: validate
                              ? null
                              : Border.all(color: MyColor.danger)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: data
                            .where((e) => e.label
                                .toLowerCase()
                                .contains(controller!.text.toLowerCase()))
                            .map((e) => InkWell(
                                  onTap: onSelect == null
                                      ? null
                                      : () => onSelect!(e),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: Text(
                                      "$e",
                                      style: MyTypo.bodyText1,
                                    ),
                                  ),
                                ))
                            .toList(),
                      ),
                    ),
                ],
              ),
              if (onClear != null)
                Positioned(
                  top: height ?? 30 / 2,
                  right: (trailing != null) ? 60 : 10,
                  child: GestureDetector(
                      onTap: onClear,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          "assets/images/icons/cross.png",
                          color: Colors.grey,
                          width: 16,
                        ),
                      )),
                )
            ],
          ),
        ),
        if (description != null) vspace(5),
        if (description != null)
          Text(
            description!,
            style: MyTypo.bodyText1,
          )
      ],
    );
  }
}
