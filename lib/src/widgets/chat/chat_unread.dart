import 'package:ametory_ui/ametory_ui.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class ChatUnread extends StatefulWidget {
  final Widget? child;
  final String companyID;
  final UserModel? userData;
  final String roomKey;
  final CollectionReference<MyRoom>? roomsRef;
  final Function(int)? onChanged;
  const ChatUnread(
      {Key? key,
      required this.companyID,
      this.userData,
      required this.roomKey,
      this.roomsRef,
      this.child,
      this.onChanged})
      : super(key: key);

  @override
  State<ChatUnread> createState() => _ChatUnreadState();
}

class _ChatUnreadState extends State<ChatUnread> {
  late CollectionReference<MyRoom> _roomsRef;
  @override
  void initState() {
    _roomsRef = widget.roomsRef ??
        FirebaseFirestore.instance.collection(widget.roomKey).withConverter(
              fromFirestore: (snapshot, _) => MyRoom.fromJson(snapshot.data()!),
              toFirestore: (MyRoom room, _) => room.toJson(),
            );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // return widget.child ?? Container();

    return widget.userData == null
        ? widget.child ?? Container()
        : StreamBuilder<QuerySnapshot<MyRoom>>(
            stream: _roomsRef
                .orderBy('last_message_date', descending: true)
                .where("user_ids", arrayContains: widget.userData!.id)
                .where("company_id", isEqualTo: widget.companyID)
                .snapshots(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                SchedulerBinding.instance.addPostFrameCallback((_) {
                  if (widget.onChanged != null) {
                    var count =
                        getUnreadChat(snapshot.data!.docs, widget.userData!);
                    widget.onChanged!(count);
                  }
                });
                return widget.child ?? Container();
              }
              if (snapshot.hasError) {
                // print(snapshot.error);
              }

              return widget.child ?? Container();
            });
  }
}
