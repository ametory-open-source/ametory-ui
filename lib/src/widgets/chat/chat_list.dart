
import 'package:ametory_ui/ametory_ui.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_parsed_text/flutter_parsed_text.dart';
import 'package:intl/intl.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:swipe_to/swipe_to.dart';
import 'package:visibility_detector/visibility_detector.dart';

import '../../models/chat/message_option.dart';
import 'chat_bubble.dart';

class ChatListWidget extends StatelessWidget {
  final AutoScrollController scrollController;
  final List<QueryDocumentSnapshot<MyChatMessage>> messages;
  final int index;
  final UserModel userData;
  final Function(MyChatUser)? onTapUser;
  final Function(MyChatMessage, bool)? onSwipe;
  final Function(MyChatMessage)? onReaded;
  final Function(MyChatMessage, bool)? onTapBubble;
  final Function(MyChatMessage, bool)? onLongPressBubble;
  final Function(MyChatMessage, bool, Offset)? onTapBubbleArrow;
  final Widget Function(MyChatMessage)? customFeature;

  final MyRoom? chatRoom;
  final bool isPrivate;
  final Color? otherBubbleColor;
  final Color? myBubbleColor;
  const ChatListWidget({
    Key? key,
    required this.messages,
    required this.onTapBubbleArrow,
    required this.index,
    required this.userData,
    this.onTapUser,
    required this.scrollController,
    this.isPrivate = false,
    this.chatRoom,
    this.otherBubbleColor,
    this.myBubbleColor,
    this.onReaded,
    this.onSwipe,
    this.onTapBubble,
    this.onLongPressBubble,
    this.customFeature,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MyChatMessage? prevMsg;
    MyChatMessage? nextMsg;
    MyChatMessage? prevUserMsg;
    MyChatMessage? nextUserMsg;
    MyChatMessage msg = messages[index].data();
    msg.id = messages[index].id;
    msg.customProperties!["index"] = index;
    if (index > 0) {
      prevMsg = messages[index - 1].data();
    }
    if (index != messages.length - 1) {
      nextMsg = messages[index + 1].data();
    }
    bool isMe = msg.user!.id == userData.id;
    bool firstMessageDay = false;
    bool userLastMessage = false;
    bool userLastDayMessage = false;
    bool userFirstMessage = false;
    if (prevMsg != null) {
      if (prevMsg.user!.id == msg.user!.id) {
        prevUserMsg = prevMsg;
      }
      if (prevUserMsg == null) userLastMessage = true;
    }
    if (nextMsg != null) {
      if (nextMsg.user!.id == msg.user!.id) {
        nextUserMsg = nextMsg;
      }
      firstMessageDay = DateFormat("ddMMyyyy").format(msg.createdAt!) !=
          DateFormat("ddMMyyyy").format(nextMsg.createdAt!);

      if (nextUserMsg == null) userFirstMessage = true;
    }
    if (index == 0) userLastMessage = true;
    if (index == 0) userLastDayMessage = true;
    if (index == messages.length - 1) firstMessageDay = true;
    if (prevMsg != null) {
      userLastDayMessage = userLastMessage ||
          (DateFormat("ddMMyyyy").format(msg.createdAt!) !=
              DateFormat("ddMMyyyy").format(prevMsg.createdAt!));
    }
    List<MatchText> matched = [];
    if (msg.mentions!.isNotEmpty) {
      for (var e in msg.mentions!) {
        matched.add(MatchText(
          // ignore: prefer_interpolation_to_compose_strings
          pattern: r"(@" + e.firstName! + ")",
          style: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            decoration: TextDecoration.underline,
          ),
          renderText: ({String? str, String? pattern}) {
            Map<String, String> map = {};
            RegExp customRegExp = RegExp(pattern!);
            RegExpMatch? match = customRegExp.firstMatch(str!);
            map['display'] = match!.group(1) ?? "";
            map['value'] = e.id;
            return map;
          },
          onTap: (match) {
            final user = msg.mentions!
                .firstWhere((e) => e.firstName == match.substring(1));
            if (onTapUser != null) {
              onTapUser!(user);
            }
          },
        ));
      }
    }
    MyMessageOptions messageOptions = const MyMessageOptions(
      showTime: true,
      textColor: Colors.white,
    );
    return AutoScrollTag(
      key: ValueKey(index),
      controller: scrollController,
      index: index,
      child: SwipeTo(
        onLeftSwipe: isMe
            ? (detail) {
                onSwipe!(msg, isMe);
              }
            : null,
        onRightSwipe: !isMe
            ? (detail) {
                // swipeMenu(msg, isMe, userData);
                onSwipe!(msg, isMe);
              }
            : null,
        child: VisibilityDetector(
          key: Key(msg.createdAt!.microsecondsSinceEpoch.toString()),
          onVisibilityChanged: (VisibilityInfo info) {
            if (msg.id.isNotEmpty && onReaded != null) onReaded!(msg);
          },
          child: Column(
            children: [
              if (firstMessageDay)
                Container(
                    margin: const EdgeInsets.only(bottom: 10, top: 20),
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Colors.black12,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Text(
                      DateTime.now().difference(msg.createdAt!).inDays < 8
                          ? DateFormat('EEEE', 'id_ID').format(msg.createdAt!)
                          : DateFormat('dd MMM yyyy', 'id_ID')
                              .format(msg.createdAt!),
                      style: MyTypo.smallText
                          .copyWith(fontSize: 12, color: Colors.black87),
                    )),
              Stack(
                children: [
                  // if (isMe && userLastDayMessage)
                  if (isMe && userLastDayMessage)
                    Positioned(
                      right: 5,
                      bottom: 0,
                      child: CustomPaint(
                        painter: CustomShape(
                            !isMe
                                ? otherBubbleColor ?? MyColor.danger
                                : myBubbleColor ?? MyColor.success,
                            isMe),
                      ),
                    ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      if (isMe)
                        Container(
                          width: 60,
                        ),
                      // AVATAR
                      if (!isMe)
                        SizedBox(
                          width: 40,
                          height: 40,
                          child: userLastMessage
                              ? InkWell(
                                  onTap: () {
                                    if (onTapUser != null) {
                                      onTapUser!(msg.user!);
                                    }
                                  },
                                  child: Initicon(
                                    backgroundColor: Colors.grey,
                                    text: msg.user!.firstName,
                                    backgroundImageUrl: msg.user!.profileImage,
                                  ),
                                )
                              : Container(),
                        ),
                      if (!isMe) hspace(10),
                      if (!isMe && userLastDayMessage)
                        CustomPaint(
                          painter: CustomShape(
                              !isMe
                                  ? otherBubbleColor ?? MyColor.danger
                                  : myBubbleColor ?? MyColor.success,
                              isMe),
                        ),

                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            if (onTapBubble != null) {
                              onTapBubble!(msg, isMe);
                            }
                          },
                          onLongPress: () {
                            if (onLongPressBubble != null) {
                              onLongPressBubble!(msg, isMe);
                            }
                          },
                          child: ChatBubble(
                            customFeature: customFeature,
                            otherBubbleColor: otherBubbleColor,
                            myBubbleColor: myBubbleColor,
                            isMe: isMe,
                            scrollController: scrollController,
                            matched: matched,
                            messageOptions: messageOptions,
                            messages: messages,
                            msg: msg,
                            chatRoom: chatRoom,
                            userFirstMessage: userFirstMessage,
                            onTap: (offset) {
                              if (onTapBubbleArrow != null) {
                                onTapBubbleArrow!(msg, isMe, offset);
                              }
                            },
                          ),
                        ),
                      ),

                      if (!isMe)
                        Container(
                          width: 60,
                        ),
                      if (isMe) hspace(15),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
