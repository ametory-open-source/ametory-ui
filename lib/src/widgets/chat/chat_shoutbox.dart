import 'package:fluentui_icons/fluentui_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_social_textfield/controller/social_text_editing_controller.dart';

import '../../../ametory_ui.dart';

class ChatShoutBox extends StatelessWidget {
  final SocialTextEditingController messageController;
  final Function(String) onSend;
  final Function()? addAttach;
  final FocusNode? focusNode;
  const ChatShoutBox({
    Key? key,
    this.focusNode,
    required this.onSend,
    required this.addAttach,
    required this.messageController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        constraints: const BoxConstraints(maxHeight: 80),
        decoration: const BoxDecoration(
            border: Border(top: BorderSide(color: Colors.black12))),
        padding: const EdgeInsets.all(10),
        child: Row(
          children: [
            InkWell(
              onTap: () => addAttach!(),
              child: const Icon(
                FluentSystemIcons.ic_fluent_add_regular,
                color: MyColor.danger,
              ),
            ),
            hspace(5),
            Expanded(
              child: SimpleInputBox(
                focusNode: focusNode,
                controller: messageController,
                onSubmitted: (text) => onSend(text),
                borderRadius: BorderRadius.circular(20),
                hintText: "Send Message ....",
                textFieldStyle: MyTypo.bodyText1,
                obscureText: false,
                validate: true,
                backgroundColor: Colors.white,
                keyboardType: TextInputType.multiline,
                maxLines: null,
              ),
            ),
            hspace(5),
            InkWell(
                onTap: messageController.text.isEmpty
                    ? null
                    : () => onSend(messageController.text),
                child: Icon(
                  FluentSystemIcons.ic_fluent_send_filled,
                  color: messageController.text.isEmpty
                      ? Colors.grey
                      : MyColor.danger,
                ))
          ],
        ));
  }
}
