import 'package:flutter/material.dart';

import '../../../ametory_ui.dart';

class ReplyWidget extends StatelessWidget {
  final MyChatMessage msg;
  final EdgeInsets? margin;
  final EdgeInsets? padding;
  final double? width;
  final Function()? onTap;
  const ReplyWidget(
      {Key? key,
      required this.msg,
      this.onTap,
      this.margin,
      this.padding,
      this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          margin: margin,
          padding: padding ?? const EdgeInsets.all(10),
          width: width,
          decoration: const BoxDecoration(
              color: Colors.black54,
              border: Border(left: BorderSide(color: Colors.white, width: 5))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                msg.user!.firstName ?? "",
                style: MyTypo.heading4.copyWith(color: Colors.white),
              ),
              Text(
                msg.text,
                style: MyTypo.bodyText1.copyWith(color: Colors.white),
              ),
            ],
          ),
        ),
        if (onTap != null)
          Positioned(
            top: 0,
            right: 0,
            bottom: 0,
            child: IconButton(
                onPressed: onTap,
                icon: const Icon(Icons.close_rounded, color: Colors.white)),
          ),
      ],
    );
  }
}
