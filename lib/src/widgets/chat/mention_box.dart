import 'package:ametory_ui/ametory_ui.dart';
import 'package:flutter/material.dart';

class MentionBox extends StatelessWidget {
  final List<MyChatUser> mentionUsers;

  final Function(MyChatUser) onTap;
  const MentionBox({Key? key, required this.onTap, required this.mentionUsers})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        constraints:
            BoxConstraints(maxHeight: MediaQuery.of(context).size.height / 4),
        decoration: const BoxDecoration(color: Colors.white),
        child: SingleChildScrollView(
          child: Column(
            children: mentionUsers
                .map((e) => InkWell(
                      onTap: () => onTap(e),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: Row(
                          children: [
                            Initicon(
                              size: 30,
                              text: e.firstName,
                              backgroundColor: Colors.grey,
                              backgroundImageUrl: e.profileImage,
                            ),
                            hspace(10),
                            Expanded(
                                child: Row(
                              children: [
                                Text(
                                  e.firstName!,
                                  style: MyTypo.heading4
                                      .copyWith(color: MyColor.danger),
                                ),
                              ],
                            ))
                          ],
                        ),
                      ),
                    ))
                .toList(),
          ),
        ));
  }
}
