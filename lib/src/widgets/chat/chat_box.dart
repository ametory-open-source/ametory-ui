import 'dart:async';
import 'dart:typed_data';

import 'package:ametory_ui/src/models/chat/chat_notif_model.dart';
import 'package:ametory_ui/src/styles/typo.dart';
import 'package:ametory_ui/src/utils/helper/string.dart';
import 'package:ametory_ui/src/widgets/chat/chat_group_info.dart';
import 'package:ametory_ui/src/widgets/chat/mention_box.dart';
import 'package:ametory_ui/src/widgets/chat/reply_widget.dart';
import 'package:ametory_ui/src/widgets/init_icon.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluentui_icons/fluentui_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_social_textfield/flutter_social_textfield.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:uuid/uuid.dart';

import '../../../ametory_ui.dart';
import '../../models/chat/chat.dart';
import '../../models/chat/room.dart';
import '../../models/user_model.dart';
import 'chat_list.dart';
import 'chat_shoutbox.dart';
import 'chat_writing_widget.dart';

class ChatBox extends StatefulWidget {
  final String companyID;
  final String roomID;
  final bool isPrivate;
  final UserModel? userData;
  final Function(MyChatUser)? onTapDeleteMember;
  final Function(MyChatUser)? onTapUser;
  final Function(MyChatUser, bool isOwner, bool isAdmin)? onLongPressUser;
  final Function()? addUser;
  final Function(MyRoom)? onInfoTap;
  final Function(MyRoom)? onHeaderTap;
  final Function(MyRoom)? onVideoTap;
  final Function(ChatNotifModel)? onSend;
  final DocumentReference<MyRoom>? roomRef;
  final CollectionReference<MyChatMessage>? chatsRef;
  final String roomKey;
  final String chatKey;
  final Color? otherBubbleColor;
  final Color? myBubbleColor;
  final Function()? addAttach;
  final Function(Uint8List)? onGroupInfoImagePick;
  final Function(Uint8List)? onAttachImage;
  final Function(MyChatMessage, bool)? onTapBubble;
  final Function(MyChatMessage, bool)? onLongPressBubble;
  final Function(MyChatMessage, bool)? onSwipe;
  final Function(MyChatMessage, bool, Offset)? onTapBubbleArrow;
  final MyChatMessage? replyMsg;
  final Widget Function(MyChatMessage)? customFeature;

  ChatBox(
      {Key? key,
      required this.roomID,
      this.isPrivate = true,
      this.onTapUser,
      this.userData,
      required this.companyID,
      this.onSend,
      this.roomRef,
      this.chatsRef,
      this.addAttach,
      this.roomKey = "rooms",
      this.chatKey = "chats",
      this.onInfoTap,
      this.onVideoTap,
      this.otherBubbleColor,
      this.myBubbleColor,
      this.onGroupInfoImagePick,
      this.onAttachImage,
      this.onTapDeleteMember,
      this.onLongPressUser,
      this.addUser,
      this.onTapBubble,
      this.onTapBubbleArrow,
      this.onLongPressBubble,
      this.onSwipe,
      this.replyMsg,
      this.customFeature,
      this.onHeaderTap})
      : super(key: key);

  @override
  State<ChatBox> createState() => _ChatBoxState();
}

class _ChatBoxState extends State<ChatBox> {
  late CollectionReference<MyChatMessage> _chatsRef;
  late DocumentReference<MyRoom> _roomRef;
  late AutoScrollController scrollController;
  SocialTextEditingController messageController = SocialTextEditingController();
  // late StreamSubscription<SocialContentDetection> _streamSubscription;
  Stream<QuerySnapshot<MyChatMessage>>? chatSnapshot;
  bool isPrivate = false;
  bool isReadUpdating = false;
  bool showScrollToBottom = false;
  MyChatMessage? _messageToReply;
  String _title = "";
  // String _subtitle = "";
  // String _avatar = "";
  List<MyChatUser> mentioned = [];
  List<MyChatUser> _mentionUsers = [];
  bool showMentionBox = false;
  bool showFooter = false;
  bool showCustomBox = false;
  bool showReplyMessage = false;
  bool showUsersToMention = false;
  MyRoom? _chatRoom;
  TextRange? _textRange;
  late FocusNode _focusNode;
  double headerHeight = 50;

  @override
  void initState() {
    isPrivate = widget.isPrivate;
    initRef();
    scrollController = AutoScrollController(
        viewportBoundaryGetter: () =>
            Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
        axis: Axis.vertical);
    messageController = SocialTextEditingController()
      ..setTextStyle(
          DetectedType.mention,
          TextStyle(
              color: Colors.purple,
              backgroundColor: Colors.purple.withAlpha(50)))
      ..setTextStyle(
          DetectedType.url,
          const TextStyle(
              color: Colors.blue, decoration: TextDecoration.underline))
      ..setTextStyle(DetectedType.hashtag,
          const TextStyle(color: Colors.blue, fontWeight: FontWeight.w600));
    // _streamSubscription = messageController.subscribeToDetection(onDetectContent);
    super.initState();
    messageController.addListener(() {
      if (messageController.text.isEmpty) {
        mentioned.clear();
        SchedulerBinding.instance.addPostFrameCallback((_) {
          setState(() {});
        });
      }
    });
    _focusNode = FocusNode(
      onKey: (FocusNode node, RawKeyEvent evt) {
        if (!evt.isShiftPressed && evt.logicalKey.keyLabel == 'Enter') {
          if (evt is RawKeyDownEvent) {
            _onSend(messageController.text, widget.userData!);
          }
          return KeyEventResult.handled;
        } else {
          return KeyEventResult.ignored;
        }
      },
    );
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(ChatBox oldWidget) {
    // print("UPDATED");
    super.didUpdateWidget(oldWidget);
    initRef();
    chatSnapshot =
        _chatsRef.orderBy("createdAt", descending: true).limit(100).snapshots();
    if (widget.replyMsg != null) {
      _messageToReply = widget.replyMsg;
      showReplyMessage = true;
      showFooter = true;
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    chatSnapshot =
        _chatsRef.orderBy("createdAt", descending: true).limit(100).snapshots();
  }

  initRef() async {
    _roomRef = widget.roomRef ??
        FirebaseFirestore.instance
            .collection(widget.roomKey)
            .doc(widget.roomID)
            .withConverter(
              fromFirestore: (snapshot, _) => MyRoom.fromJson(snapshot.data()!),
              toFirestore: (MyRoom room, _) => room.toJson(),
            );
    _chatsRef = widget.chatsRef ??
        FirebaseFirestore.instance
            .collection(widget.roomKey)
            .doc(widget.roomID)
            .collection(widget.chatKey)
            .withConverter(
              fromFirestore: (snapshot, _) =>
                  MyChatMessage.fromJson(snapshot.data()!),
              toFirestore: (MyChatMessage room, _) => room.toJson(),
            );

    final _chatRoomData = await _roomRef.get();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      setState(() {
        _chatRoom = _chatRoomData.data()!;
      });
    });
  }

  void onDetectContent(SocialContentDetection detection) {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      if (detection.text.isNotEmpty && detection.type == DetectedType.mention) {
        // print(
        //     "Detected Content: ${detection.text.substring(1)} @ ${detection.range.start} -  ${detection.range.end}");

        _mentionUsers = _chatRoom!.users!
            .where((e) => e.firstName!
                .toLowerCase()
                .contains(detection.text.substring(1)))
            .toList();
        showMentionBox = true;
        showFooter = true;
        _textRange = detection.range;
      } else {
        showMentionBox = false;
        _mentionUsers.clear();
        _textRange = null;
      }
      setState(() {});
    });

    // if (mounted) setState(() {});
  }

  swipeMenu(MyChatMessage msg, bool isMe, UserModel userData) async {
    DialogWidget(context,
        width: 300,
        height: 300,
        disableFooter: true,
        padding: EdgeInsets.all(5),
        child: SingleChildScrollView(
          child: Column(
            children: [
              ListTile(
                  title: const Text("Balas"),
                  leading: const Icon(
                      FluentSystemIcons.ic_fluent_arrow_reply_regular),
                  onTap: () {
                    _messageToReply = msg;
                    showFooter = true;
                    showReplyMessage = true;
                    if (mounted) setState(() {});
                    Navigator.of(context).pop();
                  }),
              if (isMe)
                ListTile(
                    title: const Text("Hapus"),
                    leading:
                        const Icon(FluentSystemIcons.ic_fluent_delete_regular),
                    onTap: () async {
                      // print("DELETE ${msg.id}");
                      await _chatsRef.doc(msg.id).delete();
                      Navigator.of(context).pop();
                    }),
              if (isMe)
                ListTile(
                    title: const Text("Info"),
                    leading: const Icon(Icons.info_outline),
                    onTap: () async {
                      Navigator.of(context).pop();
                      messageInfo(msg, isMe, userData);
                    }),
            ],
          ),
        )).show();
  }

  Widget _renderHeader(MyRoom roomData) {
    String name = roomData.name!;
    String description = roomData.description!;
    String avatar = roomData.avatar!;
    if (roomData.isPrivate!) {
      var other =
          roomData.users!.firstWhere((e) => e.id != widget.userData!.id);
      name = other.firstName!;
      description = "";
      avatar = other.profileImage!;
    }
    return Material(
      elevation: 1,
      child: Container(
          color: Colors.white,
          padding: const EdgeInsets.all(5),
          height: headerHeight,
          width: double.infinity,
          child: Row(
            children: [
              hspace(20),
              InkWell(
                onTap: widget.onHeaderTap == null
                    ? null
                    : () {
                        widget.onHeaderTap!(roomData);
                      },
                child: Initicon(
                  size: 30,
                  text: name,
                  backgroundColor: Colors.grey,
                  backgroundImageUrl: avatar,
                ),
              ),
              hspace(20),
              Expanded(
                child: InkWell(
                  onTap: widget.onHeaderTap == null
                      ? null
                      : () {
                          widget.onHeaderTap!(roomData);
                        },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        name,
                        style: MyTypo.heading4.copyWith(fontSize: 14),
                      ),
                      if (description.isNotEmpty)
                        Expanded(
                            child: Tooltip(
                          message: description,
                          child: Text(
                            description,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ))
                    ],
                  ),
                ),
              ),
              Row(
                children: [
                  if (!roomData.isPrivate!)
                    IconButton(
                        onPressed: () {
                          if (widget.onInfoTap != null) {
                            widget.onInfoTap!(roomData);
                          } else {
                            _showInfo();
                          }
                        },
                        icon: const Icon(
                          Icons.info_outline,
                          color: Colors.black54,
                        )),
                  if (widget.onVideoTap != null)
                    IconButton(
                        onPressed: () {
                          widget.onVideoTap!(roomData);
                        },
                        icon: const Icon(Icons.video_call,
                            color: Colors.black54)),
                ],
              )
            ],
          )),
    );
  }

  _showInfo() async {
    // ignore: use_build_context_synchronously
    showDialog(
        context: context,
        builder: (context) => Dialog(
              backgroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: StreamBuilder<DocumentSnapshot<MyRoom>>(
                  stream: _roomRef.snapshots(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      bool isOwner = snapshot.data!.data()!.createdBy ==
                          widget.userData!.id;
                      bool isAdmin = snapshot.data!
                          .data()!
                          .adminIDs!
                          .contains(widget.userData!.id);
                      TextEditingController titleController =
                          TextEditingController();
                      TextEditingController descriptionController =
                          TextEditingController();
                      titleController.text = snapshot.data!.data()!.name!;
                      descriptionController.text =
                          snapshot.data!.data()!.description!;
                      return Container(
                        constraints: BoxConstraints(maxWidth: 600),
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Info Grup",
                                  style: MyTypo.heading4,
                                ),
                                InkWell(
                                    onTap: () => Navigator.of(context).pop(),
                                    child: const Icon(
                                      Icons.close,
                                      color: Colors.black54,
                                    ))
                              ],
                            ),
                            Expanded(
                              child: SingleChildScrollView(
                                child: Column(
                                  children: [
                                    ChatGroupInfo(
                                        onLongPressUser: widget.onLongPressUser,
                                        addUser: widget.addUser,
                                        userData: widget.userData!,
                                        isAdmin: isAdmin,
                                        isOwner: isOwner,
                                        onTapPicture: () async {
                                          final ImagePicker _picker =
                                              ImagePicker();
                                          final XFile? image =
                                              await _picker.pickImage(
                                                  source: ImageSource.gallery);
                                          if (image != null) {
                                            widget.onGroupInfoImagePick!(
                                                (await image.readAsBytes()));
                                          }
                                        },
                                        room: snapshot.data!.data()!,
                                        titleController: titleController,
                                        descriptionController:
                                            descriptionController,
                                        deleteMember: (member) {
                                          if (widget.onTapDeleteMember !=
                                              null) {
                                            widget.onTapDeleteMember!(member);
                                          }
                                        })
                                  ],
                                ),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(),
                                TextButton(
                                  onPressed: () async {
                                    final room = snapshot.data!.data()!;
                                    room.name = titleController.text;
                                    room.description =
                                        descriptionController.text;
                                    await roomUpdate(
                                        room.toJson(), widget.roomID,
                                        roomKey: widget.roomKey);
                                    Navigator.of(context).pop();
                                  },
                                  style: MyStyle.buttonDefault,
                                  child: Text("Simpan"),
                                ),
                              ],
                            )
                          ],
                        ),
                      );
                    }
                    return Container();
                  }),
            ));
    // DialogWidget(
    //   context,
    //   child: ChatGroupInfo(
    //       onTapPicture: () async {
    //         final ImagePicker _picker = ImagePicker();
    //         final XFile? image =
    //             await _picker.pickImage(source: ImageSource.gallery);
    //         if (image != null) {
    //           widget.onGroupInfoImagePick!((await image.readAsBytes()));
    //         }
    //       },
    //       room: room.data()!,
    //       titleController: titleController,
    //       descriptionController: descriptionController,
    //       deleteMember: (member) {
    //         print(member);
    //       }),
    //   onSubmit: () {
    //     room.data()!.name = titleController.text;
    //     room.data()!.description = descriptionController.text;
    //   },
    // ).show();
  }

  @override
  Widget build(BuildContext context) {
    // print("BOX ROOM ID ${widget.roomID}");
    return Column(
      children: [
        Expanded(
            child: StreamBuilder<QuerySnapshot<MyChatMessage>>(
                stream: chatSnapshot!,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    // print("WAITING ....");
                  }
                  if (snapshot.hasData) {
                    return Stack(
                      children: [
                        if (_chatRoom != null)
                          Container(
                            padding:
                                EdgeInsets.fromLTRB(15, headerHeight, 15, 0),
                            child: ListView.builder(
                              cacheExtent: 1000,
                              padding: const EdgeInsets.only(bottom: 20),
                              controller: scrollController,
                              shrinkWrap: true,
                              reverse: true,
                              itemBuilder: (context, index) => ChatListWidget(
                                customFeature: widget.customFeature,
                                onTapBubble: widget.onTapBubble,
                                onTapBubbleArrow: widget.onTapBubbleArrow ??
                                    (m, i, offset) {
                                      swipeMenu(m, i, widget.userData!);
                                    },
                                onLongPressBubble: widget.onLongPressBubble,
                                onTapUser: widget.onTapUser,
                                onSwipe: (msg, _isMe) {
                                  if (widget.onSwipe != null) {
                                    widget.onSwipe!(msg, _isMe);
                                    return;
                                  }
                                  swipeMenu(msg, _isMe, widget.userData!);
                                },
                                onReaded: (msg) {
                                  _sendChatReaded(msg);
                                },
                                otherBubbleColor: widget.otherBubbleColor,
                                myBubbleColor: widget.myBubbleColor,
                                index: index,
                                messages: snapshot.data!.docs,
                                scrollController: scrollController,
                                userData: widget.userData!,
                                chatRoom: _chatRoom,
                              ),
                              itemCount: snapshot.data!.docs.length,
                            ),
                          ),
                        if (_chatRoom != null) _renderHeader(_chatRoom!),
                        if (showScrollToBottom)
                          Positioned(
                            bottom: 10,
                            right: 10,
                            child: InkWell(
                              onTap: () {
                                scrollController.animateTo(
                                  0.0,
                                  duration: const Duration(milliseconds: 300),
                                  curve: Curves.easeInOut,
                                );
                              },
                              child: Container(
                                width: 36,
                                height: 36,
                                decoration: const BoxDecoration(boxShadow: [
                                  BoxShadow(
                                      blurRadius: 3, color: Colors.black38)
                                ], shape: BoxShape.circle, color: Colors.white),
                                child: const Icon(FluentSystemIcons
                                    .ic_fluent_chevron_down_regular),
                              ),
                            ),
                          )
                      ],
                    );
                  }
                  return Container();
                })),
        chatFooterBuilder(),
        ChatWritingWidget(
          roomRef: _roomRef,
          userData: widget.userData!,
        ),
        ChatShoutBox(
          focusNode: _focusNode,
          addAttach: widget.addAttach,
          onSend: (text) {
            _onSend(text, widget.userData!);
          },
          messageController: messageController,
        ),
      ],
    );
  }

  Widget chatFooterBuilder() {
    if (showFooter) {
      return Column(
        children: [
          if (showReplyMessage)
            SizedBox(
              width: double.infinity,
              child: ReplyWidget(
                  width: double.infinity,
                  margin: EdgeInsets.only(top: 10),
                  msg: _messageToReply!,
                  onTap: () {
                    _messageToReply = null;
                    showFooter = false;
                    showReplyMessage = true;
                    setState(() {});
                    // if (mounted) setState(() {});
                  }),
            ),
          if (showMentionBox)
            MentionBox(
              mentionUsers: _mentionUsers,
              onTap: (e) {
                SchedulerBinding.instance.addPostFrameCallback((_) {
                  int index = mentioned.indexWhere((element) => element == e);
                  // print("index $index");
                  if (index < 0) {
                    mentioned.add(e);
                  } else {}
                  messageController.text = messageController.text.replaceRange(
                      _textRange!.start, _textRange!.end, "@${e.firstName!} ");
                  messageController.selection = TextSelection.fromPosition(
                      TextPosition(offset: messageController.text.length - 1));
                  _mentionUsers.clear();
                  showFooter = false;
                  showMentionBox = false;
                  FocusScope.of(context).requestFocus(_focusNode);

                  setState(() {});
                });

                // if (mounted) setState(() {});
              },
            ),
        ],
      );
    }

    return Container();
  }

  _onSend(String textMsg, UserModel _userData, {MyChatMedia? media}) async {
    try {
      List<MyChatUser> _mentioned = [];
      _mentioned.addAll(mentioned);
      messageController.clear();
      String _chatID = const Uuid().v4();
      List<MyChatMedia> chatMedia = [];
      if (isYoutube(textMsg)) {
        chatMedia.add(MyChatMedia(
            uploadedDate: DateTime.now().toLocal(),
            customProperties: {
              "is_youtube": true,
            },
            url: textMsg,
            fileName: textMsg,
            type: MyMediaType.video));
      }
      if (media != null) {
        chatMedia.add(media);
      }
      MyChatMessage chatMessage = MyChatMessage(
          id: _chatID,
          customProperties: {},
          text: textMsg,
          createdAt: DateTime.now().toLocal(),
          medias: chatMedia,
          mentions: _mentioned,
          replyTo: _messageToReply,
          user: MyChatUser(
              id: _userData.id,
              firstName: _userData.name,
              profileImage: _userData.avatar,
              customProperties: {
                "position": _userData.position,
              }));
      if (chatMessage.text.isEmpty && chatMessage.medias!.length == 0) {
        return;
      }

      await _chatsRef.doc(_chatID).set(chatMessage);

      await _roomRef.update({
        "last_message_date": Timestamp.now(),
        "last_message": textMsg,
        "company_id": widget.companyID,
        "readed_ids": [_userData.id]
      });

      String title = _userData.name;
      if (!isPrivate) {
        title += " @ $_title";
      }
      ChatNotifModel notifData = ChatNotifModel(
          title: title,
          body: textMsg,
          chatID: _chatID,
          roomID: widget.roomID,
          userData: _userData,
          sendTo: _chatRoom!.userIDs!.where((e) => e != _userData.id).toList());
      if (widget.onSend != null) {
        widget.onSend!(notifData);
      }
      // if (mounted) setState(() {});
      _messageToReply = null;
      showFooter = false;
      showReplyMessage = false;
      showCustomBox = false;
      // _customFeature = null;
      mentioned.clear();

      await Future.delayed(const Duration(seconds: 1));
      scrollController.animateTo(
        0.0,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeInOut,
      );
    } catch (e) {
      print("ERROR UPDATE CUSTOM FEATURE $e");
    }
  }

  bool isReaded(MyChatMessage _chatData) {
    if (_chatData.customProperties!.containsKey("readedBy") &&
        _chatData.customProperties!["readedBy"]
            .containsKey(widget.userData!.id)) {
      return true;
    }
    return false;
  }

  _sendChatReaded(MyChatMessage _chatData) async {
    final roomReadIDs = await getRoomReadIDs();

    if (isReadUpdating) return;
    if (isReaded(_chatData) && roomReadIDs.contains(widget.userData!.id)) {
      return;
    }

    if (!_chatData.customProperties!.containsKey("readedBy")) {
      _chatData.customProperties!["readedBy"] = {};
    }
    Map<String, dynamic> _readedBy =
        Map<String, dynamic>.from(_chatData.customProperties!["readedBy"]);
    _readedBy[widget.userData!.id] = DateTime.now().microsecondsSinceEpoch;

    if (!_chatData.customProperties!.containsKey("customFeature")) {
      _chatData.customProperties!["customFeature"] = {};
    }

    Map<String, dynamic> _customFeature =
        Map<String, dynamic>.from(_chatData.customProperties!["customFeature"]);
    var data = {
      "customProperties": {
        "readedBy": _readedBy,
        "customFeature": _customFeature
      }
    };

    print("UPDATE ${_chatData.text}");
    try {
      setState(() {
        isReadUpdating = true;
      });
      await _chatsRef.doc(_chatData.id).update(data);
      await _updateRoomReaded(_chatData, roomReadIDs);
      setState(() {
        isReadUpdating = false;
      });
    } catch (e) {
      print("ERROR $e");
    }
  }

  Future<List<String>> getRoomReadIDs() async {
    final _room = await _roomRef.get();
    return _room.data()?.readedIDs ?? [];
  }

  _updateRoomReaded(MyChatMessage _chatData, List<String> roomReadIDs) async {
    // final _room = await _roomRef.get();
    // var readedIDs = _room.data()!.readedIDs;
    if (roomReadIDs.isEmpty) {
      roomReadIDs.add(widget.userData!.id);
    } else {
      var index = roomReadIDs.indexOf(widget.userData!.id);
      if (index < 0) {
        roomReadIDs.add(widget.userData!.id);
      }
    }
    // print("CHAT READED $roomReadIDs");
    await _roomRef.update({"readed_ids": roomReadIDs});
  }

  messageInfo(MyChatMessage msg, bool isMe, UserModel _userData) async {
    List<Widget> listReaded = [];
    Map<String, int> readedBy =
        Map<String, int>.from(msg.customProperties!["readedBy"]);
    final room = await _roomRef.get();
    for (var e in readedBy.keys) {
      if (e != _userData.id) {
        var user = room.data()!.users!.firstWhere((element) => element.id == e);
        var readedAt = DateTime.fromMicrosecondsSinceEpoch(readedBy[e]!);
        listReaded.add(ListTile(
          onTap: () {
            if (widget.onTapUser != null) {
              widget.onTapUser!(user);
            }
          },
          leading: Initicon(
              size: 30,
              text: user.firstName!,
              backgroundColor: Colors.grey,
              backgroundImageUrl: user.profileImage),
          title: Text(
            user.firstName!,
            style:
                MyTypo.heading4.copyWith(fontSize: 14, color: MyColor.danger),
          ),
          trailing: Text(
            DateFormat("dd-MMM HH:mm", "id_ID").format(readedAt),
            style: MyTypo.bodyText1,
          ),
        ));
      }
    }
    DialogWidget(context,
        height: 500,
        child: SingleChildScrollView(
          child: Column(
            children: listReaded,
          ),
        )).show();
  }
}
