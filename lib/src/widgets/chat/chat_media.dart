import 'package:ametory_ui/src/utils/helper/string.dart';
import 'package:ametory_ui/src/widgets/video_player_widget.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:webviewx_plus/webviewx_plus.dart';

import '../../../ametory_ui.dart';
import 'chat_image_preview.dart';

class ChatMedia extends StatelessWidget {
  final MyChatMessage msg;
  final bool isImage;
  final bool isVideo;
  final bool isYoutube;
  final double width;
  final double height;
  const ChatMedia(
      {Key? key,
      this.isImage = false,
      this.isVideo = false,
      this.isYoutube = false,
      required this.msg,
      this.width = 400,
      this.height = 300})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (isImage) {
      return InkWell(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) {
            return ChatImagePreviewWidget(
              url: msg.medias?.first.url,
              caption: msg.text,
            );
          }));
        },
        child: Container(
          margin: const EdgeInsets.only(bottom: 5),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(7),
            child: SizedBox(
              width: 200,
              child: Image.network(msg.medias!.first.url),
            ),
          ),
        ),
      );
    }
    if (isYoutube) {
      return WebViewX(
          width: width,
          height: height,
          initialContent:
              """<iframe width="$width" height="$height" src="https://www.youtube.com/embed/${convertYoutubeUrlToId(msg.medias!.first.url)}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>""",
          initialSourceType: SourceType.html);
      // YoutubePlayerController controller = YoutubePlayerController();
      // controller.loadVideoById(
      //     videoId: convertYoutubeUrlToId(msg.medias!.first.url)!,
      //     startSeconds: 0);

      // return YoutubePlayerScaffold(
      //   controller: controller,
      //   aspectRatio: 16 / 9,
      //   builder: (context, player) {
      //     return Scaffold(
      //       body: player,
      //     );
      //   },
      // );
    }
    if (isVideo) {
      return Stack(
        children: [
          VideoPlayer(VideoPlayerController.networkUrl(
              Uri.parse(msg.medias!.first.url),
              videoPlayerOptions:
                  VideoPlayerOptions(allowBackgroundPlayback: true))),
          Positioned(
              bottom: 10,
              right: 10,
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (_) => VideoPlayeWidget(
                            videoUrl: msg.medias!.first.url,
                          )));
                  // print("${media.url}");
                },
                child: Container(
                    padding: const EdgeInsets.all(5),
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle, color: MyColor.danger),
                    child: const Icon(
                      Icons.fullscreen,
                      color: Colors.white,
                    )),
              )),
        ],
      );
    }
    return Container();
  }
}
