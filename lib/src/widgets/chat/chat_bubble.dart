import 'package:ametory_ui/ametory_ui.dart';
// import 'package:ametory_ui/src/models/chat/chat.dart';
import 'package:ametory_ui/src/utils/helper/string.dart';
import 'package:ametory_ui/src/widgets/chat/reply_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:fluentui_icons/fluentui_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_parsed_text/flutter_parsed_text.dart';
// import 'package:intl/intl.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
// import 'package:video_player/video_player.dart';

import '../../models/chat/message_option.dart';
// import '../../models/chat/room.dart';
import '../video_player_widget.dart';
import 'chat_image_preview.dart';
// import 'chat_media.dart';
import 'chat_readed.dart';

class ChatBubble extends StatelessWidget {
  final AutoScrollController scrollController;
  final List<QueryDocumentSnapshot<MyChatMessage>> messages;
  final bool isMe;
  final bool isPrivate;
  final bool userFirstMessage;
  final MyChatMessage msg;
  final MyMessageOptions messageOptions;
  final List<MatchText> matched;
  final MyRoom? chatRoom;
  final Color? otherBubbleColor;
  final Color? myBubbleColor;
  final Function(Offset)? onTap;
  final Widget Function(MyChatMessage)? customFeature;
  const ChatBubble(
      {Key? key,
      this.isMe = false,
      this.userFirstMessage = false,
      required this.msg,
      this.isPrivate = false,
      required this.scrollController,
      required this.messages,
      required this.messageOptions,
      required this.matched,
      this.chatRoom,
      this.otherBubbleColor,
      this.onTap,
      this.myBubbleColor,
      this.customFeature})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment:
          isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
      children: [
        Container(
          constraints: const BoxConstraints(minWidth: 60, maxWidth: 600),
          margin: EdgeInsets.only(bottom: 5, top: userFirstMessage ? 20 : 0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: !isMe
                  ? otherBubbleColor ?? MyColor.danger
                  : myBubbleColor ?? MyColor.success),
          padding: const EdgeInsets.all(10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Column(
                  crossAxisAlignment:
                      isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
                  children: [
                    if (!isMe && userFirstMessage)
                      Text(
                        msg.user!.firstName!,
                        style: MyTypo.heading4
                            .copyWith(fontSize: 14, color: Colors.white),
                      ),
                    if (!isMe &&
                        !isPrivate &&
                        userFirstMessage &&
                        msg.user!.customProperties != null &&
                        msg.user!.customProperties!.containsKey("position") &&
                        msg.user!.customProperties!["position"] != null)
                      Text(
                        '${msg.user!.customProperties!["position"]}',
                        style: MyTypo.smallText.copyWith(color: Colors.white),
                      ),
                    if (!isMe && !isPrivate && userFirstMessage) vspace(10),
                    if (!isMe && userFirstMessage) vspace(5),
                    if (msg.medias!.isNotEmpty &&
                        msg.medias!.first.type == MyMediaType.image)
                      InkWell(
                        onTap: () {
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return ChatImagePreviewWidget(
                              url: msg.medias?.first.url,
                              caption: msg.text,
                            );
                          }));
                        },
                        child: Container(
                          margin: const EdgeInsets.only(bottom: 5),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(7),
                            child: SizedBox(
                              width: 200,
                              child: Image.network(msg.medias!.first.url),
                            ),
                          ),
                        ),
                      ),
                    if (msg.medias!.isNotEmpty &&
                        msg.medias!.first.type == MyMediaType.video &&
                        !msg.medias!.first.customProperties!
                            .containsKey("is_youtube"))
                      Container(
                        constraints:
                            BoxConstraints(maxWidth: 400, maxHeight: 600),
                        child: Stack(
                          children: [
                            AspectRatio(
                              aspectRatio: 16 / 9,
                              child: MediaPlayer(
                                onTap: (controller) {
                                  if (controller.value.isPlaying) {
                                    controller.pause();
                                  } else {
                                    controller.play();
                                  }
                                },
                                videoUrl: msg.medias!.first.url,
                              ),
                            ),
                            Positioned(
                                bottom: 10,
                                right: 10,
                                child: InkWell(
                                  onTap: () {
                                    Navigator.of(context)
                                        .push(MaterialPageRoute(
                                            builder: (_) => VideoPlayeWidget(
                                                  videoUrl:
                                                      msg.medias!.first.url,
                                                )));
                                    // print("${media.url}");
                                  },
                                  child: Container(
                                      padding: const EdgeInsets.all(5),
                                      decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: MyColor.danger),
                                      child: const Icon(
                                        Icons.fullscreen,
                                        color: Colors.white,
                                      )),
                                )),
                          ],
                        ),
                      ),
                    if (msg.medias!.isNotEmpty &&
                        msg.medias!.first.type == MyMediaType.video &&
                        msg.medias!.first.customProperties!
                            .containsKey("is_youtube"))
                      Container(),
                    if (msg.replyTo != null)
                      InkWell(
                        onTap: () async {
                          var index = messages
                              .indexWhere((e) => e.id == msg.replyTo!.id);
                          print("scroll to $index");
                          // print("scroll to $scrollController");
                          await scrollController.scrollToIndex(index,
                              preferPosition: AutoScrollPosition.begin);
                        },
                        child: Container(
                          margin: const EdgeInsets.only(bottom: 5, top: 20),
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: ReplyWidget(msg: msg.replyTo!)),
                        ),
                      ),
                    ParsedText(
                      parse: matched.isNotEmpty
                          ? matched
                          : (messageOptions.parsePatterns != null
                              ? messageOptions.parsePatterns!
                              : defaultParsePatterns),
                      text: msg.text,
                      style: MyTypo.bodyText1.copyWith(
                        color: isMe
                            ? (messageOptions.currentUserTextColor ??
                                Colors.white)
                            : (messageOptions.textColor ?? Colors.black),
                      ),
                    ),
                    if (msg.customProperties != null &&
                        msg.customProperties!.containsKey("customFeature") &&
                        msg.customProperties!["customFeature"] != null &&
                        customFeature != null)
                      vspace(10),
                    if (msg.customProperties != null &&
                        msg.customProperties!.containsKey("customFeature") &&
                        msg.customProperties!["customFeature"] != null &&
                        customFeature != null)
                      customFeature!(msg),
                    vspace(3),
                    if (chatRoom != null)
                      ChatReaded(
                          msg: msg,
                          isMe: isMe,
                          usersCount: chatRoom!.users!.length),
                  ],
                ),
              ),
              InkWell(
                onTapDown: (details) => _getTapPosition(context, details),
                child: Icon(
                  Icons.arrow_drop_down,
                  color: Colors.white54,
                  size: 14,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  void _getTapPosition(BuildContext context, TapDownDetails details) {
    if (onTap != null) {
      onTap!(details.globalPosition);
    }
  }
}
