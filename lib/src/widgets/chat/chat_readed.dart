import 'package:fluentui_icons/fluentui_icons.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../ametory_ui.dart';

class ChatReaded extends StatelessWidget {
  final MyChatMessage msg;
  final int usersCount;
  final bool isMe;
  const ChatReaded(
      {Key? key, required this.msg, this.usersCount = 0, this.isMe = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        if (isMe && msg.customProperties!.containsKey("readedBy"))
          if (msg.customProperties!["readedBy"].keys.length != usersCount)
            const Icon(
              FluentSystemIcons.ic_fluent_checkmark_regular,
              color: Colors.white,
              size: 14,
            )
          else
            const Stack(
              children: [
                Icon(
                  FluentSystemIcons.ic_fluent_checkmark_regular,
                  color: Colors.white,
                  size: 14,
                ),
                Positioned(
                    left: 5,
                    child: Icon(
                      FluentSystemIcons.ic_fluent_checkmark_regular,
                      color: Colors.white,
                      size: 14,
                    ))
              ],
            ),
        Text(
          DateFormat('HH:mm', 'id_ID').format(msg.createdAt!),
          style: MyTypo.smallText.copyWith(fontSize: 9, color: Colors.white),
          textAlign: isMe ? TextAlign.right : TextAlign.left,
        ),
      ],
    );
  }
}
