import 'package:fluentui_icons/fluentui_icons.dart';
import 'package:flutter/material.dart';

import '../../../ametory_ui.dart';

class ChatGroupInfo extends StatelessWidget {
  final MyRoom room;
  final bool isAdmin;
  final bool isOwner;
  final UserModel userData;
  final Function()? onTapPicture;
  final TextEditingController titleController;
  final TextEditingController descriptionController;
  final Function(MyChatUser)? deleteMember;
  final Function(MyChatUser, bool, bool)? onLongPressUser;
  final Function()? addUser;
  const ChatGroupInfo(
      {Key? key,
      required this.room,
      required this.titleController,
      required this.descriptionController,
      this.isAdmin = false,
      this.isOwner = false,
      this.deleteMember,
      this.onTapPicture,
      required this.userData,
      this.onLongPressUser,
      this.addUser})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          UploadPhotoWidget(
            onTap: onTapPicture,
            onUploaded: (path, url) async {},
            url: room.avatar!,
          ),
          vspace(20),
          Input(
              minHeight: 50,
              height: 50,
              padding: const EdgeInsets.fromLTRB(15, 11, 15, 11),
              // label: "Nama Grup",
              labelSize: 15,
              hintText: "Nama Grup",
              description: "contoh: Marketing ...",
              hintStyle: const TextStyle(fontSize: 12, color: Colors.black12),
              // keyboardType: TextInputType.number,
              controller: titleController,
              fontSize: 16),
          vspace(20),
          Input(
              minHeight: 50,
              padding: const EdgeInsets.fromLTRB(15, 11, 15, 11),
              // label: "Keterangan",
              labelSize: 15,
              minLine: 3,
              maxLine: 5,
              hintText: "Keterangan",
              description: "contoh: Membahas marketing...",
              hintStyle: const TextStyle(fontSize: 12, color: Colors.black12),
              // keyboardType: TextInputType.number,
              controller: descriptionController,
              fontSize: 16),
          vspace(40),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Anggota",
                style: MyTypo.heading4,
              ),
              if (isOwner || isAdmin)
                IconButton(
                    onPressed: () {
                      if (addUser != null) addUser!();
                    },
                    icon: const Icon(
                      FluentSystemIcons.ic_fluent_add_circle_regular,
                      size: 16,
                    ))
            ],
          ),
          vspace(20),
          SizedBox(
            width: double.infinity,
            child: Wrap(
              spacing: 20,
              runSpacing: 20,
              alignment: WrapAlignment.start,
              children: [
                ...room.users!.map((e) {
                  bool memberIsOwner = e.id == room.createdBy;
                  bool memberIsAdmin = room.adminIDs!.contains(e.id);
                  return Stack(
                    children: [
                      InkWell(
                        onLongPress: () async {
                          if (onLongPressUser != null) {
                            onLongPressUser!(e, memberIsOwner, memberIsAdmin);
                          }
                        },
                        child: SizedBox(
                          width: 60,
                          child: Column(
                            children: [
                              Initicon(
                                size: 60,
                                text: e.firstName,
                                backgroundImageUrl: e.profileImage,
                                backgroundColor: Colors.grey,
                              ),
                              vspace(5),
                              Text(
                                e.firstName!,
                                style: MyTypo.smallText
                                    .copyWith(color: Colors.grey),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                      if (isAdmin || isOwner)
                        Positioned(
                          right: 0,
                          top: 0,
                          child: InkWell(
                            onTap: () {
                              if (deleteMember != null) {
                                deleteMember!(e);
                              }
                            },
                            child: Container(
                              width: 20,
                              height: 20,
                              decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(50)),
                              child: Center(
                                child: Text(
                                  "x",
                                  style: MyTypo.smallText
                                      .copyWith(color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                        ),
                      if (memberIsAdmin)
                        Positioned(
                          top: 40,
                          child: Container(
                              padding: const EdgeInsets.all(3),
                              color: MyColor.primary,
                              child: Text(
                                "Admin",
                                style: MyTypo.smallText
                                    .copyWith(color: Colors.white),
                              )),
                        ),
                      if (memberIsOwner)
                        Positioned(
                          top: 40,
                          child: Container(
                              padding: const EdgeInsets.all(3),
                              color: MyColor.danger,
                              child: Text(
                                "Owner",
                                style: MyTypo.smallText
                                    .copyWith(color: Colors.white),
                              )),
                        )
                    ],
                  );
                }).toList()
              ],
            ),
          ),
        ],
      ),
    );
  }
}
