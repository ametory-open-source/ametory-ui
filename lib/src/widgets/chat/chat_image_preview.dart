import 'package:flutter/material.dart';

import '../../models/media_model.dart';

class ChatImagePreviewWidget extends StatefulWidget {
  final String? url;
  final String? caption;
  final Color? backgroundColor;
  final Color? arrowColor;
  final Function(MediaModel)? onShare;
  const ChatImagePreviewWidget(
      {Key? key,
      this.url,
      this.caption,
      this.backgroundColor,
      this.arrowColor,
      this.onShare})
      : super(key: key);

  @override
  State<ChatImagePreviewWidget> createState() => _ChatImagePreviewWidgetState();
}

class _ChatImagePreviewWidgetState extends State<ChatImagePreviewWidget> {
  @override
  void initState() {
    super.initState();
  }

  Widget _renderPreview() {
    return InteractiveViewer(
      child: Stack(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image.network(
              widget.url!,
              fit: BoxFit.contain,
            ),
          ),
          Positioned(
            bottom: 10,
            left: 10,
            child: Container(
                margin: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: Colors.black54,
                    borderRadius: BorderRadius.circular(10)),
                padding: const EdgeInsets.all(10.0),
                // constraints: const BoxConstraints(minHeight: 60),
                // color: Colors.red,
                child: Text(
                  widget.caption!,
                  style: const TextStyle(color: Colors.white, fontSize: 10),
                )),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: widget.backgroundColor ?? Colors.black,
        body: SafeArea(
          child: Stack(
            children: [
              _renderPreview(),
              Positioned(
                  child: IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: Icon(
                        Icons.chevron_left,
                        color: widget.arrowColor ?? Colors.white,
                      ))),
              Positioned(
                  top: 10,
                  right: 10,
                  child: IconButton(
                      onPressed: widget.onShare == null
                          ? null
                          : () {
                              widget.onShare!(MediaModel(
                                  url: widget.url!,
                                  caption: widget.caption!,
                                  extention: widget.url!.split(".").last));
                            },
                      icon: Icon(
                        Icons.share,
                        color: widget.arrowColor ?? Colors.white,
                      )))
            ],
          ),
        ));
  }
}
