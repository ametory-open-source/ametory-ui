import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../../ametory_ui.dart';

class ChatWritingWidget extends StatelessWidget {
  final DocumentReference<MyRoom> roomRef;
  final UserModel userData;
  const ChatWritingWidget(
      {Key? key, required this.roomRef, required this.userData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<DocumentSnapshot<MyRoom>>(
        stream: roomRef.snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data!.data() == null) {
              return Container();
            }
            if (snapshot.data!.data()!.userWritings!.isEmpty) {
              return Container();
            }
            final isWriting = snapshot.data!
                .data()!
                .userWritings!
                .where((e) => e.id != userData.id)
                .toList();
            if (isWriting.isEmpty) return Container();
            return Container(
              width: double.infinity,
              padding: const EdgeInsets.all(10.0),
              child: Text(
                "${isWriting.first.firstName} sedang mengetik ...",
                style: MyTypo.smallText.copyWith(color: Colors.black45),
                textAlign: TextAlign.left,
              ),
            );
          }
          return Container();
        });
  }
}
