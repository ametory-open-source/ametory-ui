import 'package:ametory_ui/src/models/chat/chat.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../ametory_ui.dart';

class ChatDateWidget extends StatelessWidget {
  final MyChatMessage msg;
  const ChatDateWidget({Key? key, required this.msg}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(bottom: 10, top: 20),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.black12,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Text(
          DateTime.now().difference(msg.createdAt!).inDays < 8
              ? DateFormat('EEEE', 'id_ID').format(msg.createdAt!)
              : DateFormat('dd MMM yyyy', 'id_ID').format(msg.createdAt!),
          style: MyTypo.smallText.copyWith(fontSize: 12, color: Colors.black87),
        ));
  }
}
