// ignore_for_file: unused_import

import 'package:ametory_ui/src/widgets/form/simple_input_box.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../ametory_ui.dart';

class ChatRoom extends StatefulWidget {
  final Function()? onTapAdd;
  final Function(MyRoom)? onTap;

  final String companyID;
  final String activeRoomID;
  final UserModel? userData;
  final BoxDecoration? decoration;
  final String roomKey;
  final CollectionReference<MyRoom>? roomsRef;
  final bool hideSearchBar;
  final bool hideAddButton;
  const ChatRoom(
      {Key? key,
      required this.companyID,
      this.userData,
      this.roomsRef,
      this.onTapAdd,
      this.decoration,
      this.onTap,
      this.hideSearchBar = false,
      this.hideAddButton = false,
      required this.activeRoomID,
      this.roomKey = "rooms"})
      : super(key: key);

  @override
  State<ChatRoom> createState() => _ChatRoomState();
}

class _ChatRoomState extends State<ChatRoom> {
  late CollectionReference<MyRoom> _roomsRef;

  @override
  void initState() {
    _roomsRef = widget.roomsRef ??
        FirebaseFirestore.instance.collection(widget.roomKey).withConverter(
              fromFirestore: (snapshot, _) => MyRoom.fromJson(snapshot.data()!),
              toFirestore: (MyRoom room, _) => room.toJson(),
            );
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget _renderRoom(MyRoom room) {
    String title = room.name!;
    String avatar = room.avatar!;
    String subtitle = room.lastMessage!;
    bool unread = false;
    int countRead =
        room.readedIDs!.where((e) => e == widget.userData!.id).length;

    if (countRead == 0 && room.lastMessage!.isNotEmpty) {
      unread = true;
    }

    var others = room.users!.firstWhere((e) => e.id != widget.userData!.id);
    if (room.isPrivate!) {
      title = others.firstName!;
      avatar = others.profileImage!;
    }

    return InkWell(
      onTap: () {
        if (widget.onTap != null) {
          widget.onTap!(room);
        }
      },
      child: Container(
        color: widget.activeRoomID == room.id ? Colors.black12 : null,
        padding: const EdgeInsets.all(10.0),
        child: Row(
          children: [
            Initicon(
              backgroundColor: Colors.grey,
              text: title,
              backgroundImageUrl: avatar,
              size: 30,
            ),
            hspace(10),
            Expanded(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: MyTypo.heading4.copyWith(fontSize: 14),
                ),
                if (subtitle.isNotEmpty)
                  Text(
                    subtitle,
                    style: MyTypo.smallText,
                  ),
              ],
            )),
            hspace(10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(DateFormat('HH:mm').format(room.lastMessageDate!)),
                if (unread) vspace(3),
                if (unread)
                  Container(
                    width: 10,
                    height: 10,
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle, color: MyColor.primary),
                  )
              ],
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      decoration: widget.decoration ??
          const BoxDecoration(
              border: Border(right: BorderSide(color: Colors.black12))),
      child: StreamBuilder<QuerySnapshot<MyRoom>>(
          stream: _roomsRef
              .orderBy('last_message_date', descending: true)
              .where("user_ids", arrayContains: widget.userData!.id)
              .where("company_id", isEqualTo: widget.companyID)
              .snapshots(),
          builder: (BuildContext context,
              AsyncSnapshot<QuerySnapshot<MyRoom>> snapshot) {
            if (snapshot.hasError) {
              // print("error ${snapshot.error}");
              return const Center(child: Text('Something went wrong'));
            }

            // if (snapshot.connectionState == ConnectionState.waiting) {
            //   return const Center(child: Text("Loading"));
            // }
            if (snapshot.hasData) {
              return Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: (widget.hideSearchBar)
                            ? Container()
                            : Container(
                                padding: const EdgeInsets.all(10),
                                child: const Input(
                                  height: 35,
                                  padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                  obscureText: false,
                                  validate: true,
                                  hintText: "Cari user ...",
                                ),
                              ),
                      ),
                      if (!widget.hideAddButton)
                        InkWell(
                            onTap: () {
                              if (widget.onTapAdd != null) {
                                widget.onTapAdd!();
                              }
                            },
                            child: const Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Icon(Icons.add),
                            ))
                    ],
                  ),
                  if (snapshot.data!.docs.isNotEmpty)
                    Expanded(
                        child: ListView.separated(
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              QueryDocumentSnapshot<MyRoom> queryData =
                                  snapshot.data!.docs[index];
                              MyRoom room = queryData.data();
                              room.id = queryData.id;
                              return _renderRoom(room);
                            },
                            separatorBuilder: (context, index) => Container(
                                  width: double.infinity,
                                  height: 1,
                                  color: Colors.black12,
                                ),
                            itemCount: snapshot.data!.docs.length))
                  else
                    const Expanded(child: Center(child: Text("Belum ada chat")))
                ],
              );
            }
            return Container();
          }),
    );
  }
}
