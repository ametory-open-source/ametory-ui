import 'package:fluentui_icons/fluentui_icons.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../ametory_ui.dart';

class UploadPhotoWidget extends StatelessWidget {
  final Function(String, String)? onUploaded;
  final String url;
  final Color? backgroundColor;
  final BoxShape shape;
  final Function(bool)? onLoading;
  final Function()? onTap;
  final Function(XFile?)? onPickImage;
  final double? size;

  const UploadPhotoWidget(
      {Key? key,
      this.onUploaded,
      required this.url,
      this.onLoading,
      this.shape = BoxShape.circle,
      this.backgroundColor,
      this.onTap,
      this.onPickImage,
      this.size})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: url.isNotEmpty
          ? GestureDetector(
              onTap: onTap ??
                  () async {
                    if (onPickImage != null) {
                      final ImagePicker picker = ImagePicker();
                      final XFile? image = await picker.pickImage(source: ImageSource.gallery);
                      onPickImage!(image);
                    }
                  },
              child: (shape == BoxShape.circle)
                  ? SizedBox(
                      width: size != null ? size! / 2 : 100,
                      height: size != null ? size! / 2 : 100,
                      child: CircleAvatar(
                          backgroundColor: backgroundColor,
                          backgroundImage: NetworkImage(
                            url,
                          )))
                  : ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: SizedBox(
                        width: double.infinity,
                        height: size ?? 200,
                        child: Image.network(url, fit: BoxFit.cover),
                      ),
                    ),
            )
          : GestureDetector(
              onTap: onTap ??
                  () async {
                    if (onPickImage != null) {
                      final ImagePicker picker = ImagePicker();
                      final XFile? image = await picker.pickImage(source: ImageSource.gallery);
                      onPickImage!(image);
                    }
                  },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: shape == BoxShape.circle ? (size != null ? size! / 2 : 100) : null,
                    height: shape == BoxShape.circle ? (size != null ? size! / 2 : 100) : 200,
                    decoration: BoxDecoration(color: backgroundColor ?? Colors.white, borderRadius: BorderRadius.circular(shape == BoxShape.circle ? 50 : 20)),
                    child: const Center(
                      child: Icon(
                        FluentSystemIcons.ic_fluent_camera_regular,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  vspace(10),
                  Text(
                    'Tambahkan Photo',
                    style: MyTypo.smallText,
                  )
                ],
              ),
            ),
    );
  }
}
