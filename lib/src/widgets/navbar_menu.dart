import 'package:fluentui_icons/fluentui_icons.dart';
import 'package:flutter/material.dart';

import '../../ametory_ui.dart';

class NavbarMenu extends StatelessWidget {
  final List<NavbarMenuModel> menus;
  final EdgeInsets? padding;
  final EdgeInsets? menuPadding;
  final TextStyle? textStyle;
  final TextStyle? currentTextStyle;
  final BoxDecoration? decoration;
  final BoxDecoration? currentDecoration;
  const NavbarMenu(
      {Key? key, required this.menus, this.padding, this.menuPadding, this.textStyle, this.currentTextStyle, this.decoration, this.currentDecoration})
      : super(key: key);

  Widget renderMenu(BuildContext context, NavbarMenuModel e) {
    var newTextStyle = textStyle ?? MyTypo.bodyText1;
    var newDecoration = decoration;
    if (e.isCurrent) {
      newTextStyle = currentTextStyle ?? MyTypo.bodyText1.copyWith(fontWeight: FontWeight.bold);
      newDecoration = currentDecoration;
    }
    return Container(
      padding: menuPadding ?? const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      decoration: newDecoration,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          InkWell(
            onTap: e.onTap,
            child: Text(
              e.label,
              style: newTextStyle,
            ),
          ),
          if (e.children != null && e.children!.isNotEmpty) hspace(20),
          if (e.children != null && e.children!.isNotEmpty)
            InkWell(
              onTapDown: (details) async {
                final res = await popUpMenu<NavbarMenuModel>(context,
                    offset: details.globalPosition,
                    items: e.children!.map((c) {
                      var newTextStyle = textStyle ?? MyTypo.bodyText1;
                      var newDecoration = decoration;
                      if (c.isCurrent) {
                        newTextStyle = currentTextStyle ?? MyTypo.bodyText1.copyWith(fontWeight: FontWeight.bold);
                        newDecoration = currentDecoration;
                      }
                      return PopupMenuItem(
                        value: c,
                        child: Container(
                          decoration: newDecoration,
                          child: Text(
                            c.label,
                            style: newTextStyle,
                          ),
                        ),
                      );
                    }).toList());
                if (res != null && res.onTap != null) {
                  res.onTap!();
                }
              },
              child: const Icon(
                FluentSystemIcons.ic_fluent_chevron_down_regular,
                size: 14,
                color: Colors.grey,
              ),
            )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding ?? const EdgeInsets.all(0),
      child: Row(
        children: menus.map((e) => renderMenu(context, e)).toList(),
      ),
    );
  }
}
