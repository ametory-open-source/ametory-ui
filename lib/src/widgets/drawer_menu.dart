import 'package:ametory_ui/src/models/menu_model.dart';
import 'package:ametory_ui/src/utils/helper/layout.dart';
import 'package:flutter/material.dart';

import '../models/main_menu_model.dart';

class DrawerMenu extends StatelessWidget {
  final bool showHeader;
  final Widget? header;
  final Widget? beforeMenu;
  final Widget? footer;
  final List<MenuModel>? menu;
  final Color? backgroundColor;
  final EdgeInsets? padding;
  const DrawerMenu({Key? key, this.menu, this.showHeader = true, this.backgroundColor, this.padding, this.header, this.footer, this.beforeMenu})
      : super(key: key);

  Widget _renderItem(MenuModel e) {
    if (e is MainMenuModel) {}
    return InkWell(
      onTap: e.onTap,
      child: Row(
        children: [
          e.icon ?? Container(),
          if (e.icon != null) hspace(10),
          Expanded(child: e.menu!),
          if (e.actions != null)
            Row(
              children: e.actions!,
            ),
        ],
      ),
    );
  }

  Widget _renderMenu(MenuModel e) {
    if (e is MainMenuModel) {
      return Container(
        padding: e.padding ?? const EdgeInsets.symmetric(vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _renderItem(e),
            if (e.children != null && e.children!.isNotEmpty)
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: e.children!.map((e) => Padding(padding: const EdgeInsets.fromLTRB(10, 10, 10, 10), child: _renderItem(e))).toList(),
              )
          ],
        ),
      );
    }
    return Container();
  }

  Widget _renderHeader() {
    if (showHeader) {
      return header ?? Container();
    }
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: SafeArea(
      child: Container(
          padding: padding ?? const EdgeInsets.all(10),
          color: backgroundColor ?? Colors.white,
          child: Column(
              children: menu == null
                  ? [_renderHeader(), beforeMenu ?? Container(), footer ?? Container()]
                  : [
                      _renderHeader(),
                      beforeMenu ?? Container(),
                      Expanded(
                        child: SingleChildScrollView(
                          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: menu!.map((e) => _renderMenu(e)).toList()),
                        ),
                      ),
                      footer ?? Container(),
                    ])),
    ));
  }
}
