import 'package:ametory_ui/ametory_ui.dart';
import 'package:ametory_ui/src/utils/helper/array.dart';
import 'package:ametory_ui/src/utils/helper/date.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../widgets/calendar/day_widget.dart';
import '../widgets/calendar/month_widget.dart';
import '../widgets/calendar/weeks_widget.dart';

class MyCalendar extends StatefulWidget {
  final DateTime? initDay;
  final DateTime? firstDate;
  final DateTime? endDate;
  final bool showEvent;
  final bool showWeekNumber;
  final Function(DateTime)? onChangeMonth;
  final Function(DayModel)? onSelectDay;
  final Function(EventModel)? onSelectEvent;
  final List<EventModel>? events;
  final List<EventModel>? holidays;
  final List<String>? months;

  const MyCalendar(
      {Key? key,
      this.initDay,
      this.firstDate,
      this.months,
      this.onSelectEvent,
      this.endDate,
      this.onChangeMonth,
      this.onSelectDay,
      this.showEvent = true,
      this.showWeekNumber = false,
      this.events,
      this.holidays})
      : super(key: key);

  @override
  State<MyCalendar> createState() => _MyCalendarState();
}

class _MyCalendarState extends State<MyCalendar> with SingleTickerProviderStateMixin {
  AnimationController? expandController;
  Animation<double>? animation;
  late DateTime? initDay;
  late DateTime? firstDate;
  late DateTime? endDate;
  late DateTime? firstDayOfMonth;
  late DateTime? lastDayOfMonth;
  bool showYearPicker = false;
  Size? screenSize;

  late List<EventModel> events = [];
  late List<EventModel> holidays = [];
  DayModel? selectedDay;

  final now = DateTime.now();
  final List<List<DayModel>> _calendarDays = [];
  final List<int> _weeksOfYear = [];
  bool toolbarShow = false;

  List<String> listMonth = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ];

  @override
  void initState() {
    initDay = widget.initDay ?? DateTime.now();
    prepareAnimations();
    super.initState();
    if (widget.events != null) {
      events = widget.events!;
    }
    if (widget.holidays != null) {
      holidays = widget.holidays!;
    }
    if (widget.months != null) {
      listMonth = widget.months!;
    }
    _rebuildData();
  }

  void prepareAnimations() {
    expandController = AnimationController(vsync: this, duration: const Duration(milliseconds: 500));
    animation = CurvedAnimation(
      parent: expandController!,
      curve: Curves.fastOutSlowIn,
    );
  }

  void _runExpandCheck() {
    if (toolbarShow) {
      expandController?.forward();
    } else {
      expandController?.reverse();
    }
  }

  _rebuildData() {
    _calendarDays.clear();
    _weeksOfYear.clear();
    firstDate = widget.firstDate ?? DateTime(DateTime.now().year, 1, 1);
    endDate = widget.endDate ?? DateTime(DateTime.now().year, 12, 31);
    firstDayOfMonth = DateTime(initDay!.year, initDay!.month, 1);
    lastDayOfMonth = DateTime(initDay!.year, initDay!.month + 1, 0);
    for (var i = 0; i < 6; i++) {
      final daysOfWeek = getDaysOfWeekInDate(firstDayOfMonth!.add(Duration(days: i * 7)));
      List<DayModel> listDayOfWeek = [];
      int notDayInMonth = 0;
      for (var e in daysOfWeek) {
        if (e.month != initDay!.month) {
          notDayInMonth++;
        }
        List<EventModel> events = [];
        for (var event in events) {
          if (isThisday(event.startDate!, e)) {
            events.add(event);
          }
        }
        for (var holiday in holidays) {
          if (isThisday(holiday.startDate!, e)) {
            holiday = holiday.copyWith(isHoliday: true);
            events.add(holiday);
          }
        }
        bool enabled = isBeetween(firstDate!, endDate!, e);
        DayModel dayData = DayModel(date: e, events: events, enabled: enabled);

        listDayOfWeek.add(dayData);
      }
      if (notDayInMonth < 7) {
        _calendarDays.add(listDayOfWeek);
        if (widget.showWeekNumber) {
          _weeksOfYear.add(getWeekOfYear(firstDayOfMonth!.add(Duration(days: i * 7))));
        }
      }
    }
    // print(_weeksOfYear);
    setState(() {});
  }

  @override
  void didUpdateWidget(MyCalendar oldWidget) {
    super.didUpdateWidget(oldWidget);
    _runExpandCheck();
    initDay = widget.initDay ?? DateTime.now();
    if (widget.events != null) {
      events = widget.events!;
    }
    if (widget.holidays != null) {
      holidays = widget.holidays!;
    }
    if (widget.months != null) {
      listMonth = widget.months!;
    }
    _rebuildData();
  }

  @override
  void dispose() {
    expandController?.dispose();
    super.dispose();
  }

  Widget _renderFooter() {
    if (selectedDay != null && selectedDay!.events != null) {
      return ListView.separated(
        itemBuilder: (context, index) {
          EventModel event = selectedDay!.events![index];
          return InkWell(
            onTap: widget.onSelectEvent == null ? null : () => widget.onSelectEvent!(event),
            child: Container(
              padding: const EdgeInsets.all(10),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Container(
                    width: 6,
                    height: 37,
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(3), color: event.isHoliday ? MyColor.danger : MyColor.primary),
                  ),
                  hspace(10),
                  Expanded(
                      child: Tooltip(
                    waitDuration: const Duration(milliseconds: 500),
                    verticalOffset: 0,
                    message: event.description,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${event.name}",
                          style: MyTypo.heading4.copyWith(fontSize: 14),
                        ),
                        if (event.location != null)
                          Text(
                            "${event.location}",
                            style: MyTypo.bodyText1,
                          ),
                      ],
                    ),
                  )),
                  if (!event.isHoliday) Text(DateFormat("HH:mm").format(event.startDate!))
                ],
              ),
            ),
          );
        },
        itemCount: selectedDay!.events!.length,
        shrinkWrap: true,
        separatorBuilder: (context, index) => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Container(
            height: 1,
            width: double.infinity,
            color: Colors.black12,
          ),
        ),
      );
    }
    return const Center(
      child: Text('Tidak ada event'),
    );
  }

  Widget _renderCalendar() {
    return Row(
      children: [
        if (widget.showWeekNumber)
          SizedBox(
            width: 30,
            child: Column(
              children: _weeksOfYear
                  .map((e) => Flexible(
                        flex: 1,
                        child: Container(
                            color: Colors.grey.shade100,
                            height: double.infinity,
                            child: Center(
                                child: Text(
                              "$e",
                              style: MyTypo.bodyText1,
                            ))),
                      ))
                  .toList(),
            ),
          ),
        Expanded(
          child: GestureDetector(
            onHorizontalDragUpdate: (details) {
              // Note: Sensitivity is integer used when you don't want to mess up vertical drag
              int sensitivity = 100;
              if (details.delta.dx > sensitivity) {
                // Right Swipe
                prevMonth();
              } else if (details.delta.dx < -sensitivity) {
                nextMonth();
                //Left Swipe
              }
            },
            child: Column(
              children: _calendarDays
                  .map((week) => Flexible(
                      flex: 1,
                      child: Container(
                        decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: Colors.black12))),
                        height: double.infinity,
                        child: Row(
                          children: week
                              .map((data) => DayWidget(
                                  selectedMonth: initDay!,
                                  onTap: (day) {
                                    selectedDay = day;
                                    if (widget.onSelectDay != null) {
                                      widget.onSelectDay!(selectedDay!);
                                    }
                                    setState(() {});
                                  },
                                  isSelected: selectedDay == null
                                      ? false
                                      : DateTime(selectedDay!.date!.year, selectedDay!.date!.month, selectedDay!.date!.day) ==
                                          DateTime(data.date!.year, data.date!.month, data.date!.day),
                                  day: data))
                              .toList(),
                        ),
                      )))
                  .toList(),
            ),
          ),
        ),
      ],
    );
  }

  setMonth(DateTime day) {
    initDay = day;
    setState(() {});
    if (widget.onChangeMonth != null) {
      widget.onChangeMonth!(initDay!);
    }
    selectedDay = null;
    _rebuildData();
  }

  prevMonth() {
    initDay = DateTime(initDay!.year, initDay!.month - 1, 1);
    setState(() {});
    if (widget.onChangeMonth != null) {
      widget.onChangeMonth!(initDay!);
    }
    selectedDay = null;
    _rebuildData();
  }

  nextMonth() {
    initDay = DateTime(initDay!.year, initDay!.month + 1, 1);
    setState(() {});
    if (widget.onChangeMonth != null) {
      widget.onChangeMonth!(initDay!);
    }
    selectedDay = null;
    _rebuildData();
  }

  Widget _renderToolbar(double width) {
    return SizeTransition(
        axisAlignment: 1.0,
        sizeFactor: animation!,
        child: SizedBox(
          width: double.infinity,
          height: 120,
          child: showYearPicker
              ? YearPicker(
                  firstDate: firstDate!,
                  lastDate: endDate!,
                  selectedDate: initDay!,
                  currentDate: initDay!,
                  onChanged: (val) {
                    initDay = DateTime(val.year, initDay!.month, 1);
                    showYearPicker = false;
                    setState(() {});
                  })
              : Row(
                  children: [
                    Flexible(
                        flex: 7,
                        child: Column(
                          children: arrayChunk(listMonth, 3)
                              .map((List<dynamic> group) => Row(
                                    children: group.map((e) {
                                      var index = listMonth.indexOf(e);
                                      bool selected = initDay!.month == index + 1;
                                      return Flexible(
                                        flex: 1,
                                        child: InkWell(
                                          onTap: () {
                                            setMonth(DateTime(initDay!.year, index + 1, 1));
                                            // toolbarShow = !toolbarShow;
                                            setState(() {});
                                            _runExpandCheck();
                                          },
                                          child: Container(
                                            padding: const EdgeInsets.all(3),
                                            // width: width / 4,
                                            height: 120 / 4,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  border: Border.all(color: selected ? MyColor.primary : Colors.black12),
                                                  borderRadius: BorderRadius.circular(20)),
                                              padding: const EdgeInsets.all(3),
                                              child: Center(
                                                child: Text(
                                                  e,
                                                  style: MyTypo.smallText.copyWith(color: selected ? MyColor.primary : Colors.black87),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      );
                                    }).toList(),
                                  ))
                              .toList(),
                        )
                        // Wrap(
                        //   children: listMonth.map(
                        //     (e) {
                        //       ;
                        //     },
                        //   ).toList(),
                        // ),
                        ),
                    Flexible(
                        flex: 1,
                        child: Center(
                          child: IconButton(
                              onPressed: () {
                                setState(() {
                                  showYearPicker = true;
                                });
                              },
                              icon: const Icon(Icons.chevron_right_outlined)),
                        ))
                  ],
                ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    double heightRatio = ResponsiveWidget.isSmallScreen(context) ? 2.5 : 2;
    return WidgetSize(
      onChange: (size) {
        setState(() {
          screenSize = size;
        });
      },
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: [
            MonthWidget(
              months: listMonth,
              onTap: () {
                setState(() {
                  toolbarShow = !toolbarShow;
                  _runExpandCheck();
                });
              },
              day: initDay!,
              prev: IconButton(onPressed: prevMonth, icon: const Icon(Icons.chevron_left_rounded)),
              next: IconButton(onPressed: nextMonth, icon: const Icon(Icons.chevron_right_rounded)),
            ),
            _renderToolbar(screenSize != null ? screenSize!.width : MediaQuery.of(context).size.width),
            WeeksWidget(
              showWeekNumber: widget.showWeekNumber,
              useAbbreviation: ResponsiveWidget.isSmallScreen(context),
            ),
            if (widget.showEvent)
              Container(
                  constraints: BoxConstraints(
                      maxHeight: _calendarDays.length > 5
                          ? ((screenSize != null ? screenSize!.height : MediaQuery.of(context).size.height) / (heightRatio * 5) * 6)
                          : (screenSize != null ? screenSize!.height : MediaQuery.of(context).size.height) / heightRatio),
                  child: _renderCalendar()),
            Expanded(child: widget.showEvent ? _renderFooter() : _renderCalendar())
          ],
        ),
      ),
    );
  }
}
