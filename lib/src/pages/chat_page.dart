import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../ametory_ui.dart';
import '../models/chat/chat_notif_model.dart';

class ChatPage extends StatefulWidget {
  final bool hideChatRoom;
  final Function()? onTapAdd;
  final Function(ChatNotifModel)? onSend;
  final Function(MyRoom)? onRoomTap;
  final Function(MyChatUser)? onTapUser;
  final Function(MyChatUser)? onTapDeleteMember;
  final Function(MyChatMessage, bool)? onTapBubble;
  final Function(MyChatMessage, bool)? onLongPressBubble;

  final Function(MyChatUser, bool isOwner, bool isAdmin)? onLongPressUser;
  final Function()? addUser;
  final UserModel userData;
  final Color? otherBubbleColor;
  final Color? myBubbleColor;
  final String companyID;
  final String roomID;
  final String roomKey;
  final String chatKey;
  final CollectionReference<MyRoom>? roomsRef;
  final CollectionReference<MyChatMessage>? chatsRef;
  final DocumentReference<MyRoom>? roomRef;
  final Function(MyRoom)? onInfoTap;
  final Function(MyRoom)? onVideoTap;
  final Function()? addAttach;
  final Function(Uint8List)? onGroupInfoImagePick;
  final Function(MyChatMessage, bool)? onSwipe;
  final Function(MyChatMessage, bool, Offset)? onTapBubbleArrow;
  final MyChatMessage? replyMsg;
  final Widget Function(MyChatMessage)? customFeature;
  final Function(MyRoom)? onHeaderTap;
  const ChatPage({
    Key? key,
    required this.userData,
    required this.companyID,
    this.hideChatRoom = false,
    this.onTapAdd,
    required this.roomID,
    this.onRoomTap,
    this.roomKey = "rooms",
    this.chatKey = "chats",
    this.onSend,
    this.addAttach,
    this.roomsRef,
    this.roomRef,
    this.chatsRef,
    this.onInfoTap,
    this.onVideoTap,
    this.otherBubbleColor,
    this.myBubbleColor,
    this.onTapUser,
    this.onGroupInfoImagePick,
    this.onTapDeleteMember,
    this.onLongPressUser,
    this.addUser,
    this.onTapBubble,
    this.onLongPressBubble,
    this.onSwipe,
    this.onTapBubbleArrow,
    this.replyMsg,
    this.customFeature,
    this.onHeaderTap,
  }) : super(key: key);

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: double.infinity,
      child: Row(
        children: [
          if (!widget.hideChatRoom)
            ChatRoom(
              roomsRef: widget.roomsRef,
              activeRoomID: widget.roomID,
              onTap: widget.onRoomTap,
              onTapAdd: widget.onTapAdd,
              companyID: widget.companyID,
              userData: widget.userData,
              roomKey: widget.roomKey,
            ),
          Expanded(
              child: widget.roomID.isEmpty
                  ? const Center(
                      child: Text("Pilih Chat Room"),
                    )
                  : ChatBox(
                      onHeaderTap: widget.onHeaderTap,
                      customFeature: widget.customFeature,
                      onTapBubbleArrow: widget.onTapBubbleArrow,
                      replyMsg: widget.replyMsg,
                      onSwipe: widget.onSwipe,
                      onTapBubble: widget.onTapBubble,
                      onLongPressBubble: widget.onLongPressBubble,
                      onLongPressUser: widget.onLongPressUser,
                      addUser: widget.addUser,
                      onTapDeleteMember: widget.onTapDeleteMember,
                      onGroupInfoImagePick: widget.onGroupInfoImagePick,
                      onTapUser: widget.onTapUser,
                      otherBubbleColor: widget.otherBubbleColor,
                      myBubbleColor: widget.myBubbleColor,
                      onInfoTap: widget.onInfoTap,
                      onVideoTap: widget.onVideoTap,
                      addAttach: widget.addAttach,
                      roomRef: widget.roomRef,
                      chatsRef: widget.chatsRef,
                      roomKey: widget.roomKey,
                      chatKey: widget.chatKey,
                      companyID: widget.companyID,
                      roomID: widget.roomID,
                      userData: widget.userData,
                      onSend: (notifData) {
                        if (widget.onSend != null) {
                          widget.onSend!(notifData);
                        }
                      },
                    )),
        ],
      ),
    );
  }
}
