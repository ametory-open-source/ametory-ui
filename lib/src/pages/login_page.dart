import 'package:ametory_ui/ametory_ui.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

class MyLoginPage extends StatefulWidget {
  final Function(UserCredential)? onSubmitGoogle;
  final Color? backgroundColor;
  final Gradient? gradient;
  final Color? boxColor;
  final Color? inputBackgroundColor;
  final BorderRadius? boxRadius;
  final Widget? title;
  final Widget? subtitle;
  final Widget? footer;
  final Widget? beforeButton;
  final Widget? header;
  final List<BoxShadow>? boxShadow;
  final double? boxWidth;
  final double? boxHeight;
  final TextEditingController? emailController;
  final TextEditingController? passwordController;
  final Function()? onSubmit;
  final MyLoginPageTemplate template;
  final bool enableGoogleLogin;
  final Widget? rightColumn;
  final ButtonStyle? buttonStyle;
  final EdgeInsets? padding;
  final String? hintText;
  final TextStyle? hintStyle;
  final double? buttonHeight;
  final bool reverseColumn;
  final String buttonText;
  final TextStyle? googleButtonTextStyle;
  final ButtonStyle? googleButtonStyle;
  const MyLoginPage(
      {Key? key,
      this.boxWidth,
      this.backgroundColor,
      this.boxColor,
      this.inputBackgroundColor,
      this.boxRadius,
      this.boxShadow,
      this.title,
      this.subtitle,
      this.boxHeight,
      this.onSubmit,
      this.onSubmitGoogle,
      this.footer,
      this.beforeButton,
      this.header,
      this.emailController,
      this.passwordController,
      this.gradient,
      this.rightColumn,
      this.buttonStyle,
      this.buttonHeight,
      this.template = MyLoginPageTemplate.box,
      this.enableGoogleLogin = true,
      this.reverseColumn = false,
      this.padding,
      this.hintText,
      this.buttonText = "Login",
      this.hintStyle,
      this.googleButtonTextStyle,
      this.googleButtonStyle})
      : super(key: key);

  @override
  State<MyLoginPage> createState() => _MyLoginPageState();
}

class _MyLoginPageState extends State<MyLoginPage> {
  double? width;
  double? height;
  bool obscureText = true;

  Widget box() {
    return Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
            color: widget.backgroundColor ?? MyColor.info,
            gradient: widget.gradient ??
                LinearGradient(colors: [
                  MyColor.info,
                  MyColor.info.shade100,
                ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
        child: Container(
          padding: widget.padding ?? EdgeInsets.all(ResponsiveWidget.isSmallScreen(context) ? 20 : 40),
          alignment: Alignment.center,
          child: WidgetSize(
            onChange: (Size size) {
              width = size.width;
              height = size.height;
              setState(() {});
            },
            child: Container(
              decoration: BoxDecoration(
                  boxShadow: widget.boxShadow ?? [const BoxShadow(color: Colors.black45, blurRadius: 10)],
                  color: widget.boxColor ?? Colors.white,
                  borderRadius: widget.boxRadius ?? BorderRadius.circular(10)),
              padding: EdgeInsets.all(ResponsiveWidget.isSmallScreen(context) ? 20 : 40),
              width: ResponsiveWidget.isSmallScreen(context) ? double.infinity : widget.boxWidth ?? 400,
              // height: widget.boxHeight ?? MediaQuery.of(context).size.height,
              child: form(),
            ),
          ),
        ));
  }

  Widget form() {
    return SingleChildScrollView(
      child: Column(
        children: [
          widget.title ??
              Text(
                "Login",
                style: MyTypo.heading1,
              ),
          vspace(10),
          widget.subtitle ?? Container(),
          vspace(40),
          widget.header ?? Container(),
          Input(
            backgroundColor: widget.inputBackgroundColor,
            keyboardType: TextInputType.emailAddress,
            leading: !ResponsiveWidget.isLargeScreen(context) || widget.template == MyLoginPageTemplate.column
                ? null
                : SizedBox(
                    child: Row(
                      children: [
                        SizedBox(
                          width: width != null ? width! / 6 : 0,
                          child: Text(
                            "Email",
                            style: MyTypo.heading4.copyWith(fontSize: 12, color: Colors.black54),
                          ),
                        ),
                        hspace(10)
                      ],
                    ),
                  ),
            onChanged: (val) {
              setState(() {});
            },
            controller: widget.emailController,
            hintText: widget.hintText ?? "Email",
            hintStyle: widget.hintStyle,
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            onClear: widget.emailController != null && widget.emailController!.text.isNotEmpty
                ? () {
                    widget.emailController!.clear();
                  }
                : null,
          ),
          vspace(20),
          Input(
            backgroundColor: widget.inputBackgroundColor,
            obscureText: obscureText,
            leading: !ResponsiveWidget.isLargeScreen(context) || widget.template == MyLoginPageTemplate.column
                ? null
                : SizedBox(
                    child: Row(
                      children: [
                        SizedBox(
                          width: width != null ? width! / 6 : 0,
                          child: Text(
                            "Password",
                            style: MyTypo.heading4.copyWith(fontSize: 12, color: Colors.black54),
                          ),
                        ),
                        hspace(10)
                      ],
                    ),
                  ),
            controller: widget.passwordController,
            hintText: widget.hintText ?? "Password",
            hintStyle: widget.hintStyle,
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            trailing: InkWell(
              onTap: () => setState(() => obscureText = !obscureText),
              child: const Icon(
                Icons.remove_red_eye_outlined,
                size: 16,
                color: Colors.black38,
              ),
            ),
          ),
          widget.beforeButton ?? Container(),
          vspace(80),
          SizedBox(
            height: widget.buttonHeight ?? 50,
            width: widget.template == MyLoginPageTemplate.column ? double.infinity : 200,
            child: TextButton(
                onPressed: widget.onSubmit,
                style: widget.buttonStyle ?? MyStyle.buttonDefault,
                child: Text(
                  widget.buttonText,
                  style: MyTypo.whiteColor,
                )),
          ),
          if (widget.enableGoogleLogin) vspace(20),
          if (widget.enableGoogleLogin)
            SizedBox(
                height: widget.buttonHeight ?? 50,
                width: widget.template == MyLoginPageTemplate.column ? double.infinity : 200,
                child: TextButton.icon(
                    onPressed: () async {
                      try {
                        // await FirebaseAuth.instance.signOut();
                        final user = await signInWithGoogle();
                        if (widget.onSubmitGoogle != null) {
                          widget.onSubmitGoogle!(user);
                        }
                      } catch (e) {
                        // print("ERROR $e");
                      }
                    },
                    icon: Image.asset(
                      width: 30,
                      'assets/images/logo-google.png',
                      package: "ametory_ui",
                    ),
                    label: Text("Login By Google", style: widget.googleButtonTextStyle),
                    style: widget.googleButtonStyle ?? MyStyle.buttonWhite)),
          widget.footer ?? Container(),
        ],
      ),
    );
  }

  Widget bgCol() {
    if (!ResponsiveWidget.isLargeScreen(context)) return Container();
    if (widget.rightColumn != null) return Expanded(child: widget.rightColumn!);
    return Expanded(
        child: Stack(children: [
      Positioned.fill(
          child: Image.network(
        "https://img.freepik.com/free-photo/flat-lay-office-desk-assortment-with-empty-screen-tablet_23-2148707960.jpg",
        fit: BoxFit.cover,
      ))
    ]));
  }

  Widget col() {
    return Container(
      decoration: BoxDecoration(color: widget.backgroundColor, gradient: widget.gradient),
      child: WidgetSize(
        onChange: (Size size) {
          width = size.width;
          height = size.height;
          setState(() {});
        },
        child: Row(
          children: [
            if (widget.reverseColumn) bgCol(),
            Expanded(
                child: Padding(
              padding: widget.padding ?? (!ResponsiveWidget.isLargeScreen(context) ? const EdgeInsets.all(40) : const EdgeInsets.fromLTRB(160, 40.0, 160, 40)),
              child: form(),
            )),
            if (!widget.reverseColumn) bgCol()
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    switch (widget.template) {
      case MyLoginPageTemplate.box:
        return box();
      case MyLoginPageTemplate.column:
        return col();
      default:
        return box();
    }
  }

  Future<UserCredential> signInWithGoogle() async {
    // Trigger the authentication flow
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication? googleAuth = await googleUser?.authentication;

    // Create a new credential
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth?.accessToken,
      idToken: googleAuth?.idToken,
    );

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }
}

enum MyLoginPageTemplate { box, column }
