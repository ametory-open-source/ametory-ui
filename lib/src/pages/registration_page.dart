import 'package:ametory_ui/ametory_ui.dart';
import 'package:flutter/material.dart';

class MyRegistrationPage extends StatefulWidget {
  final Color? backgroundColor;
  final Color? boxColor;
  final BorderRadius? boxRadius;
  final Widget? title;
  final Widget? subtitle;
  final Widget? footer;
  final Widget? beforeButton;
  final Widget? header;
  final List<BoxShadow>? boxShadow;
  final double? boxWidth;
  final double? boxHeight;
  final TextEditingController? nameController;
  final TextEditingController? emailController;
  final TextEditingController? phoneController;
  final TextEditingController? addressController;
  final TextEditingController? passwordController;
  final TextEditingController? confirmController;
  final Function()? onSubmit;
  final Gradient? gradient;
  final MyLoginPageTemplate template;
  final Widget? rightColumn;
  final ButtonStyle? buttonStyle;
  final EdgeInsets? padding;
  final String? hintText;
  final TextStyle? hintStyle;
  final double? buttonHeight;
  final bool reverseColumn;
  final String buttonText;

  const MyRegistrationPage(
      {Key? key,
      this.boxWidth,
      this.backgroundColor,
      this.boxColor,
      this.boxRadius,
      this.boxShadow,
      this.title,
      this.subtitle,
      this.boxHeight,
      this.onSubmit,
      this.nameController,
      this.footer,
      this.beforeButton,
      this.header,
      this.emailController,
      this.phoneController,
      this.addressController,
      this.passwordController,
      this.confirmController,
      this.gradient,
      required this.template,
      this.rightColumn,
      this.buttonStyle,
      this.padding,
      this.hintText,
      this.hintStyle,
      this.reverseColumn = false,
      this.buttonText = "Daftar Sekarang",
      this.buttonHeight})
      : super(key: key);

  @override
  State<MyRegistrationPage> createState() => _MyRegistrationPageState();
}

class _MyRegistrationPageState extends State<MyRegistrationPage> {
  double? width;
  double? height;
  bool obscureText = true;

  Widget box() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
          color: widget.backgroundColor ?? MyColor.info,
          gradient: widget.gradient ??
              LinearGradient(colors: [
                MyColor.info,
                MyColor.info.shade100,
              ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
      child: Container(
        padding: EdgeInsets.all(ResponsiveWidget.isSmallScreen(context) ? 20 : 40),
        alignment: Alignment.center,
        child: WidgetSize(
          onChange: (Size size) {
            width = size.width;
            height = size.height;
            setState(() {});
          },
          child: Container(
              decoration: BoxDecoration(
                  boxShadow: widget.boxShadow ?? [const BoxShadow(color: Colors.black45, blurRadius: 10)],
                  color: widget.boxColor ?? Colors.white,
                  borderRadius: widget.boxRadius ?? BorderRadius.circular(10)),
              padding: EdgeInsets.all(ResponsiveWidget.isSmallScreen(context) ? 20 : 40),
              width: ResponsiveWidget.isSmallScreen(context) ? double.infinity : widget.boxWidth ?? 800,
              // height: widget.boxHeight ?? MediaQuery.of(context).size.height,
              child: form()),
        ),
      ),
    );
  }

  Widget phone() {
    return Input(
      keyboardType: TextInputType.number,
      leading: !ResponsiveWidget.isLargeScreen(context) || widget.template == MyLoginPageTemplate.column
          ? null
          : SizedBox(
              child: Row(
                children: [
                  SizedBox(
                    width: width != null ? width! / 8 : 0,
                    child: Text(
                      "Telepon",
                      style: MyTypo.heading4.copyWith(fontSize: 12, color: Colors.black54),
                    ),
                  ),
                  hspace(10)
                ],
              ),
            ),
      controller: widget.phoneController,
      hintText: "Telepon",
      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
    );
  }

  Widget confirm() {
    return Input(
      obscureText: obscureText,
      leading: !ResponsiveWidget.isLargeScreen(context) || widget.template == MyLoginPageTemplate.column
          ? null
          : SizedBox(
              child: Row(
                children: [
                  SizedBox(
                    width: width != null ? width! / 8 : 0,
                    child: Text(
                      "Ulangi",
                      style: MyTypo.heading4.copyWith(fontSize: 12, color: Colors.black54),
                    ),
                  ),
                  hspace(10)
                ],
              ),
            ),
      controller: widget.confirmController,
      hintText: "Ulangi Password",
      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      trailing: InkWell(
        onTap: () => setState(() => obscureText = !obscureText),
        child: const Icon(
          Icons.remove_red_eye_outlined,
          size: 16,
          color: Colors.black38,
        ),
      ),
    );
  }

  Widget form() {
    return SingleChildScrollView(
      child: Column(
        children: [
          widget.title ??
              Text(
                "Pendaftaran",
                style: MyTypo.heading1,
              ),
          vspace(10),
          widget.subtitle ??
              Text(
                "Silakan isi data lengkap anda",
                style: MyTypo.bodyText1,
              ),
          vspace(40),
          widget.header ?? Container(),
          Input(
            leading: !ResponsiveWidget.isLargeScreen(context) || widget.template == MyLoginPageTemplate.column
                ? null
                : SizedBox(
                    child: Row(
                      children: [
                        SizedBox(
                          width: width != null ? width! / 8 : 0,
                          child: Text(
                            "Nama Lengkap",
                            style: MyTypo.heading4.copyWith(fontSize: 12, color: Colors.black54),
                          ),
                        ),
                        hspace(10)
                      ],
                    ),
                  ),
            controller: widget.nameController,
            hintText: "Nama Lengkap",
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          ),
          vspace(20),
          Row(
            children: [
              Flexible(
                child: Input(
                  keyboardType: TextInputType.emailAddress,
                  leading: !ResponsiveWidget.isLargeScreen(context) || widget.template == MyLoginPageTemplate.column
                      ? null
                      : SizedBox(
                          child: Row(
                            children: [
                              SizedBox(
                                width: width != null ? width! / 8 : 0,
                                child: Text(
                                  "Email",
                                  style: MyTypo.heading4.copyWith(fontSize: 12, color: Colors.black54),
                                ),
                              ),
                              hspace(10)
                            ],
                          ),
                        ),
                  controller: widget.emailController,
                  hintText: "Email",
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                ),
              ),
              if (widget.template == MyLoginPageTemplate.box) hspace(10),
              if (widget.template == MyLoginPageTemplate.box) Flexible(child: phone()),
            ],
          ),
          vspace(20),
          if (widget.template == MyLoginPageTemplate.column) phone(),
          if (widget.template == MyLoginPageTemplate.column) vspace(20),
          Row(
            children: [
              Flexible(
                child: Input(
                  obscureText: obscureText,
                  leading: !ResponsiveWidget.isLargeScreen(context) || widget.template == MyLoginPageTemplate.column
                      ? null
                      : SizedBox(
                          child: Row(
                            children: [
                              SizedBox(
                                width: width != null ? width! / 8 : 0,
                                child: Text(
                                  "Password",
                                  style: MyTypo.heading4.copyWith(fontSize: 12, color: Colors.black54),
                                ),
                              ),
                              hspace(10)
                            ],
                          ),
                        ),
                  controller: widget.passwordController,
                  hintText: "Password",
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  trailing: InkWell(
                    onTap: () => setState(() => obscureText = !obscureText),
                    child: const Icon(
                      Icons.remove_red_eye_outlined,
                      size: 16,
                      color: Colors.black38,
                    ),
                  ),
                ),
              ),
              if (widget.template == MyLoginPageTemplate.box) hspace(10),
              if (widget.template == MyLoginPageTemplate.box) Flexible(child: confirm()),
            ],
          ),
          vspace(20),
          if (widget.template == MyLoginPageTemplate.column) confirm(),
          if (widget.template == MyLoginPageTemplate.column) vspace(20),
          Input(
            height: null,
            maxLine: 3,
            leading: !ResponsiveWidget.isLargeScreen(context) || widget.template == MyLoginPageTemplate.column
                ? null
                : SizedBox(
                    child: Row(
                      children: [
                        SizedBox(
                          width: width != null ? width! / 8 : 0,
                          child: Text(
                            "Alamat",
                            style: MyTypo.heading4.copyWith(fontSize: 12, color: Colors.black54),
                          ),
                        ),
                        hspace(10)
                      ],
                    ),
                  ),
            controller: widget.addressController,
            hintText: "Alamat",
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
          ),
          vspace(20),
          widget.beforeButton ?? Container(),
          vspace(20),
          SizedBox(
            height: widget.buttonHeight ?? 50,
            width: widget.template == MyLoginPageTemplate.column ? double.infinity : 200,
            child: TextButton(
                onPressed: widget.onSubmit,
                // ignore: sort_child_properties_last
                child: Text(
                  widget.buttonText,
                  style: MyTypo.whiteColor,
                ),
                style: widget.buttonStyle ?? MyStyle.buttonDefault),
          ),
          widget.footer ?? Container(),
        ],
      ),
    );
  }

  Widget bgCol() {
    if (!ResponsiveWidget.isLargeScreen(context)) return Container();
    return Expanded(
        child: Stack(children: [
      Positioned.fill(
          child: Image.network(
        "https://img.freepik.com/free-photo/flat-lay-office-desk-assortment-with-empty-screen-tablet_23-2148707960.jpg",
        fit: BoxFit.cover,
      ))
    ]));
  }

  Widget col() {
    return Container(
      decoration: BoxDecoration(color: widget.backgroundColor, gradient: widget.gradient),
      child: WidgetSize(
        onChange: (Size size) {
          width = size.width;
          height = size.height;
          setState(() {});
        },
        child: Row(
          children: [
            if (widget.reverseColumn) bgCol(),
            Expanded(
                child: Padding(
              padding: widget.padding ?? (!ResponsiveWidget.isLargeScreen(context) ? const EdgeInsets.all(40) : const EdgeInsets.fromLTRB(160, 40.0, 160, 40)),
              child: form(),
            )),
            if (!widget.reverseColumn) bgCol()
          ],
        ),
      ),
    );
  }

// This function is trggered somehow after build() called
  @override
  Widget build(BuildContext context) {
    switch (widget.template) {
      case MyLoginPageTemplate.box:
        return box();
      case MyLoginPageTemplate.column:
        return col();
      default:
        return box();
    }
  }
}
