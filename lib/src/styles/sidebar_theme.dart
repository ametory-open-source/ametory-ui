import 'package:flutter/material.dart';

class SidebarTheme {
  final bool isAuth;
  final bool isCollapsed;
  final double? width;
  final Color? color;
  final Color? expandedIconColor;
  final Color? collapsedIconColor;
  final double? elevation;
  final BoxDecoration? decoration;
  final EdgeInsets? padding;

  final Duration? duration;
  final Curve? curve;

  SidebarTheme(
      {this.isAuth = false,
      this.isCollapsed = false,
      this.expandedIconColor,
      this.collapsedIconColor,
      this.duration,
      this.curve,
      this.width,
      this.color,
      this.elevation,
      this.decoration,
      this.padding});
}
