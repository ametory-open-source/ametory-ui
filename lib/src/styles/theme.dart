import 'package:flutter/material.dart';

import 'typo.dart';

class MyTheme {
  final BuildContext context;
  MyTheme(this.context);

  static ThemeData get lightTheme => ThemeData(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      scaffoldBackgroundColor: Colors.white,
      primarySwatch: Colors.purple,
      appBarTheme: const AppBarTheme(backgroundColor: Colors.white),
      brightness: Brightness.light,
      fontFamily: MyTypo.fontFamily);

  static ThemeData get darkTheme => ThemeData(
      brightness: Brightness.dark,
      fontFamily: MyTypo.fontFamily,
      scaffoldBackgroundColor: const Color(0xFF1b1b1d),
      appBarTheme: const AppBarTheme(backgroundColor: Color(0xFF1b1b1d)));
}
