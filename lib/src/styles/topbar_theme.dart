import 'package:flutter/material.dart';

class TopBarTheme {
  final bool isAuth;
  final double? height;
  final Color? color;
  final double? elevation;
  final BoxDecoration? decoration;
  final EdgeInsets? padding;

  TopBarTheme(
      {this.isAuth = false,
      this.height,
      this.color,
      this.elevation,
      this.decoration,
      this.padding});
}
