import 'package:ametory_ui/src/styles/color.dart';
import 'package:flutter/material.dart';

class MyStyle {
  static ButtonStyle buttonRed = TextButton.styleFrom(
      foregroundColor: Colors.white,
      minimumSize: const Size(100, 44),
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      backgroundColor: MyColor.danger);

  static ButtonStyle buttonDefault = TextButton.styleFrom(
      foregroundColor: Colors.white,
      minimumSize: const Size(100, 44),
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      backgroundColor: MyColor.primary);
  static ButtonStyle buttonWhite = TextButton.styleFrom(
      foregroundColor: MyColor.primary,
      minimumSize: const Size(100, 44),
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      shape: const RoundedRectangleBorder(
        side: BorderSide(color: MyColor.primary, width: 1),
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      backgroundColor: Colors.white);
  static ButtonStyle loginButton = TextButton.styleFrom(
      foregroundColor: Colors.black87,
      textStyle: const TextStyle(
        fontWeight: FontWeight.normal,
        fontSize: 16,
      ),
      minimumSize: const Size(100, 44),
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 15),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      backgroundColor: Colors.white);
  static ButtonStyle logiesButton = TextButton.styleFrom(
      foregroundColor: Colors.white,
      textStyle: const TextStyle(
        fontWeight: FontWeight.normal,
        fontSize: 16,
      ),
      minimumSize: const Size(100, 44),
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 15),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      backgroundColor: MyColor.primary);
}
