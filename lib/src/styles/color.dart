import 'package:flutter/material.dart';

class MyColor {
  static const MaterialColor info =
      MaterialColor(_infoPrimaryValue, <int, Color>{
    50: Color(0xFFE1FAFD),
    100: Color(0xFFB4F2FA),
    200: Color(0xFF82E9F7),
    300: Color(0xFF50E0F3),
    400: Color(0xFF2BDAF1),
    500: Color(_infoPrimaryValue),
    600: Color(0xFF04CEEC),
    700: Color(0xFF04C8E9),
    800: Color(0xFF03C2E7),
    900: Color(0xFF01B7E2),
  });
  static const int _infoPrimaryValue = 0xFF05D3EE;

  static const MaterialColor infoAccent =
      MaterialColor(_infoAccentValue, <int, Color>{
    100: Color(0xFFFFFFFF),
    200: Color(_infoAccentValue),
    400: Color(0xFFA3EBFF),
    700: Color(0xFF8AE6FF),
  });
  static const int _infoAccentValue = 0xFFD6F6FF;

  static const MaterialColor success =
      MaterialColor(_successPrimaryValue, <int, Color>{
    50: Color(0xFFECF6EC),
    100: Color(0xFFCEEACF),
    200: Color(0xFFAEDCB0),
    300: Color(0xFF8ECD90),
    400: Color(0xFF75C378),
    500: Color(_successPrimaryValue),
    600: Color(0xFF55B158),
    700: Color(0xFF4BA84E),
    800: Color(0xFF41A044),
    900: Color(0xFF309133),
  });
  static const int _successPrimaryValue = 0xFF5DB860;

  static const MaterialColor successAccent =
      MaterialColor(_successAccentValue, <int, Color>{
    100: Color(0xFFD9FFDA),
    200: Color(_successAccentValue),
    400: Color(0xFF73FF77),
    700: Color(0xFF59FF5E),
  });
  static const int _successAccentValue = 0xFFA6FFA8;

  static const MaterialColor warning =
      MaterialColor(_warningPrimaryValue, <int, Color>{
    50: Color(0xFFFFF4E4),
    100: Color(0xFFFFE3BB),
    200: Color(0xFFFFD18D),
    300: Color(0xFFFFBE5F),
    400: Color(0xFFFFB03D),
    500: Color(_warningPrimaryValue),
    600: Color(0xFFFF9A18),
    700: Color(0xFFFF9014),
    800: Color(0xFFFF8610),
    900: Color(0xFFFF7508),
  });
  static const int _warningPrimaryValue = 0xFFFFA21B;

  static const MaterialColor warningAccent =
      MaterialColor(_warningAccentValue, <int, Color>{
    100: Color(0xFFFFFFFF),
    200: Color(_warningAccentValue),
    400: Color(0xFFFFD9C1),
    700: Color(0xFFFFCAA7),
  });
  static const int _warningAccentValue = 0xFFFFF8F4;

  static const MaterialColor danger =
      MaterialColor(_dangerPrimaryValue, <int, Color>{
    50: Color(0xFFFEEBEA),
    100: Color(0xFFFCCECA),
    200: Color(0xFFFAADA7),
    300: Color(0xFFF88C83),
    400: Color(0xFFF77369),
    500: Color(_dangerPrimaryValue),
    600: Color(0xFFF45247),
    700: Color(0xFFF2483D),
    800: Color(0xFFF03F35),
    900: Color(0xFFEE2E25),
  });
  static const int _dangerPrimaryValue = 0xFFF55A4E;

  static const MaterialColor dangerAccent =
      MaterialColor(_dangerAccentValue, <int, Color>{
    100: Color(0xFFFFFFFF),
    200: Color(_dangerAccentValue),
    400: Color(0xFFFFC4C2),
    700: Color(0xFFFFACA8),
  });
  static const int _dangerAccentValue = 0xFFFFF5F5;

  static const MaterialColor primary =
      MaterialColor(_primaryPrimaryValue, <int, Color>{
    50: Color(0xFFF5E6F8),
    100: Color(0xFFE7C0ED),
    200: Color(0xFFD796E1),
    300: Color(0xFFC76BD5),
    400: Color(0xFFBB4CCC),
    500: Color(_primaryPrimaryValue),
    600: Color(0xFFA827BD),
    700: Color(0xFF9F21B5),
    800: Color(0xFF961BAE),
    900: Color(0xFF8610A1),
  });
  static const int _primaryPrimaryValue = 0xFFAF2CC3;

  static const MaterialColor primaryAccent =
      MaterialColor(_primaryAccentValue, <int, Color>{
    100: Color(0xFFF6D1FF),
    200: Color(_primaryAccentValue),
    400: Color(0xFFE16BFF),
    700: Color(0xFFDC52FF),
  });
  static const int _primaryAccentValue = 0xFFEB9EFF;
}
