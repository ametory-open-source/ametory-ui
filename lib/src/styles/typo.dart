import 'package:flutter/material.dart';

class MyTypo {
  static String fontFamily = "Montserrat";
  static TextStyle heading1 = const TextStyle(
      color: Colors.black87, fontWeight: FontWeight.w900, fontSize: 32);
  static TextStyle heading2 = const TextStyle(
      color: Colors.black87, fontWeight: FontWeight.bold, fontSize: 24);
  static TextStyle heading3 = const TextStyle(
      color: Colors.black87, fontWeight: FontWeight.bold, fontSize: 18);
  static TextStyle heading4 = const TextStyle(
      color: Colors.black87, fontWeight: FontWeight.bold, fontSize: 16);
  static TextStyle smallText =
      const TextStyle(color: Colors.black87, fontSize: 10);
  static TextStyle bodyText1 =
      const TextStyle(color: Colors.black87, fontSize: 12, height: 1.2);
  static TextStyle topMenu = const TextStyle(
      color: Colors.black54,
      fontSize: 14,
      height: 1.2,
      letterSpacing: 0.6,
      fontWeight: FontWeight.w500);
  static TextStyle linkText = const TextStyle(color: Colors.blue);
  static TextStyle whiteColor = const TextStyle(color: Colors.white);
  static TextStyle menuText = linkText.copyWith(fontSize: 16);
  static TextStyle menuBottom =
      const TextStyle(color: Colors.black54, fontSize: 14);
}
